/*
 * This program's job prints blob of random data on stdout
 * This blob will be encoded in base64 format
*/

#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

// Inlcluding cpp files is fairly ugly, but greatly reduces the build complexity
#include "encryption/CryptoEngine.cpp"
#include "encryption/Base64Engine.cpp"
#include "BPMException.cpp"

using bpm::encryption::CryptoEngine;
using bpm::encryption::Base64Engine;

static void usage(const char* name)
{
  std::cerr << "Usage: " << name << " bytes [-o file]\n";
  std::cerr << "\t bytes represents the number of random bytes to generate\n";
  std::cerr << "\t -o represents the header file to write" << std::endl;
}

int write_file(const char* path, const std::string& result)
{
  std::ofstream file(path);

  if (!file)
  {
    return 1;
  }

  file << "#pragma once\n"
       << "#define RANDOM_BLOB \"" << result << "\"\n\n";

  return 0;
}

const char* get_file_path(int argc, char** argv)
{
  if (argc <= 3 || strcmp(argv[2], "-o") != 0)
  {
    return nullptr;
  }

  return argv[3];
}

int main(int argc, char** argv)
{
  if (argc <= 1)
  {
    usage(argv[0]);
    return 1;
  }

  long size = -1;
  try
  {
    size = std::stol(argv[1]);
  }
  catch (const std::exception&)
  {
  }

  if (size <= 0)
  {
    std::cerr << "\"" << argv[1]
              << "\" is not a valid strictly positive integer\n";
    usage(argv[0]);
    return 1;
  }

  // Generate random bytes
  std::vector<unsigned char> data(size);
  CryptoEngine::GenerateRandomBlock(data.data(), size);

  std::string result(Base64Engine::GetEncodedSize(size), '\0');

  size_t resultsize = result.size();
  Base64Engine::Encode(data.data(), size, const_cast<char*>(result.data()),
                       resultsize);

  result.resize(resultsize);

  const char* output = get_file_path(argc, argv);

  if (output == nullptr)
  {
    std::cout << result;
    return 0;
  }

  return write_file(output, result);
}
