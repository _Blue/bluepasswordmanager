#pragma once

#include "ConfigManager.h"
#include "data/SecuredStorage.h"
#include "display/Banner.h"

namespace bpm
{
  namespace input
  {
    class InputMode
    {
    public:
      InputMode(ConfigManager& config, data::Storage& storage);
      virtual ~InputMode() = default;

      void Start();

    protected:
      virtual std::string GetRequestUrl() = 0;
      virtual void SetData(const data::SecuredString& value) = 0;
      virtual void WaitForNextStep() const = 0;
      virtual bool InputCredentials(data::SecuredString& domain,
                                    data::SecuredString& user,
                                    data::SecuredString& password) = 0;
      virtual std::shared_ptr<display::Banner> BuildBanner() const = 0;

      virtual void CreateBackup();
      virtual void RestoreBackup();

      ConfigManager& _config;
      data::Storage& _storage;

    private:
      void Run();
    };
  }
}
