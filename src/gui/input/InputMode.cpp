#include "InputMode.h"
#include "CancelException.h"
#include "Helper.h"

using bpm::input::InputMode;
using bpm::ConfigManager;
using bpm::data::Storage;
using bpm::data::SecuredString;

InputMode::InputMode(ConfigManager& cfg, Storage& storage)
    : _config(cfg), _storage(storage)
{
}

void InputMode::Start()
{
  // First back-up the data currently in clipboard
  CreateBackup();

  try
  {
    Run();
  }
  catch (const CancelException&)
  {
  }
  RestoreBackup(); // Make sure that backup will be restored if user cancels
}

void InputMode::Run()
{
  auto banner = BuildBanner();

  if (_config.IsBannerEnabled())
  {
    banner->ShowAsync();
    banner->SetText("BPM: Waiting for input");
  }

  SecuredString domain = Helper::GetDomainAuthority(GetRequestUrl());

  if (!_storage.HasCred(domain))
  {
    banner->Hide();
    data::SecuredString user;
    data::SecuredString password;
    if (InputCredentials(domain, user, password))
    {
      _storage.AddCred(domain, user, password, static_cast<size_t>(Helper::Now()), false);
    }
    return;
  }

  data::SecuredString user;
  data::SecuredString password;

  _storage.ReadCred(domain, user, password);

  SetData(user);
  banner->SetText("Username: \"" + user + "\" in clipboard");
  WaitForNextStep();

  SetData(password);
  banner->SetText("Password for username: \"" + user + "\" in clipboard");
  WaitForNextStep();
}

void InputMode::CreateBackup()
{
}

void InputMode::RestoreBackup()
{
}
