#pragma once

#include <string>
#include "ConfigManager.h"
#include "data/SecuredString.h"

#undef GetUserName // Win32 macro destroying method

namespace bpm
{
  namespace input
  {
    class CredentialsInput
    {
    public:
      using SymlinkValidateRoutine = std::function<bool(const data::SecuredString)>;

      CredentialsInput(const data::SecuredString& uri,
                       const data::SecuredString& user,
                       const data::SecuredString& password,
                       bool symlink,
                       const ConfigManager& config,
                       const SymlinkValidateRoutine& routine);
      ~CredentialsInput() = default;

      // Prompt the user for credentials, returns false if the user
      // clicks on 'Select from list' instead
      virtual bool QueryCredentials() = 0;

      const data::SecuredString& GetDomain() const;
      const data::SecuredString& GetUserName() const;
      const data::SecuredString& GetPassword() const;

    protected:
      void SelectCredentials();
      data::SecuredString GeneratePassword() const;

      data::SecuredString _domain;
      data::SecuredString _user;
      data::SecuredString _password;
      bool _symlink = false;
      ConfigManager _config;
      SymlinkValidateRoutine _symlink_validate;
    };
  }
}
