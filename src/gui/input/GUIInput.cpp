#include <cassert>

#include "GUIInput.h"
#include "BPMException.h"
#include "CancelException.h"
#include "OSSpecific.h"

#undef GetUserName // Windows macros ...

using bpm::ConfigManager;
using bpm::data::SecuredString;
using bpm::data::Storage;
using bpm::input::GUIInput;
using bpm::input::KeyWatcher;

GUIInput::GUIInput(ConfigManager& config, Storage& storage)
    : InputMode(config, storage)
{
}

std::string GUIInput::GetRequestUrl()
{
  if (!_request_uri.empty()) // In case of forced request URI
  {
    return _request_uri;
  }

  return WaitForClipboardChange();
}

void GUIInput::SetRequestURI(const std::string& request_uri)
{
  assert(_request_uri.empty());
  _request_uri = request_uri;
}

bool GUIInput::InputCredentials(SecuredString& domain,
                                SecuredString& username,
                                SecuredString& password)
{
  auto credinput = BuildCredentialsInput(domain, username);

  if (!credinput->QueryCredentials()) // If false, user clicked 'Display list'
  {
    OSSpecific::CredentialsDisplay display(_config, _storage);
    display.Show();

    return false;
  }

  domain = credinput->GetDomain();
  username = credinput->GetUserName();
  password = credinput->GetPassword();

  return true;
}

void GUIInput::CreateBackup()
{
  try
  {
    _original_data = ReadClipboard();
  }
  catch (const BPMException&) // If there is no clipboard set
  {
    // Leave at least an empty string,
    // don't keep a password in the clipboard
    _original_data.clear();
  }
}

void GUIInput::RestoreBackup()
{
  SetData(_original_data.c_str());
}

std::string GUIInput::WaitForClipboardChange()
{
  // Start checking for cancel (exit key)
  auto watcher = StartExitKeyWait();

  // Initialize clipboard with placeholder
  SetData(defs::placeholder_text);

  auto text = ReadClipboard();
  // Loop until its content change
  while (text.empty() || text == defs::placeholder_text)
  {
    // Wait for timeout
    auto timeout =
        std::chrono::milliseconds(static_cast<size_t>(defs::clip_refresh_ms));

    std::this_thread::sleep_for(timeout);
    text = ReadClipboard();

    // Check that the user did not cancel
    if (_exit_key_pressed)
    {
      break;
    }
  }

  // Stop waiting for exit key
  CancelExitWait(watcher);

  if (_exit_key_pressed) // user cancelled
  {
    throw CancelException();
  }

  return text;
}

std::shared_ptr<KeyWatcher> GUIInput::StartExitKeyWait()
{
  auto watcher = BuildKeyWatcher();

  // Start another thread waiting for exit key
  _exit_thread =
      std::thread(std::bind(&GUIInput::WaitForExitKeyImpl, this, watcher));

  return watcher;
}

void GUIInput::CancelExitWait(std::shared_ptr<KeyWatcher> watcher)
{
  assert(_exit_thread.joinable());
  watcher->CancelAsync();

  _exit_thread.join();
}

void GUIInput::WaitForExitKeyImpl(std::shared_ptr<KeyWatcher> watcher)
{
  assert(!_exit_key_pressed);

  auto exit_keys = _config.GetExitKeys();

  // Special case below on Windows escape key
  // Because the key display name depends on the locale, we can only do this
  // In the GUI module, and not in ConfigManager
#ifdef _WIN32
  if (exit_keys.empty())
  {
    exit_keys = {Win32KeyWatcher::GetEscapeKeyName()};
  }
#endif

  watcher->WaitForKeys({exit_keys});
  if (!watcher->Cancelled())
  {
    _exit_key_pressed = true;
  }
}

void GUIInput::WaitForNextStep() const
{
  // Wait for two groups of keys:
  //  - paste_keys: user pasted the text, we can continue
  //  - exit_keys: user cancelled

  auto watcher = BuildKeyWatcher();
  size_t group =
      watcher->WaitForKeys({_config.GetPasteKeys(), _config.GetExitKeys()});

  if (group == 1) // If user pressed exit keys
  {
    throw CancelException();
  }
}

bpm::input::CredentialsInput::SymlinkValidateRoutine
GUIInput::GetSymlinkValidateRoutine(const data::Storage& storage)
{
  return [&](const auto& target) {
    data::Storage::String username;
    data::Storage::String password;

    return storage.ReadCred(target, username, password, nullptr);
  };
}
