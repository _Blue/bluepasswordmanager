#pragma once

#include <set>
#include <vector>
#include <cstddef>
#include <string>

namespace bpm
{
  namespace input
  {
    class KeyWatcher
    {
    public:
      KeyWatcher() = default;
      ~KeyWatcher() = default;

      // Blocks until one of the set of keys is pressed
      // returns the position of the set in the vector
      virtual size_t WaitForKeys(const std::vector<std::set<std::string>> &keys) = 0;
      virtual void PrintPressedKeys() = 0;

      // Unblock a thread waiting in WaitForKeys
      void CancelAsync();

      // Returns true if CancelAsync has unblocked a waiting thread
      bool Cancelled() const;

    protected:
      virtual void CancelAsyncImpl();
      bool _cancelled = false;
    };
  }
}
