#include <algorithm>
#include <iostream>
#include <cstring>
#include <chrono>
#include <thread>
#include <X11/Xutil.h>
#include <X11/keysymdef.h>

#include "XKeyWatcher.h"
#include "BPMException.h"
#include "Defs.h"

using namespace std::placeholders;
using bpm::input::XKeyWatcher;

XKeyWatcher::XKeyWatcher()
{
  _display = XOpenDisplay(nullptr);

  if (_display == nullptr)
  {
    throw BPMException("Failed to open X display");
  }

  // XSynchronize returns the previous value, no need to check return value
  XSynchronize(_display, true);
}

size_t XKeyWatcher::WaitForKeys(const std::vector<std::set<std::string>>& keys)
{
  // Convert strings to key codes
  std::vector<std::set<KeyCode>> keycodes(keys.size());

  auto routine = std::bind(&XKeyWatcher::StringToKeyCode, this, _1);

  for (size_t i = 0; i < keys.size(); i++)
  {
    std::transform(keys[i].begin(), keys[i].end(),
                   std::inserter(keycodes[i], keycodes[i].begin()), routine);
  }

  // Wait for keys being pressed
  size_t ret = WaitForState(keycodes, true);

  // Wait for key set that has been pressed to be released
  WaitForState({keycodes[ret]}, false);

  return ret;
}

void XKeyWatcher::PrintPressedKeys()
{
  _printpressed = true;
  WaitForKeys({{"Escape"}});
}

size_t XKeyWatcher::WaitForState(const std::vector<std::set<KeyCode>>& keycodes,
                                 bool pressed)
{
  // Prepare timeout
  auto timeout =
      std::chrono::milliseconds(static_cast<size_t>(defs::x_clipboard_freq_ms));

  // Prepare keyboard state
  char state[32];
  if (!XQueryKeymap(_display, state))
  {
    throw BPMException("Failed to read X keymap");
  }

  // Loop until the keys we want are in the desired state (pressed or released)
  size_t pos = 0;
  while (!KeySetMatch(keycodes, state, pressed, pos) && !_cancelled)
  {
    std::this_thread::sleep_for(timeout);

    if (!XQueryKeymap(_display, state))
    {
      throw BPMException("Failed to read X keymap");
    }

    if (_printpressed)
    {
      PrintPressed(state);
    }
  }

  return pos;
}

bool XKeyWatcher::KeySetMatch(const std::vector<std::set<KeyCode>>& keys,
                              char const state[32], bool pressed,
                              size_t& pos) const
{
  // Find a set that is completely pressed
  for (size_t i = 0; i < keys.size(); i++)
  {
    if (GotAllKeys(keys[i], state, pressed))
    {
      pos = i;
      return true;
    }
  }
  return false;
}

bool XKeyWatcher::GotAllKeys(const std::set<KeyCode>& keys,
                             char const state[32], bool pressed) const
{
  // Two cases:
  // pressed = true
  //  We want all key pressed
  // pressed = false
  //  We all at least one key that is not pressed

  for (auto const& key : keys)
  {
    bool keypressed = IsKeyPressed(state, key);
    if (!keypressed)
    {
      return !pressed;
    }
  }
  return pressed;
}

KeyCode XKeyWatcher::StringToKeyCode(const std::string& keyname) const
{
  // Convert string to KeySym
  KeySym keysym = XStringToKeysym(keyname.data());
  if (keysym == 0)
  {
    throw BPMException("Failed to match key code for some key");
  }

  // Then convert KeySym to KeyCode
  KeyCode code = XKeysymToKeycode(_display, keysym);

  if (code == 0)
  {
    throw BPMException("Failed to match key code for some key");
  }

  return code;
}

std::string XKeyWatcher::KeyCodeToString(KeySym key)
{
  const char* name = XKeysymToString(key);

  if (name == nullptr)
  {
    throw BPMException("Failed to convert key code to string");
  }

  return name;
}

KeySym XKeyWatcher::KeyCodeToKeySym(unsigned short keycode) const
{
  int count;
  KeySym* sym = XGetKeyboardMapping(_display, keycode, 1, &count);

  if (sym == nullptr)
  {
    throw BPMException("Failed to convert key code to KeySym");
  }

  return *sym;
}

bool XKeyWatcher::IsKeyPressed(char const state[32], KeyCode key)
{
  // To check if the key is pressed, we have to check the key's bit

  // First isolate byte
  char byte = state[key / 8];

  // Then check bit
  return byte & (1 << (key % 8));
}

void XKeyWatcher::PrintPressed(char const state[32])
{
  for (size_t i = 0; i < 32 * 3; i++)
  {
    if (IsKeyPressed(state, i) && !IsKeyPressed(_laststate, i))
    {
      try
      {
        std::cout << KeyCodeToString(KeyCodeToKeySym(i)) << std::endl;
      }
      catch (const BPMException&)
      {
      }
    }
  }
  std::copy(state, state + 32, _laststate);
}

XKeyWatcher::~XKeyWatcher()
{
  XCloseDisplay(_display);
}
