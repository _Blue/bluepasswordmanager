#include <gtk/gtk.h>
#include <X11/Xlib.h>
#include <cstring>
#include "XPasswordInput.h"
#include "CancelException.h"

using bpm::input::XPasswordInput;
using bpm::input::PasswordInput;
using bpm::data::SecuredString;

XPasswordInput::XPasswordInput(const PasswordInput::CheckRoutine& routine,
                               bool create)
    : PasswordInput(routine, create)
{
}

SecuredString XPasswordInput::QueryPassword(const std::string& storage)
{
  _storage = storage;

  Display();

  if (!_success)
  {
    throw CancelException();
  }

  return _password;
}

bool XPasswordInput::PasswordValid(const SecuredString& password)
{
  return _checkpassword(password);
}

void XPasswordInput::Build()
{
  _window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(_window), "BPM - unlock storage");
  gtk_widget_set_size_request(_window, 400, 80);

  _password_field = gtk_entry_new();

  std::string label_text;
  if (_create)
  {
    label_text = "Create password for storage: \"" + _storage + "\"";
  }
  else
  {
    label_text = "Password required to unlock storage:\"" + _storage + "\"";
  }

  GtkWidget* label = gtk_label_new(label_text.data());
  _listbox = gtk_list_box_new();

  gtk_list_box_set_selection_mode(GTK_LIST_BOX(_listbox), GTK_SELECTION_NONE);

  gtk_widget_show(_listbox);

  gtk_entry_set_visibility(GTK_ENTRY(_password_field), false);
  gtk_entry_set_placeholder_text(GTK_ENTRY(_password_field),
                                 "Storage password");

  gtk_container_add(GTK_CONTAINER(_listbox), label);
  gtk_container_add(GTK_CONTAINER(_listbox), _password_field);
  gtk_container_add(GTK_CONTAINER(_window), _listbox);
  gtk_container_set_border_width(GTK_CONTAINER(_window), 10);

  if (_create)
  {
    AddConfirmationField();
  }

  // Set the focus to the text input so that no click is needed
  gtk_window_set_focus(GTK_WINDOW(_window), _password_field);

  g_signal_connect(G_OBJECT(_window), "delete-event", G_CALLBACK(Quit), this);

  g_signal_connect(G_OBJECT(_password_field),
                   "activate",
                   G_CALLBACK(&XPasswordInput::CheckPassword),
                   this);

  gtk_widget_show_all(_window);
}

void XPasswordInput::AddConfirmationField()
{
  _confirmation_field = gtk_entry_new();
  gtk_entry_set_visibility(GTK_ENTRY(_confirmation_field), false);
  gtk_entry_set_placeholder_text(GTK_ENTRY(_confirmation_field),
                                 "Confirm password");

  g_signal_connect(G_OBJECT(_confirmation_field),
                   "activate",
                   G_CALLBACK(&XPasswordInput::CheckPassword),
                   this);

  gtk_container_add(GTK_CONTAINER(_listbox), _confirmation_field);
}

void XPasswordInput::CheckPassword(GtkWidget* text, XPasswordInput* instance)
{
  const char* password = gtk_entry_get_text(GTK_ENTRY(text));

  bool success;
  // If user is creating a new storage, just check for field validity
  if (instance->_create)
  {
    success = instance->CheckPasswordCreation();
  }
  else // If the storage already exists, verify it
  {
    success = instance->PasswordValid(password);
  }

  if (success)
  {
    // Save the password
    instance->SetPassword(password);

    instance->_success = true;

    // Exit if the password is valid
    Quit(nullptr, nullptr, instance);
    return;
  }

  if (!instance->_create)
  {
    gtk_entry_set_text(GTK_ENTRY(text), "");
  }

  instance->ShowBadPasswordNotice();
}

bool XPasswordInput::CheckPasswordCreation() const
{
  const char* password = gtk_entry_get_text(GTK_ENTRY(_password_field));
  const char* confirmation = gtk_entry_get_text(GTK_ENTRY(_confirmation_field));

  // Check password match and don't allow empty password
  return std::strcmp(password, confirmation) == 0 && strlen(password) > 0;
}

void XPasswordInput::SetPassword(const SecuredString& password)
{
  _password = password;
}

void XPasswordInput::ShowBadPasswordNotice()
{
  if (_bad_password_notice_visible)
  {
    return;
  }

  _bad_password_notice_visible = true;
  GtkWidget* label;

  if (_create)
  {
    label = gtk_label_new("Passwords don't match");
  }
  else
  {
    label = gtk_label_new("Failed to unlock storage");
  }

  gtk_container_add(GTK_CONTAINER(_listbox), label);

  gtk_widget_show(label);
}

void XPasswordInput::Quit(GtkWidget*, GdkEvent*, XPasswordInput* instance)
{
  gtk_widget_destroy(instance->_window);
  static_cast<XWindow*>(instance)->Quit();
}
