#include <X11/Xlib.h>
#include <gtk/gtk.h>
#include <string.h>
#include <thread>
#include <map>
#include <cassert>
#include <functional>
#include "GTKHandler.h"

using bpm::input::GTKHandler;

namespace
{
  std::thread _thread;
}

void GTKHandler::RunGTKImpl(std::promise<void>& promise)
{
  XInitThreads();
  gtk_init(nullptr, nullptr);

  promise.set_value();

  gtk_main();
}

int GTKHandler::CallInit(const std::function<void()>* init)
{
  (*init)();

  return false;
}

void GTKHandler::StartGTKLoop()
{
  assert(!_thread.joinable());

  if (!X11Available())
  {
    throw std::runtime_error("X11 is not available");
  }

  std::promise<void> promise;

  _thread = std::thread(std::bind(&GTKHandler::RunGTKImpl, std::ref(promise)));

  promise.get_future().get();
}

void GTKHandler::Schedule(const std::function<void()>& routine)
{
  std::promise<void> promise;

  std::function<void()> wrapper = [&promise, &routine]() {
    routine();

    promise.set_value();
  };

  g_idle_add(reinterpret_cast<GSourceFunc>(&GTKHandler::CallInit),
             (gpointer)&wrapper);

  promise.get_future().get();
}

bool GTKHandler::X11Available()
{
  for (char** var = environ; *var != nullptr; var++)
  {
    if (strstr(*var, "DISPLAY=") == *var)
    {
      return true;
    }
  }

  return false;
}

void GTKHandler::ExitGTKLoop()
{
  gtk_main_quit();
  _thread.join();
}

GTKHandler::GTKLock::GTKLock()
{
  GTKHandler::StartGTKLoop();
}

GTKHandler::GTKLock::~GTKLock()
{
  if (!_moved)
  {
    GTKHandler::ExitGTKLoop();
  }
}

GTKHandler::GTKLock::GTKLock(GTKLock&& other)
{
  *this = std::move(other);
}

const GTKHandler::GTKLock& GTKHandler::GTKLock::operator=(GTKLock&& other)
{
  other._moved = true;

  return *this;
}
