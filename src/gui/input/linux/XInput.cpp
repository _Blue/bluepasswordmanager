#include <X11/Xlib.h>
#include <gtk/gtk.h>
#include <cstring>

#include "XInput.h"
#include "XKeyWatcher.h"
#include "XCredentialsInput.h"
#include "BPMException.h"
#include "CancelException.h"
#include "GTKHandler.h"
#include "display/linux/XBanner.h"

using bpm::data::SecuredString;

namespace bpm
{
  namespace input
  {
    XInput::XInput(ConfigManager& config, data::Storage& storage)
        : GUIInput(config, storage)
    {
      _clipboard = gtk_clipboard_get(GDK_SELECTION_CLIPBOARD);
      if (_clipboard == nullptr)
      {
        throw BPMException("Failed to get access to the clipboard");
      }
    }

    std::shared_ptr<CredentialsInput>
    XInput::BuildCredentialsInput(const SecuredString& uri,
                                  const SecuredString& username) const
    {
      auto ptr = std::make_shared<XCredentialsInput>(uri, username, "", false, _config, GetSymlinkValidateRoutine(_storage));
      return std::static_pointer_cast<CredentialsInput>(ptr);
    }

    void XInput::SetData(const SecuredString& value)
    {
      auto routine = [&]() {
        gtk_clipboard_set_text(_clipboard, value.data(), value.size());
      };

      GTKHandler::RunInGtkContext<void>(routine);
    }

    std::string XInput::ReadClipboardImpl()
    {
      const char* text = gtk_clipboard_wait_for_text(_clipboard);

      if (text == nullptr)
      {
        return "";
      }

      std::string result(text);
      g_free(const_cast<void*>(reinterpret_cast<const void*>(text)));

      return result;
    }

    std::string XInput::ReadClipboard()
    {
      return GTKHandler::RunInGtkContext<std::string>(
          std::bind(&XInput::ReadClipboardImpl, this));
    }

    std::shared_ptr<display::Banner> XInput::BuildBanner() const
    {
      return std::make_unique<bpm::display::XBanner>(_config);
    }

    std::shared_ptr<KeyWatcher> XInput::BuildKeyWatcher() const
    {
      return std::make_shared<XKeyWatcher>();
    }
  }
}
