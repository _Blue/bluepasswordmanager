#pragma once

// This header forward declares gtk structures

struct _GtkWidget;
typedef struct _GtkWidget GtkWidget;

struct _GtkEntry;
typedef struct _GtkEntry GtkEntry;

union _GdkEvent;
typedef _GdkEvent GdkEvent;

struct _GtkClipboard;
typedef _GtkClipboard GtkClipboard;

struct _GdkEventButton;
typedef _GdkEventButton GdkEventButton;

struct _GtkListStore;
typedef _GtkListStore  GtkListStore;

struct _GtkCellRendererText;
typedef _GtkCellRendererText GtkCellRendererText;

struct _GtkEntry;
typedef _GtkEntry GtkEntry;

struct _GtkCellRenderer;
typedef _GtkCellRenderer GtkCellRenderer;

struct _GtkToggleButton;
typedef _GtkToggleButton GtkToggleButton;

struct _GtkContainer;
typedef _GtkContainer GtkContainer;

struct _GdkEventKey;
typedef _GdkEventKey GdkEventKey;
