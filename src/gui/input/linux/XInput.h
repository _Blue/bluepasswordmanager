#pragma once

#include <thread>

#include "input/GUIInput.h"
#include "display/Banner.h"
#include "GTKfwd.h"
#include "GTKHandler.h"

namespace bpm
{
  namespace input
  {
    class XInput : public GUIInput
    {
    public:
      XInput(ConfigManager& config, data::Storage& storage);
      virtual ~XInput() = default;

      virtual void SetData(const data::SecuredString& value) override;

    protected:
      virtual std::shared_ptr<CredentialsInput>
      BuildCredentialsInput(const data::SecuredString& uri,
                            const data::SecuredString& password) const override;
      virtual std::shared_ptr<display::Banner> BuildBanner() const override;
      virtual std::shared_ptr<KeyWatcher> BuildKeyWatcher() const override;

    private:
      std::string ReadClipboard() override;
      std::string ReadClipboardImpl();
      GtkClipboard* _clipboard = nullptr;
    };
  }
}
