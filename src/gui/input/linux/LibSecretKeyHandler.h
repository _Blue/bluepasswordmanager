#pragma once

#include "persistence/KeyHandler.h"

namespace bpm
{
  namespace input
  {
    class LibSecretKeyHandler : public persistence::KeyHandler
    {
      public:
        LibSecretKeyHandler(size_t key_size);

        virtual ~LibSecretKeyHandler() = default;

        virtual data::SecuredVector Read(size_t max) override;

        virtual void Write(const data::SecuredVector& data) override;

        virtual void Clear() override;

        virtual bool IsKeyAvailable() override;

      private:
        size_t _key_size = 0;
    };
  }
}
