#pragma once

#include <map>
#include <functional>
#include <string>
#include <X11/X.h>
#include <X11/Xlib.h>
#include "input/KeyWatcher.h"

namespace bpm
{
  namespace input
  {
    class XKeyWatcher : public KeyWatcher
    {
    public:
      XKeyWatcher();
      ~XKeyWatcher();

      size_t
      WaitForKeys(const std::vector<std::set<std::string>>& keys) override;
      void PrintPressedKeys() override;

    private:
      size_t WaitForState(const std::vector<std::set<KeyCode>>& keys,
                          bool pressed);
      bool GotAllKeys(const std::set<KeyCode>& keys, char const state[32],
                      bool pressed) const;

      bool KeySetMatch(const std::vector<std::set<KeyCode>>& key,
                       char const state[32], bool pressed, size_t& pos) const;

      KeyCode StringToKeyCode(const std::string& keyname) const;
      KeySym KeyCodeToKeySym(unsigned short keycode) const;
      void PrintPressed(char const state[32]);

      static std::string KeyCodeToString(KeySym key);
      static bool IsKeyPressed(char const state[32], KeyCode key);

      Display* _display = nullptr;
      bool _printpressed = false;
      char _laststate[32] = {0};
      std::set<KeySym> _pressedkeys;
      std::map<int, std::function<void(XEvent&)>> _routines;
    };
  }
}
