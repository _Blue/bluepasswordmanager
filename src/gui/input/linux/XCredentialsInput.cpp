#include <X11/Xlib.h>
#include <functional>
#include <gdk/gdkkeysyms.h>
#include <gtk/gtk.h>

#include "BPMException.h"
#include "CancelException.h"
#include "XCredentialsInput.h"
#include "input/InputMode.h"
#include "Helper.h"

using bpm::data::SecuredString;
using bpm::input::XCredentialsInput;

XCredentialsInput::XCredentialsInput(
    const SecuredString& uri,
    const SecuredString& username,
    const SecuredString& password,
    bool symlink,
    const ConfigManager& config,
    const SymlinkValidateRoutine& symlink_validate)
    : CredentialsInput(
          uri, username, password, symlink, config, symlink_validate)
{
}

bool XCredentialsInput::QueryCredentials()
{
  XWindow::Display();

  if (_display_list)
  {
    return false;
  }

  if (!_success)
  {
    throw CancelException();
  }

  return true;
}

void XCredentialsInput::Build()
{
  // Create window
  _window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(_window), "BPM - Add credentials");
  gtk_window_set_default_size(GTK_WINDOW(_window), 400, 150);

  // Create text inputs
  _uri_entry = gtk_entry_new();
  _username_entry = gtk_entry_new();
  _password_entry = gtk_entry_new();
  _symlink_checkbox = gtk_check_button_new_with_label("Symlink");
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(_symlink_checkbox), _symlink);

  gtk_entry_set_text(GTK_ENTRY(_uri_entry), _domain.data());
  gtk_entry_set_text(GTK_ENTRY(_password_entry), _password.data());

  gtk_entry_set_visibility(GTK_ENTRY(_password_entry), false);
  gtk_entry_set_placeholder_text(GTK_ENTRY(_uri_entry), "URI");
  gtk_entry_set_placeholder_text(GTK_ENTRY(_username_entry), "Username");
  gtk_entry_set_placeholder_text(GTK_ENTRY(_password_entry), "Password");

  // Create domain label
  _domain_label = gtk_label_new("Invalid URI");
  // gtk_label_set_selectable(GTK_LABEL(_domain_label), false);

  // Create the list box (vertical layout)
  GtkWidget* box = gtk_list_box_new();

  GtkWidget* buttonbox = gtk_button_box_new(GTK_ORIENTATION_HORIZONTAL);
  gtk_button_box_set_layout(GTK_BUTTON_BOX(buttonbox), GTK_BUTTONBOX_END);

  _OK_button = gtk_button_new_with_label("OK");
  _open_list_button = gtk_button_new_with_label("Display list");
  GtkWidget* cancel = gtk_button_new_with_label("Cancel");
  _generate_password = gtk_button_new_with_label("Generate password");

  gtk_list_box_set_selection_mode(GTK_LIST_BOX(box), GTK_SELECTION_NONE);

  // Add items to their containers
  gtk_box_set_spacing(GTK_BOX(buttonbox), 10);
  gtk_container_add(GTK_CONTAINER(buttonbox), _open_list_button);
  gtk_container_add(GTK_CONTAINER(buttonbox), _generate_password);
  gtk_container_add(GTK_CONTAINER(buttonbox), _OK_button);
  gtk_container_add(GTK_CONTAINER(buttonbox), cancel);
  gtk_container_add(GTK_CONTAINER(buttonbox), _symlink_checkbox);
  gtk_container_add(GTK_CONTAINER(box), _uri_entry);
  gtk_container_add(GTK_CONTAINER(box), _domain_label);
  gtk_container_add(GTK_CONTAINER(box), _username_entry);
  gtk_container_add(GTK_CONTAINER(box), _password_entry);
  gtk_container_add(GTK_CONTAINER(box), buttonbox);
  gtk_container_add(GTK_CONTAINER(_window), box);
  gtk_container_set_border_width(GTK_CONTAINER(_window), 10);

  // Register signal handlers
  g_signal_connect(_window, "delete-event", G_CALLBACK(WindowClosed), this);
  g_signal_connect(_OK_button, "clicked", G_CALLBACK(OKPressed), this);
  g_signal_connect(
      _open_list_button, "clicked", G_CALLBACK(CancelPressed), this);
  g_signal_connect(
      _symlink_checkbox, "toggled", G_CALLBACK(SymlinkPressed), this);

  g_signal_connect(cancel, "clicked", G_CALLBACK(CancelPressed), this);
  g_signal_connect(
      _generate_password, "clicked", G_CALLBACK(GeneratePassword), this);
  g_signal_connect(
      _username_entry, "notify::text", G_CALLBACK(EntryEdited), this);
  g_signal_connect(
      _password_entry, "notify::text", G_CALLBACK(EntryEdited), this);
  g_signal_connect(_uri_entry, "notify::text", G_CALLBACK(EntryEdited), this);

  g_signal_connect(_uri_entry, "key_press_event", G_CALLBACK(KeyPressed), this);
  g_signal_connect(
      _username_entry, "key_press_event", G_CALLBACK(KeyPressed), this);
  g_signal_connect(
      _password_entry, "key_press_event", G_CALLBACK(KeyPressed), this);

  Update();

  gtk_widget_grab_focus(_uri_entry);
  gtk_widget_show_all(_window);
}

bool XCredentialsInput::KeyPressed(GtkWidget* widget,
                                   GdkEventKey* event,
                                   XCredentialsInput* instance)
{
  if (event->keyval == GDK_KEY_Return)
  {
    ReturnPressed(widget, instance);
    return true;
  }

  if (event->keyval != GDK_KEY_Tab)
  {
    return false;
  }

  if (widget == instance->_uri_entry)
  {
    gtk_widget_grab_focus(GTK_WIDGET(instance->_username_entry));
  }
  else if (widget == instance->_username_entry)
  {
    gtk_widget_grab_focus(GTK_WIDGET(instance->_password_entry));
  }
  else
  {
    return false;
  }

  return true;
}

void XCredentialsInput::ReturnPressed(GtkWidget*, XCredentialsInput* instance)
{
  if (!gtk_widget_is_sensitive(instance->_OK_button))
  {
    return;
  }

  OKPressed(instance->_OK_button, instance);
}

void XCredentialsInput::EntryEdited(GtkEntry*,
                                    gchar*,
                                    XCredentialsInput* instance)
{
  instance->Update();
}

bool XCredentialsInput::UpdateDomainLabel()
{
  std::string text;
  bool valid = true;
  try
  {
    const char* uri = gtk_entry_get_text(GTK_ENTRY(_uri_entry));
    text =
        "Domain name: " + std::string(Helper::GetDomainAuthority(uri).c_str());
  }
  catch (const BPMException&) // In case the URI is invalid
  {
    text = "Invalid URI";
    valid = false;
  }

  gtk_label_set_text(GTK_LABEL(_domain_label), text.data());

  return valid;
}

void XCredentialsInput::Update()
{
  if (gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(_symlink_checkbox)))
  {
    gtk_entry_set_text(GTK_ENTRY(_username_entry), defs::symlink_prefix);
    gtk_entry_set_placeholder_text(GTK_ENTRY(_password_entry), "Target domain");
    gtk_entry_set_visibility(GTK_ENTRY(_password_entry), true);
    gtk_widget_set_sensitive(_username_entry, false);
    gtk_widget_set_sensitive(_generate_password, false);
    gtk_widget_set_sensitive(
        _OK_button,
        UpdateDomainLabel() &&
            _symlink_validate(gtk_entry_get_text(GTK_ENTRY(_password_entry))));
  }
  else
  {
    gtk_entry_set_visibility(GTK_ENTRY(_password_entry), false);

    bool emptyfield =
        gtk_entry_get_text_length(GTK_ENTRY(_uri_entry)) == 0 ||
        gtk_entry_get_text_length(GTK_ENTRY(_username_entry)) == 0 ||
        gtk_entry_get_text_length(GTK_ENTRY(_password_entry)) == 0;

    gtk_widget_set_sensitive(_username_entry, true);
    gtk_widget_set_sensitive(_generate_password, true);
    gtk_entry_set_placeholder_text(GTK_ENTRY(_password_entry), "Password");

    std::string username = gtk_entry_get_text(GTK_ENTRY(_username_entry));
    if (username == defs::symlink_prefix)
    {
      gtk_entry_set_text(GTK_ENTRY(_username_entry), "");
    }

    // Disable the "OK"
    gtk_widget_set_sensitive(_OK_button, UpdateDomainLabel() && !emptyfield);
  }
}

void XCredentialsInput::OKPressed(GtkWidget*, XCredentialsInput* instance)
{
  instance->_success = true;
  instance->Quit();
}

void XCredentialsInput::CancelPressed(GtkWidget* widget,
                                      XCredentialsInput* instance)
{
  if (widget == instance->_open_list_button)
  {
    instance->_display_list = true;
  }
  instance->Quit();
}

void XCredentialsInput::Quit()
{
  if (_success)
  {
    _domain =
        Helper::GetDomainAuthority(gtk_entry_get_text(GTK_ENTRY(_uri_entry)));
    _user = gtk_entry_get_text(GTK_ENTRY(_username_entry));
    _password = gtk_entry_get_text(GTK_ENTRY(_password_entry));
  }

  gtk_widget_destroy(_window);

  XWindow::Quit();
}

bool XCredentialsInput::WindowClosed(GtkWidget*,
                                     GdkEvent*,
                                     XCredentialsInput* instance)
{
  instance->Quit();
  return false;
}

bool XCredentialsInput::GeneratePassword(GtkWidget*, XCredentialsInput* ptr)
{
  ptr->GeneratePasswordImpl();

  return true;
}

void XCredentialsInput::GeneratePasswordImpl()
{
  auto password = CredentialsInput::GeneratePassword();
  gtk_entry_set_text(GTK_ENTRY(_password_entry), password.c_str());
  gtk_entry_set_visibility(GTK_ENTRY(_password_entry), true);
}

void XCredentialsInput::SymlinkPressed(GtkWidget*, XCredentialsInput* ptr)
{
  ptr->Update();
}
