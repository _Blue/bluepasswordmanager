#pragma once

#include <future>

namespace bpm
{
  namespace input
  {
    class XWindow
    {
    public:
      virtual ~XWindow() = default;

      void Display();
      void Quit();

    protected:
      virtual void Build() = 0;

    private:
      void ShowImpl();

      std::promise<void> _promise;
    };
  }
}
