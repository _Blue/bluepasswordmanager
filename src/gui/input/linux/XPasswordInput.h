#pragma once

#include "input/PasswordInput.h"
#include "GTKfwd.h"
#include "GTKHandler.h"
#include "XWindow.h"

namespace bpm
{
  namespace input
  {
    class XPasswordInput : public PasswordInput, public XWindow
    {
    public:
      XPasswordInput(const PasswordInput::CheckRoutine& routine, bool create);

      virtual data::SecuredString
      QueryPassword(const std::string& storage) override;

    private:
      static void CheckPassword(GtkWidget* widget, XPasswordInput* instance);
      bool CheckPasswordCreation() const;
      bool PasswordValid(const data::SecuredString& password);
      void SetPassword(const data::SecuredString& password);
      void ShowBadPasswordNotice();
      virtual void Build() override;
      void AddConfirmationField();
      static void Quit(GtkWidget*, GdkEvent*, XPasswordInput* instance);

      GtkWidget* _window = nullptr;
      GtkWidget* _listbox = nullptr;
      GtkWidget* _password_field = nullptr;
      GtkWidget* _confirmation_field = nullptr;
      data::SecuredString _password;
      std::string _storage;
      bool _success = false;
      bool _bad_password_notice_visible = false;
    };
  }
}
