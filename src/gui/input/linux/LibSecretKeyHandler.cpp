#include <libsecret/secret.h>

#include "LibSecretKeyHandler.h"
#include "Defs.h"
#include "encryption/Encryptor.h"
#include "BPMException.h"

using bpm::data::SecuredVector;
using bpm::input::LibSecretKeyHandler;

static const SecretSchema schema = {
    "org.bpm",
    SECRET_SCHEMA_NONE,
    {
        {"bpm", SECRET_SCHEMA_ATTRIBUTE_BOOLEAN},
        {"NULL", SECRET_SCHEMA_ATTRIBUTE_STRING},
    },
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0};

LibSecretKeyHandler::LibSecretKeyHandler(size_t key_size)
    : persistence::KeyHandler(key_size)
{
}

bool LibSecretKeyHandler::IsKeyAvailable()
{
  GError* error = nullptr;

  char* content = secret_password_lookup_sync(
      &schema, nullptr, &error, "bpm", true, nullptr);

  if (content != nullptr)
  {
    secret_password_free(content);
    return true;
  }
  else
  {
    return false;
  }
}

SecuredVector LibSecretKeyHandler::Read(size_t)
{
  GError* error = nullptr;

  char* content = secret_password_lookup_sync(
      &schema, nullptr, &error, "bpm", true, nullptr);

  if (content == nullptr)
  {
    throw new BPMException("Failed to read key from libsecret, " +
                           std::string(error->message));
  }

  auto key = encryption::Encryptor::Base64Decode(content);

  secret_password_free(content);

  return key;
}

void LibSecretKeyHandler::Write(const SecuredVector& data)
{
  GError* error = nullptr;

  data::SecuredString content = encryption::Encryptor::Base64Encode(data);
  if (!secret_password_store_sync(
          &schema,
          SECRET_COLLECTION_DEFAULT,
          defs::gnome_keyring_display_name,
          content.c_str(),
          nullptr,
          &error,
          "bpm",
          true,
          nullptr))
  {
    throw BPMException("Failed to write libsecret key, " +
                       std::string(error->message));
  }
}

void LibSecretKeyHandler::Clear()
{
  GError* error = nullptr;

  if (!secret_password_clear_sync(
          &schema, nullptr, &error, "bpm", true, nullptr) &&
      error != nullptr)
  {
    throw BPMException("Failed to delete key from libsecret store, " +
                       std::string(error->message));
  }
}
