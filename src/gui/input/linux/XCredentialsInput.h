#pragma once

#include "GTKfwd.h"
#include "input/CredentialsInput.h"
#include "XWindow.h"
#include <optional>

namespace bpm
{
  namespace input
  {
    class XCredentialsInput : public CredentialsInput, public XWindow
    {
    public:
      XCredentialsInput(const data::SecuredString& uri,
                        const data::SecuredString& username,
                        const data::SecuredString& password,
                        bool symlink,
                        const ConfigManager& config,
                        const SymlinkValidateRoutine& symlink_validate);
      virtual ~XCredentialsInput() = default;

      virtual bool QueryCredentials() override;

    private:
      virtual void Build() override;
      bool UpdateDomainLabel(); // Returns true if the uri is valid
      void Update();
      void Quit();
      static void OKPressed(GtkWidget*, XCredentialsInput* ptr);
      static void CancelPressed(GtkWidget*, XCredentialsInput*);
      static bool WindowClosed(GtkWidget*, GdkEvent*, XCredentialsInput* ptr);
      static void EntryEdited(GtkEntry*, char*, XCredentialsInput* ptr);
      static bool
      KeyPressed(GtkWidget* entry, GdkEventKey* event, XCredentialsInput* ptr);
      static void ReturnPressed(GtkWidget*, XCredentialsInput* ptr);
      static bool GeneratePassword(GtkWidget*, XCredentialsInput* ptr);
      static void SymlinkPressed(GtkWidget*, XCredentialsInput* ptr);
      void GeneratePasswordImpl();

      GtkWidget* _window = nullptr;
      GtkWidget* _uri_entry = nullptr;
      GtkWidget* _username_entry = nullptr;
      GtkWidget* _password_entry = nullptr;
      GtkWidget* _domain_label = nullptr;
      GtkWidget* _OK_button = nullptr;
      GtkWidget* _open_list_button = nullptr;
      GtkWidget* _symlink_checkbox = nullptr;
      GtkWidget* _generate_password;
      bool _success = false;
      bool _display_list = false;
    };
  }
}
