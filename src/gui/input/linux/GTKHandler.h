#pragma once

/*
 * This class is used to easily maintain a GTK loop across multiple threads
 */

#include <future>
namespace bpm
{
  namespace input
  {
    class GTKHandler
    {
    public:
      static void StartGTKLoop();
      static void ExitGTKLoop();

      template <typename T>
      static T RunInGtkContext(const std::function<T()>& routine);

      class GTKLock
      {
        public:
          GTKLock();
          ~GTKLock();

         GTKLock(const GTKLock&) = delete;
         GTKLock(GTKLock&& other);

         const GTKLock& operator=(const GTKLock&) = delete;
         const GTKLock& operator=(GTKLock&& other);

        private:
         bool _moved = false;
      };


    private:
      static void Schedule(const std::function<void()>& routine);
      static void RunGTKImpl(std::promise<void>& promise);
      static void RunImpl();
      static int CallInit(const std::function<void()>* init);
      static bool X11Available();
    };
  }
}

#include "GTKHandler.hxx"
