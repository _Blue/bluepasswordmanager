#include <functional>
#include "XWindow.h"
#include "GTKHandler.h"

using bpm::input::XWindow;

void XWindow::Display()
{
  _promise = {};

  GTKHandler::RunInGtkContext<void>(std::bind(&XWindow::ShowImpl, this));

  _promise.get_future().wait();
}

void XWindow::ShowImpl()
{
  Build();
}

void XWindow::Quit()
{
  _promise.set_value();
}
