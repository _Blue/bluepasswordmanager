#pragma once
#include "GTKHandler.h"

namespace bpm
{
  namespace input
  {
    template <typename T>
    inline T GTKHandler::RunInGtkContext(const std::function<T()>& routine)
    {
      T value;

      std::function<void()> wrapper = [&value, &routine]() {
        value = routine();
      };

      Schedule(wrapper);
      return value;
    }

    template <>
    inline void
    GTKHandler::RunInGtkContext<void>(const std::function<void()>& routine)
    {
      Schedule(routine);
    }
  }
}
