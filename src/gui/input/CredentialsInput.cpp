#include "CredentialsInput.h"
#include "Helper.h"

using bpm::data::SecuredString;
using bpm::input::CredentialsInput;

CredentialsInput::CredentialsInput(
    const SecuredString& domain,
    const SecuredString& user,
    const SecuredString& password,
    bool symlink,
    const ConfigManager& config,
    const CredentialsInput::SymlinkValidateRoutine& symlink_validate)
    : _domain(domain),
      _user(user),
      _password(password),
      _symlink(symlink),
      _config(config),
      _symlink_validate(symlink_validate)
{
}

const SecuredString& CredentialsInput::GetDomain() const
{
  return _domain;
}

const SecuredString& CredentialsInput::GetUserName() const
{
  return _user;
}

const SecuredString& CredentialsInput::GetPassword() const
{
  return _password;
}

SecuredString CredentialsInput::GeneratePassword() const
{
  return Helper::GeneratePassword(_config.GetPasswordGenerationCharset(),
                                  _config.GetPasswordGenerationSize());
}
