#include <algorithm>
#include <iostream>
#include <cassert>
#include "Win32KeyWatcher.h"
#include "BPMException.h"
#include "CancelException.h"
#include "input/win32/SyscallWrapper.h"

using bpm::input::Win32KeyWatcher;

//No other way to get class ptr inside keyboard hook
Win32KeyWatcher *instance = nullptr;

void Win32KeyWatcher::InstallHook()
{
  HMODULE module = GetModuleHandleA(nullptr);

  if (module != nullptr)
  {
    _hook = SetWindowsHookExA(WH_KEYBOARD_LL, &Win32KeyWatcher::KeyEvent, module, 0);
  }

  if (_hook == nullptr)
  {
    throw BPMException("Failed to install keyboard hook");
  }

  instance = this;
}

void Win32KeyWatcher::InitState()
{
  BYTE state[256];

  Syscall(GetKeyboardState, state);

  for (unsigned short i = 0; i < 256; i++)
  {
    //Check if key is pressed
    if (GetAsyncKeyState(i) & 0x8000)
    {
      KBDLLHOOKSTRUCT key = { 0 };
      key.vkCode = i;

      //Retrieve scan code
      key.scanCode = MapVirtualKeyA(key.vkCode, MAPVK_VK_TO_VSC);

      //Check that translation succeeded
      if (key.scanCode != 0)
      {
        KeyPressed(&key, true);
      }
    }
  }
}

size_t Win32KeyWatcher::WaitForKeys(const std::vector<std::set<std::string>> &keys)
{
  assert(_thread_id == 0);

  _expected_keys = keys;
  _thread_id = GetCurrentThreadId();
  InstallHook();
  InitState();
  RunEventLoop();

  _thread_id = 0;
  return _key_group;
}

void Win32KeyWatcher::PrintPressedKeys()
{
  _printkeys = true;
  std::cout << "Press escape to exit" << std::endl;

  WaitForKeys({ { GetEscapeKeyName() } });
}

std::string Win32KeyWatcher::GetEscapeKeyName()
{
  KBDLLHOOKSTRUCT esc = { 0 };
  esc.vkCode = VK_ESCAPE;
  esc.scanCode = 1;//escape scan code is 1

  return VKCodeToString(&esc);
}

void Win32KeyWatcher::RunEventLoop()
{
  MSG message;
  while (GetMessageA(&message, nullptr, 0, 0))
  {
    DispatchMessageA(&message);
  }
}

LRESULT CALLBACK Win32KeyWatcher::KeyEvent(int code, WPARAM wparam, LPARAM lparam)
{
  // A key state has changed
  if (code == HC_ACTION)
  {
    // Get VK code
    KBDLLHOOKSTRUCT *key = reinterpret_cast<KBDLLHOOKSTRUCT*>(lparam);
    instance->KeyPressed(key, wparam == WM_KEYDOWN);
  }

  return instance->CallNextHook(code, wparam, lparam);
}

std::string bpm::input::Win32KeyWatcher::VKCodeToString(const KBDLLHOOKSTRUCT* key)
{
  // https://msdn.microsoft.com/en-us/library/ms646300%28v=VS.85%29.aspx
  // Apparently, there is no way to know the correct buffer size
  // MAX_PATH should be enough
  std::string name(MAX_PATH, '\0');

  // Black magic, read the doc to understand
  LONG code = 1 + (key->scanCode << 16) + (key->flags << 24);

  name.resize(GetKeyNameText(code, const_cast<char*>(name.data()), static_cast<int>(name.size())));

  return name;
}

void Win32KeyWatcher::KeyPressed(const KBDLLHOOKSTRUCT *code, bool pressed)
{
  // Logic:
  // 1) Wait for all key to be pressed at the same time
  // 2) Set '_allkeyspressed' to true when done
  // 3) Wait for at least one key to be released before exiting
  // (to make sure that the clipboard had the time to be pased)
  std::string keyname = VKCodeToString(code);

  // If key is released, remove it from the list of pressed keys
  if (!pressed)
  {
    if (IsKeyExpected(keyname))
    {
      if (_allkeyspressed)
      {
        UninstallHook();
      }
      _pressed_keys.erase(keyname);
    }
    return;
  }


  if (_printkeys)
  {
    std::cout << VKCodeToString(code) << std::endl;
  }

  // If the key is expected, add it to the pressed key set
  if (IsKeyExpected(keyname))
  {
    _pressed_keys.insert(keyname);

    // If all keys are in one set, then we can continue
    if (CheckForKeyGroup())
    {
      _allkeyspressed = true;
    }
  }
}

bool Win32KeyWatcher::CheckForKeyGroup()
{
  for (size_t i = 0; i < _expected_keys.size(); i++)
  {
    if (IsGroupComplete(_expected_keys[i]))
    {
      _key_group = i;
      return true;
    }
  }
  return false;
}

bool Win32KeyWatcher::IsGroupComplete(const std::set<std::string> &group) const
{
  // Check that each key in group is being pressed
  auto pred = [this](const auto &key)
  {
    return _pressed_keys.find(key) != _pressed_keys.end();
  };

  return std::all_of(group.begin(), group.end(), pred);
}

bool Win32KeyWatcher::IsKeyExpected(const std::string & keyname) const
{
  for (const auto &e : _expected_keys)
  {
    if (e.find(keyname) != e.end())
    {
      return true;
    }
  }

  return false;
}

void Win32KeyWatcher::Cancel()
{
  UninstallHook();
  throw CancelException();
}

void Win32KeyWatcher::UninstallHook()
{
  Syscall(UnhookWindowsHookEx, _hook);
  PostQuitMessage(0);
}


LRESULT Win32KeyWatcher::CallNextHook(int code, WPARAM wparam, LPARAM lparam) const
{
  return CallNextHookEx(_hook, code, wparam, lparam);
}

void Win32KeyWatcher::CancelAsyncImpl()
{
  // If thread_id is zero, then the main thread has already finished. No need to post a thread message
  if (_thread_id == 0) 
  {
    return;
  }
  
  // Let the thread exit from the GetMessage loop
  Syscall(PostThreadMessage, _thread_id, WM_QUIT, 0, 0);
}