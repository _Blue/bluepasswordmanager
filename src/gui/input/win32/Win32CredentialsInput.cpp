#include <windowsx.h>
#include "Win32CredentialsInput.h"
#include "input/InputMode.h"
#include "res/resource.h"
#include "BPMException.h"
#include "CancelException.h"
#include "Helper.h"
#include "input/win32/SyscallWrapper.h"

using bpm::input::Win32CredentialsInput;
using bpm::data::SecuredString;


Win32CredentialsInput::Win32CredentialsInput(const SecuredString&uri,
                                             const SecuredString& username,
                                             const SecuredString& password,
                                             bool symlink,
                                             const ConfigManager& config,
                                             const Win32CredentialsInput::SymlinkValidateRoutine& symlink_validate,
                                             bool allow_empty,
                                             HWND parent)
    : CredentialsInput(uri, username, password, symlink, config, symlink_validate), _allow_empty(allow_empty), _parent(parent)
{
}

bool Win32CredentialsInput::QueryCredentials()
{
  if (!DialogBoxParamA(nullptr, MAKEINTRESOURCE(IDD_ADDCRED), _parent, DlgProc, reinterpret_cast<LPARAM>(this)))
  {
    throw CancelException();
  }

  return !_display_list;
}

void Win32CredentialsInput::Init()
{
  Helper::CenterWindow(_window);
  Syscall(SetDlgItemTextA, _window, IDC_DNS, _domain.data());
  Syscall(SetDlgItemTextA, _window, IDC_USER, _user.data());
  Syscall(SetDlgItemTextA, _window, IDC_PASSWORD, _password.data());
  Edit_SetCueBannerText(GetDlgItem(_window, IDC_DNS), L"URI");
  Edit_SetCueBannerText(GetDlgItem(_window, IDC_PASSWORD), L"Password");

  if (_symlink)
  {
    Button_SetCheck(GetDlgItem(_window, IDC_SYMLINK), true);
  }

  // Enable top most window
  Syscall(SetWindowPos, _window, HWND_TOPMOST, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE);

  //Set OK as default button
  SendMessage(_window, DM_SETDEFID, IDOK, NULL);

  Update();

  // If "parent" is not null, it means that we come from a CredentialsDisplay window
  // In that case, the "Display list" button is hidden
  if (_parent != nullptr)
  {
    ShowWindow(Syscall(GetDlgItem, _window, IDC_SHOWLIST), SW_HIDE);
  }
}

INT_PTR CALLBACK Win32CredentialsInput::DlgProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
  if (msg == WM_INITDIALOG)
  {
    //Save the class pointer
    SetWindowLongPtrA(hwnd, GWLP_USERDATA, lparam);
    Win32CredentialsInput *instance = reinterpret_cast<Win32CredentialsInput*>(lparam);
    instance->_window = hwnd;

    instance->Init();
    return true;
  }

  Win32CredentialsInput *instance = reinterpret_cast<Win32CredentialsInput*>(GetWindowLongPtr(hwnd, GWLP_USERDATA));

  return instance->HandleEvent(msg, wparam, lparam);
}

INT_PTR Win32CredentialsInput::HandleEvent(UINT msg, WPARAM wparam, LPARAM lparam)
{
  if (msg == WM_CLOSE)
  {
    Cancel();
    return false;
  }

  if (msg == WM_COMMAND)
  {
    if (HIWORD(wparam) == EN_CHANGE || LOWORD(wparam) == IDC_SYMLINK)
    {
      Update();
      return false;
    }

    if (LOWORD(wparam) == IDOK)
    {
      OKPressed();
      return true;
    }
    else if (LOWORD(wparam) == IDC_CANCEL)
    {
      Cancel();
    }
    else if (LOWORD(wparam) == IDC_SHOWLIST)
    {
      DisplayList();
    }
    else if (LOWORD(wparam) == IDC_GENERATEPW)
    {
      GeneratePasswordPressed();
    }
  }

  return false;
}

std::string Win32CredentialsInput::GetControlText(size_t id)
{
  HWND control = GetDlgItem(_window, static_cast<int>(id));
  int len = GetWindowTextLengthA(control);
  if (len == -1)
  {
    throw BPMException("Failed to read control text length");
  }

  std::string text(len, '\0');
  if (GetWindowTextA(control, const_cast<char*>(text.data()), len + 1) != len)
  {
    throw BPMException("Failed to read control text");
  }

  return text;
}

void Win32CredentialsInput::OKPressed()
{
  _domain = Helper::GetDomainAuthority(GetControlText(IDC_DNS));
  _user = GetControlText(IDC_USER).c_str();
  _password = GetControlText(IDC_PASSWORD).c_str();

  EndDialog(_window, true);
}

void Win32CredentialsInput::Cancel()
{
  EndDialog(_window, false);
}

void Win32CredentialsInput::Update()
{
  bool validUri = false;
  try
  {
    auto dns = Helper::GetDomainAuthority(GetControlText(IDC_DNS));
    std::string text = "Domain name: " + dns;
    Syscall(SetDlgItemTextA, _window, IDC_ADDCRED_DOMAIN, text.data());
    validUri = true;
  }
  catch (const BPMException &)
  {
    Syscall(SetDlgItemTextA, _window, IDC_ADDCRED_DOMAIN, "Invalid URI");
    Button_Enable(GetDlgItem(_window, IDOK), false);
  }

  if (Button_GetCheck(GetDlgItem(_window, IDC_SYMLINK)))
  {
    Syscall(SetDlgItemTextA, _window, IDC_USER, defs::symlink_prefix);
    Edit_SetPasswordChar(GetDlgItem(_window, IDC_PASSWORD), '\0');
    Button_Enable(GetDlgItem(_window, IDC_USER), false);
    Button_Enable(GetDlgItem(_window, IDC_GENERATEPW), false);
    Edit_SetCueBannerText(GetDlgItem(_window, IDC_PASSWORD), L"Target domain");

    Button_Enable(GetDlgItem(_window, IDOK),
      GetWindowTextLength(GetDlgItem(_window, IDC_DNS)) > 0
      && _symlink_validate(SecuredString(GetControlText(IDC_PASSWORD))));
  }
  else
  {
    Edit_SetPasswordChar(GetDlgItem(_window, IDC_PASSWORD), '*');
    Button_Enable(GetDlgItem(_window, IDC_GENERATEPW), true);
    Edit_SetCueBannerText(GetDlgItem(_window, IDC_PASSWORD), L"Password");

    if (GetControlText(IDC_USER) == defs::symlink_prefix)
    {
      Syscall(SetDlgItemTextA, _window, IDC_USER, "");
    }

    Button_Enable(GetDlgItem(_window, IDC_USER), true);

    bool passwordok = _allow_empty || GetWindowTextLength(GetDlgItem(_window, IDC_PASSWORD)) > 0;

    Button_Enable(GetDlgItem(_window, IDOK),
      validUri
      && passwordok);
  }

  RedrawWindow(_window, nullptr, nullptr, RDW_ALLCHILDREN);
}

void Win32CredentialsInput::DisplayList()
{
  _display_list = true;
  Syscall(EndDialog, _window, true);
}

void Win32CredentialsInput::GeneratePasswordPressed()
{
  Syscall(SetWindowText, GetDlgItem(_window, IDC_PASSWORD), GeneratePassword().c_str());

  Edit_SetPasswordChar(GetDlgItem(_window, IDC_PASSWORD), '\0');
}