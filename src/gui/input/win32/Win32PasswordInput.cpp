#include <cassert>
#include "res/resource.h"
#include "Win32PasswordInput.h"
#include "CancelException.h"
#include "BPMException.h"
#include "Helper.h"
#include "input/win32/SyscallWrapper.h"

using bpm::input::Win32PasswordInput;
using bpm::input::PasswordInput;
using bpm::data::SecuredString;

Win32PasswordInput::Win32PasswordInput(const PasswordInput::CheckRoutine& routine, bool create)
  : PasswordInput(routine, create)
{
}

SecuredString Win32PasswordInput::QueryPassword(const std::string& storage)
{
  _storage = storage;
  if (!DialogBoxParam(nullptr, MAKEINTRESOURCE(IDD_QUERYPW), nullptr, DlgProc, reinterpret_cast<LPARAM>(this)))
  {
    throw CancelException();
  }

  return _password;
}
void bpm::input::Win32PasswordInput::HideConfirmPasswordField()
{ 
  Syscall(ShowWindow, Syscall(GetDlgItem, _window, IDC_PW_CONFIRM), SW_HIDE);

  RECT rect = { 0 };
  Syscall(GetWindowRect, Syscall(GetDlgItem, _window, IDC_PW_CONFIRM), &rect);

  size_t offset = rect.bottom - rect.top + 10;
  ShiftControl(IDOK, offset);
  ShiftControl(IDC_PW_NOTICE, offset);

  RECT window = { 0 };
  Syscall(GetWindowRect, _window, &window);
  //Syscall(AdjustWindowRectEx, &window, SyscallWithError(GetWindowLongPtr, 0, _window, GWL_STYLE), false, SyscallWithError(GetWindowLongPtr, 0, _window, GWL_EXSTYLE));

  Syscall(
    SetWindowPos,
    _window,
    nullptr,
    0,
    0,
    static_cast<int>(window.right - window.left),
    static_cast<int>(window.bottom - window.top - offset),
    SWP_NOMOVE | SWP_NOZORDER);
}

bool Win32PasswordInput::Init()
{
  // Store the instance pointer
  SetWindowLongPtrA(_window, GWLP_USERDATA, reinterpret_cast<LONG_PTR>(this));
  
  Helper::CenterWindow(_window);
  std::string text;
  if (!_create)
  {
    text = "Password required to unlock storage: \"";
    HideConfirmPasswordField();
  }
  else
  {
    text = "Create password for storage: \"";
  }
  // Set the notice text
  text += _storage + "\"\n";
  Syscall(SetDlgItemText, _window, IDC_PW_STORAGE, text.data());
  Syscall(SetFocus, Syscall(GetDlgItem, _window, IDC_PW_PASSWORD));
  return false;
}

void Win32PasswordInput::ShiftControl(size_t id, size_t size)
{
  ShiftControl(Syscall(GetDlgItem, _window, static_cast<int>(id)), size, true);
}

void Win32PasswordInput::ShiftControl(HWND handle, size_t size, bool map)
{
  RECT rect = { 0 };
  Syscall(GetWindowRect, handle, &rect);

  if (map)
  {
    Syscall(MapWindowPoints, HWND_DESKTOP, _window, reinterpret_cast<LPPOINT>(&rect), 2);
  }

  rect.bottom -= static_cast<LONG>(size);
  rect.top -= static_cast<LONG>(size);

  Syscall(SetWindowPos, handle, nullptr, rect.left, rect.top, 100, 100, SWP_NOSIZE | SWP_NOZORDER);
}

INT_PTR Win32PasswordInput::DlgProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam)
{
  if (msg == WM_INITDIALOG)
  {
    Win32PasswordInput *instance = reinterpret_cast<Win32PasswordInput*>(lparam);
    instance->_window = hwnd;
    instance->Init();
  }

  if (msg == WM_CLOSE || (msg == WM_COMMAND && LOWORD(wparam) == IDC_CANCEL))
  {
    Syscall(EndDialog, hwnd, false);
    return false;
  }

  if (msg == WM_COMMAND && LOWORD(wparam) == IDOK)
  {
    auto ptr = GetWindowLongPtrA(hwnd, GWLP_USERDATA);
    Win32PasswordInput *instance = reinterpret_cast<Win32PasswordInput*>(ptr);

    HWND edit = Syscall(GetDlgItem, hwnd, IDC_PW_PASSWORD);
    SecuredString password(GetWindowTextLengthA(edit), '\0');

    //The size argument of "GetWindowText" takes the '\0' into account, the return value does not
    if (GetWindowTextA(edit, const_cast<char*>(password.data()), static_cast<int>(password.size() + 1)) != password.size())
    {
      throw BPMException("Error while retrieving password");
    }

    //End dialog if password is correct
    if (instance->TryPassword(password))
    {
      Syscall(EndDialog, hwnd, true);
      return false;
    }

    // Show bad password notice
    if (!instance->_create)
    {
      Syscall(SetDlgItemText, hwnd, IDC_PW_NOTICE, "Failed to unlock storage");
    }
    else
    {
      Syscall(SetDlgItemTextA, hwnd, IDC_PW_NOTICE, "Password's don't match");
    }
    Syscall(ShowWindow, Syscall(GetDlgItem, hwnd, IDC_PW_NOTICE), SW_SHOW);
  }
  return false;
}

bool Win32PasswordInput::TryPassword(const SecuredString& password)
{
  if (_create)
  {
    HWND field = Syscall(GetDlgItem, _window, IDC_PW_CONFIRM);
    SecuredString confirm(GetWindowTextLength(field), '\0');

    SyscallWithExpectedReturn(GetWindowText, static_cast<int>(confirm.size()), field, const_cast<char*>(confirm.data()), static_cast<int>(confirm.size() + 1));

    if (confirm == password)
    {
      _password = password;
      return true;
    }
    return false;
  }
  if (_checkpassword(password))
  {
    _password = password;
    return true;
  }

  // Reset password field 
  Syscall(SetWindowText, GetDlgItem(_window, IDC_PW_PASSWORD), "");
  return false;
}