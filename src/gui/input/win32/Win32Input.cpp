#include "Win32Input.h"
#include "Win32CredentialsInput.h"
#include "Win32KeyWatcher.h"
#include "BPMException.h"
#include "CancelException.h"
#include "display/win32/Win32Banner.h"

using bpm::input::Win32Input;
using bpm::ConfigManager;
using bpm::input::CredentialsInput;
using bpm::display::Banner;
using bpm::display::Win32Banner;
using bpm::data::SecuredString;

Win32Input::Win32Input(ConfigManager& config, bpm::data::Storage& storage)
    : GUIInput(config, storage)
{
}

void Win32Input::SetData(const SecuredString& value)
{
  // Open clipboard
  if (!OpenClipboard(nullptr) || !EmptyClipboard())
  {
    throw BPMException("Failed to access to windows clipboard");
  }

  // Call GlobalAlloc
  HANDLE hmem;
  hmem = GlobalAlloc(GMEM_MOVEABLE | GMEM_DDESHARE, value.size() + 1);
  if (hmem == nullptr)
  {
    throw BPMException("GlobalAlloc failed (rare)");
  }

  // Write our buffer in the GlobalAlloc'd ptr
  char* ptr = reinterpret_cast<char*>(GlobalLock(hmem));
  std::memcpy(ptr, value.data(), value.size() + 1);
  GlobalUnlock(hmem);

  if (SetClipboardData(CF_TEXT, hmem) == nullptr || !CloseClipboard())
  {
    throw BPMException("Failed to write to windows clipboard");
  }

  // We would like to do: GlobalFree(hmem);
  // But now the memory belongs to the system, so don't free it
}

std::shared_ptr<CredentialsInput>
Win32Input::BuildCredentialsInput(const SecuredString& uri,
                                  const SecuredString& username) const
{
  auto ptr = std::make_shared<Win32CredentialsInput>(uri, username, "", false, _config, GetSymlinkValidateRoutine(_storage));

  return std::static_pointer_cast<CredentialsInput>(ptr);
}

std::string Win32Input::ReadClipboard()
{
  // Open clipboard (might fail if the clipboard has already been opened by a dialog window)
  // But in that case we can continue anyway
  OpenClipboard(nullptr);

  // Check for clipboard content
  if (!IsClipboardFormatAvailable(CF_TEXT))
  {
    return "";
  }

  // Read clipboard content
  HANDLE hmem = GetClipboardData(CF_TEXT);
  if (hmem == nullptr)
  {
    return "";
  }

  // Read content
  const char* str = reinterpret_cast<const char*>(hmem);

  if (str == nullptr || !GlobalUnlock(hmem) || !CloseClipboard())
  {
    throw BPMException("Error while reading clipboard content");
  }

  return str;
}

std::shared_ptr<Banner> Win32Input::BuildBanner() const
{
  return std::make_shared<Win32Banner>(_config);
}

std::shared_ptr<bpm::input::KeyWatcher> Win32Input::BuildKeyWatcher() const
{
  return std::make_shared<bpm::input::Win32KeyWatcher>();
}
