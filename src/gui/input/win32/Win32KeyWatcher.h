#pragma once

#include <set>
#include <string>
#include <vector>
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include "input/KeyWatcher.h"

namespace bpm
{
  namespace input
  {
    class Win32KeyWatcher: public KeyWatcher
    {
    public:
      Win32KeyWatcher() = default;


      size_t WaitForKeys(const std::vector<std::set<std::string>>& keys) override;
      void PrintPressedKeys() override;

      // This function returns the friendly name of the escape key
      // (depends on the system locale)
      static std::string GetEscapeKeyName();

    private:
      void InstallHook();
      void UninstallHook();
      void RunEventLoop();
      void Cancel();
      void InitState();
      void CancelAsyncImpl() override;

      void KeyPressed(const KBDLLHOOKSTRUCT *key, bool pressed);
      bool CheckForKeyGroup();
      bool IsGroupComplete(const std::set<std::string> &group) const;
      bool IsKeyExpected(const std::string &keyname) const;
      LRESULT CallNextHook(int code, WPARAM wparam, LPARAM lparam) const;

      static LRESULT CALLBACK KeyEvent(int code, WPARAM wparam, LPARAM lparam);
      static std::string VKCodeToString(const KBDLLHOOKSTRUCT *key);

      std::vector<std::set<std::string>> _expected_keys;
      std::set<std::string> _pressed_keys;
      HHOOK _hook = nullptr;
      bool _printkeys = false;
      bool _allkeyspressed = false;
      size_t _key_group = 0;
      DWORD _thread_id = 0;
    };
  }
}
