#pragma once


#include <Windows.h>
#include <CommCtrl.h>
#include "input/CredentialsInput.h"
namespace bpm
{
  namespace input
  {
    class Win32CredentialsInput : public CredentialsInput
    {
    public:
      Win32CredentialsInput(const data::SecuredString& uri,
                            const data::SecuredString& username,
                            const data::SecuredString& password,
                            bool symlink,
                            const ConfigManager& config,
                            const SymlinkValidateRoutine& symlink_validate,
                            bool allow_empty = false,
                            HWND parent = nullptr);

      virtual bool QueryCredentials() override;

    private:
      static INT_PTR CALLBACK DlgProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);
      std::string GetControlText(size_t id);
      INT_PTR HandleEvent(UINT msg, WPARAM wparam, LPARAM lparam);
      void Init();
      void OKPressed();
      void Cancel();
      void Update();
      void DisplayList();
      void GeneratePasswordPressed();

      HWND _window = nullptr;
      HWND _parent = nullptr;
      bool _allow_empty = false; // True to allow empty password (case of display)
      bool _display_list = false;
    };
  }
}
