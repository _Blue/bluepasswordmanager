#pragma once

#include <thread>
#include "input/GUIInput.h"

namespace bpm
{
  namespace input
  {
    class Win32Input : public GUIInput
    {
    public:
      Win32Input(ConfigManager& config, data::Storage& storage);
      ~Win32Input() = default;

      virtual void SetData(const data::SecuredString& value) override;

    protected:
      virtual std::shared_ptr<CredentialsInput>
      BuildCredentialsInput(const data::SecuredString& uri = "",
                            const data::SecuredString& username = "") const override;

      virtual std::shared_ptr<KeyWatcher> BuildKeyWatcher() const override;

      virtual std::shared_ptr<display::Banner> BuildBanner() const;

    private:
      std::string ReadClipboard();
    };
  }
}
