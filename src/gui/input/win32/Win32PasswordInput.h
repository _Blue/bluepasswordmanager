#include <Windows.h>
#include "input/PasswordInput.h"


namespace bpm
{
  namespace input
  {
    class Win32PasswordInput : public PasswordInput
    {
    public:
      Win32PasswordInput(const PasswordInput::CheckRoutine& routine, bool create);

      virtual data::SecuredString QueryPassword(const std::string& storage) override;

    private:
      static INT_PTR CALLBACK DlgProc(HWND hwnd, UINT msg, WPARAM wparam, LPARAM lparam);
      bool Init();
      bool TryPassword(const data::SecuredString& password);
      void ShiftControl(size_t id, size_t shift);
      void ShiftControl(HWND handle, size_t shift, bool map);
      void HideConfirmPasswordField();

      data::SecuredString _password;
      std::string _storage ;
      HWND _window = nullptr;
    };
  }
}
