#include "KeyWatcher.h"

using bpm::input::KeyWatcher;

void KeyWatcher::CancelAsync()
{
  _cancelled = true;
  CancelAsyncImpl();
}

bool KeyWatcher::Cancelled() const
{
  return _cancelled;
}

// Empty function instead of pure virtual
// avoid creating an empty function in XKeyWatcher
void KeyWatcher::CancelAsyncImpl()
{

}