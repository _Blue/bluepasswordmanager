#pragma once

#include <thread>
#include <memory>
#include "InputMode.h"
#include "CredentialsInput.h"
#include "KeyWatcher.h"

namespace bpm
{
  namespace input
  {
    class GUIInput : public InputMode
    {
    public:
      GUIInput(ConfigManager& config, data::Storage& storage);
      ~GUIInput() = default;

      // In case of login from bpm::display, we want to be able to 'force'
      // a specific URI, that's what this method does
      void SetRequestURI(const std::string& uri);
      virtual std::string GetRequestUrl() override;
      virtual bool InputCredentials(data::SecuredString& domain,
                                    data::SecuredString& user,
                                    data::SecuredString& password) override;
      virtual void SetData(const data::SecuredString& value) override = 0;

      virtual void CreateBackup() override;
      virtual void RestoreBackup() override;

      static CredentialsInput::SymlinkValidateRoutine
      GetSymlinkValidateRoutine(const data::Storage& storage);

    protected:
      virtual std::string ReadClipboard() = 0;
      virtual std::shared_ptr<CredentialsInput>
      BuildCredentialsInput(const data::SecuredString& uri,
                            const data::SecuredString& username) const = 0;
      virtual std::shared_ptr<KeyWatcher> BuildKeyWatcher() const = 0;
      std::string WaitForClipboardChange();
      void WaitForNextStep() const override;

    private:
      std::shared_ptr<KeyWatcher> StartExitKeyWait();
      void WaitForExitKeyImpl(std::shared_ptr<KeyWatcher> watcher);
      void CancelExitWait(std::shared_ptr<KeyWatcher> watcher);

      std::string _request_uri;
      std::string _original_data;
      bool _exit_key_pressed = false;
      std::thread _exit_thread;
    };
  }
}
