#include "CredentialsDisplay.h"
#include "BPMException.h"
#include "OSSpecific.h"
#include "Helper.h"

using bpm::ConfigManager;
using bpm::data::Storage;
using bpm::display::CredentialsDisplay;

CredentialsDisplay::CredentialsDisplay(ConfigManager& config, Storage& storage)
    : _config(config), _storage(storage)
{
}

void CredentialsDisplay::Show()
{
  BuildWindow();
  Refresh();
  RunEventLoop();

  if (_on_close)
  {
    _on_close();
  }
}

void CredentialsDisplay::Delete(const SecuredString& domain)
{
  _storage.DeleteCred(domain);
}

size_t CredentialsDisplay::Edit(const SecuredString& domain,
                                const SecuredString& username,
                                const SecuredString& password,
                                bool forcepw)
{
  size_t ts = Helper::Now();
  if (password.empty() &&
      (!_showpasswords || !forcepw)) // Ignore password change in this case
  {
    _storage.ChangeUsername(domain, username, ts);
  }
  else
  {
    _storage.AddCred(domain, username, password, ts, true);
  }

  return ts;
}

void CredentialsDisplay::SetShowPasswords(bool show)
{
  _showpasswords = show;
  Refresh();
}

void CredentialsDisplay::Refresh()
{
  CleanList();

  auto pred = [this](const SecuredString& domain,
                     const SecuredString& user,
                     const SecuredString& password,
                     size_t ts) {
    // Check for pattern match
    if (domain.find(_pattern.c_str()) == std::string::npos &&
        user.find(_pattern.c_str()) == std::string::npos &&
        (!_showpasswords ||
         password.find(_pattern.c_str()) == std::string::npos))
    {
      return true;
    }

    if (_showpasswords)
    {
      AddToList(domain, user, password, Helper::TimeString(ts));
    }
    else
    {
      AddToList(domain, user, "", Helper::TimeString(ts));
    }

    return true;
  };

  _storage.Enumerate(pred);
}

void CredentialsDisplay::SearchPattern(const std::string& pattern)
{
  _pattern = pattern;
  Refresh();
}

bool CredentialsDisplay::ChangeDomain(const SecuredString& olddomain,
                                      const SecuredString& newdomain,
                                      const std::function<bool()>& cb)
{
  try
  {
    _storage.ChangeDomain(
        olddomain, newdomain, Helper::Now()); // Can throw if target exists
    return true;
  }
  catch (const BPMException&)
  {
    if (cb()) // ask for user confirmation
    {
      _storage.ChangeDomain(olddomain, newdomain, Helper::Now());
      return true;
    }
  }

  return false;
}

void CredentialsDisplay::Add(const SecuredString& domain,
                             const SecuredString& username,
                             const SecuredString& password,
                             const std::function<bool()>& overwritecb)
{
  if (!_storage.AddCred(domain, username, password, Helper::Now(), false) &&
      overwritecb())
  {
    _storage.AddCred(domain, username, password, Helper::Now(), true);
  }
  Refresh();
}

void CredentialsDisplay::Login(const SecuredString& domain)
{
  // String reference may not be valid when called, so make a copy
  _on_close = [this, domain]() {
    OSSpecific::InputMode input(_config, _storage);

    input.SetRequestURI(domain.c_str());

    input.Start();
  };

  Close();
}
