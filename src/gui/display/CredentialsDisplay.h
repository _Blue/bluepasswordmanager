#pragma once

#include "data/Storage.h"
#include "ConfigManager.h"

namespace bpm
{
  namespace display
  {
    class CredentialsDisplay
    {
    public:
      using SecuredString = data::SecuredString;

      CredentialsDisplay(ConfigManager& config, data::Storage& storage);
      ~CredentialsDisplay() = default;
      CredentialsDisplay(const CredentialsDisplay&) = delete;
      CredentialsDisplay(CredentialsDisplay&&) = delete;

      CredentialsDisplay& operator=(const CredentialsDisplay&) = delete;
      CredentialsDisplay& operator=(CredentialsDisplay&&) = delete;

      void Show();

    protected:
      enum class Column
      {
        Domain = 0,
        Username,
        Password,
        Date
      };

      virtual void BuildWindow() = 0;
      virtual void RunEventLoop() = 0;
      virtual void CleanList() = 0;
      virtual void AddToList(const SecuredString& domain,
                             const SecuredString& username,
                             const SecuredString& password,
                             const std::string& date) = 0;

      void Delete(const SecuredString& domain);

      // This function is to be called when the user changes a value in the list
      // view
      // because the password may not be visible, if _showpassords is false
      // the `password` field will be ignored
      // Returns the new timestamp of the row
      size_t Edit(const SecuredString& domain,
                  const SecuredString& username,
                  const SecuredString& password,
                  bool forcepassword = false);

      void Add(const SecuredString& domain,
               const SecuredString& username,
               const SecuredString& password,
               const std::function<bool()>& overwritecb);

      // Change a domain, takes a callback in case of overwrite
      bool ChangeDomain(const SecuredString& olddomain,
                        const SecuredString& newdomain,
                        const std::function<bool()>& overwritecb);

      // Start login procedure on specified domain
      void Login(const SecuredString& domain);
      void SetShowPasswords(bool show);
      void Refresh();
      void SearchPattern(const std::string& pattern);
      virtual void Close() = 0;

      bool _showpasswords = false;
      std::function<void()> _on_close;
      ConfigManager& _config;
      data::Storage& _storage;

    private:
      std::string _pattern;
    };
  }
}
