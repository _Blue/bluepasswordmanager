#include "Banner.h"

using bpm::display::Banner;
using bpm::ConfigManager;

Banner::Banner(const ConfigManager& config) : _config(config)
{
}

void Banner::ShowAsync()
{
  // First, build window in this thread (avoids race condition)
  BuildWindow();
  ShowWindow();

  _visible = true;

  // Then handle event loop in separate thread
  _thread = std::thread(std::bind(&Banner::Work, this));
}

void Banner::Work()
{
  RunEventLoop();
}

void Banner::SetText(const std::string& text)
{
  _text = text;

  if (_visible)
  {
    SetTextImpl(text);
  }
}

Banner::~Banner()
{
  if (_thread.joinable())
  {
    _thread.join();
  }
}
