#include <cassert>
#include "Win32Banner.h"
#include "input/Win32/SyscallWrapper.h"
#include "res/resource.h"

using bpm::input::Syscall;
using bpm::input::SyscallWithExpectedReturn;
using bpm::display::Win32Banner;

Win32Banner::Win32Banner(const bpm::ConfigManager &config) : Banner(config)
{

}

Win32Banner::~Win32Banner()
{
  ExitEventLoop();
}

void Win32Banner::Hide()
{
  if (_window == nullptr)
  {
    return;
  }
  // Escape from local namespace, (because of bpm::display::Win32Banner::ShowWindow)
  ::ShowWindow(_window, SW_HIDE);
}

void Win32Banner::BuildWindow()
{
  _window = Syscall(CreateDialogParam, nullptr, MAKEINTRESOURCE(IDD_BANNER),
    nullptr, Callback, reinterpret_cast<LPARAM>(_text.data()));

  AdjustWindow(_text);
}

void Win32Banner::ShowWindow()
{
  if (_window == nullptr)
  {
    return;
  }
  ::ShowWindow(_window, SW_SHOW);
}

void Win32Banner::SetTextImpl(const std::string &text)
{
  assert(_window != nullptr);

  AdjustWindow(text);
  Syscall(SetDlgItemText, _window, IDC_BANNER_LABEL, text.data());
}

void Win32Banner::AdjustWindow(const std::string& text)
{
  assert(_window != nullptr);
  HWND label = Syscall(GetDlgItem, _window, IDC_BANNER_LABEL);

  // Get label dimensions
  RECT labelrect = { 0 };
  Syscall(GetWindowRect, label, &labelrect);
  MapWindowPoints(HWND_DESKTOP, _window, reinterpret_cast<POINT*>(&labelrect), 2);

  // Compute label margin
  size_t xmargin = labelrect.left;
  size_t ymargin = labelrect.top;

  // Compute text size in pixels
  size_t width = GetTextWidth(text);
  size_t height = labelrect.bottom - labelrect.top;

  width += xmargin * 2;
  height += ymargin * 2;

  RECT screen = { 0 };

  Syscall(SystemParametersInfo, SPI_GETWORKAREA, 0, &screen, 0);

  // Compute window position
  size_t x = (screen.right - screen.left) - width;
  size_t y = (screen.bottom - screen.top) - height;

  // Apply config offsets
  x -= _config.GetBannerOffset().first;
  y -= _config.GetBannerOffset().second;

  // Apply move and resize
  Syscall(SetWindowPos, label, nullptr, 0, 0, static_cast<int>(width - xmargin), static_cast<int>(height - ymargin), SWP_NOZORDER | SWP_NOMOVE);
  Syscall(SetWindowPos, _window, HWND_TOPMOST, static_cast<int>(x), static_cast<int>(y), static_cast<int>(width), static_cast<int>(height), 0);
}

size_t Win32Banner::GetTextWidth(const std::string &text) const
{
  HWND label = Syscall(GetDlgItem, _window, IDC_BANNER_LABEL);
  
  // Get Draw context
  HDC dc = Syscall(GetDC, _window);

  // Get label font
  HFONT hfont = reinterpret_cast<HFONT>(SendMessage(label, WM_GETFONT, 0, 0));
  assert(hfont != nullptr);

  RECT rect = { 0 };
  rect.right = 1000;
  rect.bottom = 20;
  Syscall(DrawText, dc, text.data(), -1, &rect, DT_CALCRECT);

  return rect.right - rect.left;
}

void Win32Banner::ExitEventLoop()
{
  //Because this function will be called by another thread than
  // the thread running the EventLoop, PostQuitMessage cannot be used
  // as it would post the message on the wrong thread event queue

  if (_event_thread_id != 0)
  {
    Syscall(PostThreadMessage, _event_thread_id, WM_QUIT, 0, 0);
  }
}

void Win32Banner::RunEventLoop()
{
  _event_thread_id = GetCurrentThreadId();
  MSG message;
  while (GetMessageA(&message, nullptr, 0, 0))
  {
    TranslateMessage(&message);
    DispatchMessageA(&message);
  }
}

INT_PTR Win32Banner::Callback(HWND window, UINT msg, WPARAM wparam, LPARAM lparam)
{
  if (msg == WM_INITDIALOG)
  {
    // Set label text (passed as LPARAM)
    Syscall(SetDlgItemText, window, IDC_BANNER_LABEL, reinterpret_cast<char*>(lparam));

    return true;
  }
  return false;
}
