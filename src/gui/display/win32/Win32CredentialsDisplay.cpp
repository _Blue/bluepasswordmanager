#include <cassert>

#include "res/resource.h"
#include "Win32CredentialsDisplay.h"
#include "input/GUIInput.h"
#include "input/win32/Win32CredentialsInput.h"
#include "BPMException.h"
#include "CancelException.h"
#include "Helper.h"
#include "input/Win32/SyscallWrapper.h"
#include <windowsx.h> // Cannot be included before "windows.h"

// Use recent CommCtrl version
#pragma comment(linker,"\"/manifestdependency:type='win32' name='Microsoft.Windows.Common-Controls' version='6.0.0.0' processorArchitecture='*' publicKeyToken='6595b64144ccf1df' language='*'\"")

using namespace std::placeholders;
using bpm::display::Win32CredentialsDisplay;
using bpm::input::Syscall;
using bpm::ConfigManager;
using bpm::data::Storage;
using bpm::data::SecuredString;


Win32CredentialsDisplay::Win32CredentialsDisplay(ConfigManager &config, Storage &storage)
  : CredentialsDisplay(config, storage)
{
  InitHandlers();
  InitContextMenu();
}

void Win32CredentialsDisplay::BuildWindow()
{
  _window = CreateDialogParamA(nullptr,
                               MAKEINTRESOURCE(IDD_DISPLAY),
                               nullptr,
                               Callback,
                               reinterpret_cast<LPARAM>(this));
  InitControlInfos();
}

void Win32CredentialsDisplay::InitHandlers()
{
  _handlers.emplace(WM_CLOSE, std::bind(&Win32CredentialsDisplay::WindowClosed, this, _1, _2));
  _handlers.emplace(WM_NOTIFY, std::bind(&Win32CredentialsDisplay::Notify, this, _1, _2));
  _handlers.emplace(WM_COMMAND, std::bind(&Win32CredentialsDisplay::Command, this, _1, _2));
  _handlers.emplace(WM_CONTEXTMENU, std::bind(&Win32CredentialsDisplay::ShowMenu, this, _1, _2));
  _handlers.emplace(WM_SIZING, std::bind(&Win32CredentialsDisplay::WindowResized, this, _1, _2));
  _notifyhandlers.emplace(NM_DBLCLK, std::bind(&Win32CredentialsDisplay::RequestEdit, this, _1));
}

void Win32CredentialsDisplay::InitContextMenu()
{
  _contextmenu = Syscall(GetSubMenu, Syscall(LoadMenu, nullptr, MAKEINTRESOURCE(IDM_DISPLAY)), 0);
}

// Save controls position and size to allow window resize
void Win32CredentialsDisplay::InitControlInfos()
{
  // Compute window dimensions
  RECT rect;
  GetWindowRect(_window, &rect);

  _window_width = rect.right - rect.left;
  _window_height = rect.bottom - rect.top;

  SavePosInfos(GetDlgItem(_window, IDC_DISPLAY_LIST));
  SavePosInfos(GetDlgItem(_window, IDC_DISPLAY_PATTERN));
  SavePosInfos(GetDlgItem(_window, IDC_DISPLAY_SHOWPW));
  SavePosInfos(GetDlgItem(_window, IDC_DISPLAY_ADD));
}

void Win32CredentialsDisplay::SavePosInfos(HWND item)
{
  RECT rect = { 0 };

  // Get absolute rect
  Syscall(GetWindowRect, item, &rect);

  //Map rectangle to window
  Syscall(MapWindowPoints, HWND_DESKTOP, _window, reinterpret_cast<POINT*>(&rect), 2);

  _posinfos[item] = rect;

  // Save distance to bottom and right of the window
  _posinfos[item].right = static_cast<LONG>(_window_width - _posinfos[item].right);
  _posinfos[item].bottom = static_cast<LONG>(_window_height - _posinfos[item].bottom);
}

void Win32CredentialsDisplay::RunEventLoop()
{
  ShowWindow(_window, SW_SHOW);

  MSG message = { 0 };
  while (GetMessageA(&message, nullptr, 0, 0))
  {
    TranslateMessage(&message);
    DispatchMessageA(&message);
  }
}

// This function must take the hwnd as parameter
// because when called, the _window field of the class
// is still not set
void Win32CredentialsDisplay::InitDialog(HWND window)
{
  HWND listview = Syscall(GetDlgItem, window, IDC_DISPLAY_LIST);

  // Set list view flags
  DWORD style = ListView_GetExtendedListViewStyle(listview);
  ListView_SetExtendedListViewStyle(listview, style | LVS_EX_FULLROWSELECT);

  // Add columns to list view
  AddColumn(listview, "Domain", 0);
  AddColumn(listview, "Username", 1);
  AddColumn(listview, "Password", 2);
  AddColumn(listview, "Edited", 3);

  // Set search text placeholder text
  Edit_SetCueBannerText(GetDlgItem(window, IDC_DISPLAY_PATTERN), L"Filter");

  // Move the window in the center
  Helper::CenterWindow(window);
}

INT_PTR CALLBACK Win32CredentialsDisplay::Callback(HWND window, UINT msg, WPARAM wparam, LPARAM lparam)
{
  if (msg == WM_INITDIALOG)
  {
    // Store the instance pointer
    SetWindowLongPtrA(window, GWLP_USERDATA, lparam);

    Win32CredentialsDisplay *instance = reinterpret_cast<Win32CredentialsDisplay*>(lparam);
    instance->InitDialog(window);
    return true;
  }

  Win32CredentialsDisplay *instance = reinterpret_cast<Win32CredentialsDisplay*>(
    GetWindowLongPtr(window, GWLP_USERDATA));

  if (instance == nullptr) // If dialog is not initialised yes
  {
    return false;
  }
  return instance->Event(msg, wparam, lparam);
}

void Win32CredentialsDisplay::AddColumn(HWND listview, const std::string &name, size_t pos)
{
  LVCOLUMNA column = { 0 };

  column.mask = LVCF_TEXT | LVCF_WIDTH | LVCF_SUBITEM;
  column.cchTextMax = static_cast<int>(name.size() + 1);
  column.pszText = const_cast<char*>(name.data());
  column.cx = 120;
  ListView_InsertColumn(listview, pos, &column);
}

int Win32CredentialsDisplay::Event(UINT msg, WPARAM wparam, LPARAM lparam)
{
  // Search for a handler for this event
  const auto &handler = _handlers.find(msg);

  if (handler == _handlers.end())
  {
    // If not found, return default value
    return false;
  }

  return handler->second(wparam, lparam);
}

int Win32CredentialsDisplay::Notify(WPARAM, LPARAM lparam)
{
  NMHDR *header = reinterpret_cast<NMHDR*>(lparam);
  const auto &handler = _notifyhandlers.find(header->code);

  if (handler == _notifyhandlers.end())
  {
    return false;
  }

  return handler->second(header);
}

int Win32CredentialsDisplay::WindowClosed(WPARAM, LPARAM)
{
  Close();
  return true;
}

int Win32CredentialsDisplay::Command(WPARAM wparam, LPARAM lparam)
{
  if (HIWORD(wparam) == EN_CHANGE && LOWORD(wparam) == IDC_DISPLAY_PATTERN) // Pattern control changed
  {
    HWND edit = Syscall(GetDlgItem, _window, IDC_DISPLAY_PATTERN);
    std::string pattern;
    pattern.resize(GetWindowTextLength(edit));

    if (GetWindowText(edit, const_cast<char*>(pattern.data()), static_cast<int>(pattern.size() + 1)) != pattern.size())
    {
      throw BPMException("GetWindowText failed");
    }

    SearchPattern(pattern);
  }
  else if (LOWORD(wparam) == IDC_DISPLAY_SHOWPW)// "Show passwords" is checked
  {
    SetShowPasswords(Button_GetCheck(GetDlgItem(_window, IDC_DISPLAY_SHOWPW)) == TRUE);
  }
  else if (LOWORD(wparam) == MENU_EDIT)
  {
    StartEdit();
  }
  else if (LOWORD(wparam) == MENU_DELETE)
  {
    RemoveRow();
  }
  else if (LOWORD(wparam) == IDC_DISPLAY_ADD)
  {
    AddCredentials({});
  }
  else if (LOWORD(wparam) == MENU_CREATE_SYMLINK)
  {
    CreateSymlink();
  }
  else if (LOWORD(wparam) == MENU_LOGIN)
  {
    int row = SelectedRow();
    if (row != -1)
    {
      Login(ReadCell(row, Column::Domain));
    }
  }

  return false;
}

/* Here comes some black magic
Because the standart listview item allows us to
modify items and not sub items,
we have to subclass this method and check which
column is begin edited based on the click position
*/
int Win32CredentialsDisplay::RequestEdit(NMHDR *hdr)
{
  HWND listview = Syscall(GetDlgItem, _window, IDC_DISPLAY_LIST);
  if (hdr->hwndFrom != listview)
  {
    return false; // Click does not come from listview, don't handle
  }

  StartEdit();
  return true;
}

void Win32CredentialsDisplay::CreateSymlink()
{
  HWND listview = Syscall(GetDlgItem, _window, IDC_DISPLAY_LIST);

  // Get select row
  int row = ListView_GetNextItem(listview, -1, LVNI_SELECTED);

  if (row == -1) // abort if nothing is selected
  {
    return;
  }

  AddCredentials(ReadCell(row, Column::Domain));
}

void Win32CredentialsDisplay::StartEdit()
{
  HWND listview = Syscall(GetDlgItem, _window, IDC_DISPLAY_LIST);

  // Get select row
  int row = ListView_GetNextItem(listview, -1, LVNI_SELECTED);

  if (row == -1) // abort if nothing is selected
  {
    return;
  }

  auto domain = ReadCell(row, Column::Domain);
  auto username = ReadCell(row, Column::Username);

  // Display sub-dialog
  input::Win32CredentialsInput input(domain, username, "", false, _config, input::GUIInput::GetSymlinkValidateRoutine(_storage), true, _window);

  try
  {
    input.QueryCredentials();
  }
  catch (const CancelException&)
  {
    return;
  }

  // Apply changes
  if (input.GetDomain() != domain)
  {

    if (!ChangeDomain(domain, input.GetDomain(), GetOverwriteCallback(input.GetDomain())))
    {
      return;
    }
  }
  Edit(input.GetDomain(), input.GetUserName(), input.GetPassword(), !input.GetPassword().empty());
  Refresh();
}

std::function<bool()> Win32CredentialsDisplay::GetOverwriteCallback(const SecuredString &domain) const
{
  std::string message;
  if (domain.empty())
  {
    message = "An entry already exists for that domain, overwrite ?";
  }
  else
  {
    message = "An entry for domain \"" + std::string(domain.c_str()) + "\" already exists, overwrite ?";
  }
  return std::bind(&Win32CredentialsDisplay::Confirm, this, message);
}

// This is probably the worst function of the whole project
// because there is, as far as I now, no way to know the correct
// buffer size to allocate, we have to loop, multiplying our buffer
// size by two until we have a sufficient size
//
// Because the message's return value is the number of characters copied
// into our buffer, we loop until the return value is less than the buffer
// size
SecuredString Win32CredentialsDisplay::ReadCell(size_t line, Column column)
{
  HWND listview = Syscall(GetDlgItem, _window, IDC_DISPLAY_LIST);
  size_t copied; // Will hold the character copied in our buffer

  size_t col = static_cast<size_t>(column);

  size_t size = 8;
  SecuredString text;

  LVITEM item = { 0 };
  item.iSubItem = static_cast<int>(col);
  item.iItem = static_cast<int>(line);

  do
  {
    size *= 2;
    item.cchTextMax = static_cast<int>(size);
    text.resize(size);
    item.pszText = const_cast<char*>(text.data());

    copied = SendMessage(listview, LVM_GETITEMTEXT, line, reinterpret_cast<LPARAM>(&item));

    if (copied < 0)
    {
      throw BPMException("Failed to retrieve text of list view cell");
    }
  } while (copied >= size - 1);

  text.resize(copied);
  assert(text.size() == strlen(text.data()));
  
  return text;
}

void Win32CredentialsDisplay::CleanList()
{
  ListView_DeleteAllItems(GetDlgItem(_window, IDC_DISPLAY_LIST));
}

void Win32CredentialsDisplay::InsertLine(const std::vector<SecuredString>& values)
{
  assert(values.size() == 4);

  HWND listview = Syscall(GetDlgItem, _window, IDC_DISPLAY_LIST);
  
  LVITEMA row = { 0 };
  row.cchTextMax = static_cast<int>(values[0].size() + 1);
  row.mask = LVIF_TEXT;
  row.pszText = const_cast<char*>(values[0].data());
  ListView_InsertItem(listview, &row);

  for (size_t i = 1; i < values.size(); i++)
  {
    row.cchTextMax = static_cast<int>(values[i].size() + 1);
    row.pszText = const_cast<char*>(values[i].data());
    row.iSubItem++;
    ListView_SetItem(listview, &row);
  }
}

void Win32CredentialsDisplay::AddToList(const SecuredString& domain,
                                        const SecuredString& username,
                                        const SecuredString& password,
                                        const std::string& date)
{
  InsertLine({ domain, username, password, date.c_str() });
}

bool Win32CredentialsDisplay::Confirm(const std::string &text) const
{
  return MessageBox(_window, text.data(), "BPM", MB_YESNO) == IDYES;
}

int Win32CredentialsDisplay::ShowMenu(WPARAM wparam, LPARAM lparam)
{
  HWND listview = Syscall(GetDlgItem, _window, IDC_DISPLAY_LIST);
  // Check that right click is on the list view
  if (reinterpret_cast<HANDLE>(wparam) != listview)
  {
    return false;
  }

  // Check that a row is selected
  if (ListView_GetNextItem(listview, -1, LVNI_SELECTED) == -1)
  {
    return false;
  }

  int x = GET_X_LPARAM(lparam);
  int y = GET_Y_LPARAM(lparam);

  Syscall(TrackPopupMenu, _contextmenu, TPM_TOPALIGN | TPM_LEFTALIGN, x, y, 0, _window, nullptr);
  return true;
}

int Win32CredentialsDisplay::SelectedRow()
{
  HWND listview = Syscall(GetDlgItem, _window, IDC_DISPLAY_LIST);
  return ListView_GetNextItem(listview, -1, LVNI_SELECTED);
}

void Win32CredentialsDisplay::RemoveRow()
{
  // Get selected row
  int row = SelectedRow();

  // Check that selection is not empty
  if (row == -1)
  {
    return;
  }

  // Read domain
  auto domain = ReadCell(row, Column::Domain);

  std::string message = "Delete credentials for domain: \"" + std::string(domain.c_str()) + "\"?";

  // Ask for confirmation
  if (Confirm(message))
  {
    // Perform actual deletion
    Delete(domain);
    Refresh();
  }
}

int Win32CredentialsDisplay::WindowResized(WPARAM wparam, LPARAM lparam)
{
  RECT *rect = reinterpret_cast<RECT*>(lparam);

  size_t width = rect->right - rect->left;
  size_t height = rect->bottom - rect->top;

  // Compute listview size
  HWND listview = Syscall(GetDlgItem, _window, IDC_DISPLAY_LIST);
  size_t w = width - _posinfos[listview].right - _posinfos[listview].left;
  size_t h = height - _posinfos[listview].bottom - _posinfos[listview].top;
  Syscall(SetWindowPos, listview, HWND_TOP, 0, 0, static_cast<int>(w), static_cast<int>(h), SWP_NOMOVE | SWP_FRAMECHANGED);

  BottomPad(Syscall(GetDlgItem, _window, IDC_DISPLAY_PATTERN), width, height, true);
  BottomPad(Syscall(GetDlgItem, _window, IDC_DISPLAY_ADD), width, height, true);
  BottomPad(Syscall(GetDlgItem, _window, IDC_DISPLAY_SHOWPW), width, height, false);

  return true;
}

void Win32CredentialsDisplay::BottomPad(HWND control, size_t width, size_t height, bool left)
{
  RECT rect = { 0 };
  Syscall(GetWindowRect, control, &rect);

  int y = static_cast<int>(height - _posinfos[control].bottom);
  y -= rect.bottom - rect.top;
  int x;

  if (left)
  {
    x = _posinfos[control].left;
  }
  else
  {
    x = static_cast<int>(width) - _posinfos[control].right;
    x -= rect.right - rect.left;
  }

  Syscall(SetWindowPos, control, HWND_TOP, x, y, 0, 0, SWP_NOSIZE | SWP_FRAMECHANGED);
}

void Win32CredentialsDisplay::AddCredentials(const std::optional<SecuredString>& symlinkDomain)
{
  // Display sub-dialog
  input::Win32CredentialsInput input("", "", symlinkDomain.value_or(""), symlinkDomain.has_value(), _config, input::GUIInput::GetSymlinkValidateRoutine(_storage), false, _window);

  try
  {
    if (!input.QueryCredentials())
    {
      return; // Reached if "Display list" button has been clicked
    }
  }
  catch (const CancelException&)
  {
    return;
  }

  auto routine = GetOverwriteCallback();
  Add(input.GetDomain(), input.GetUserName(), input.GetPassword(), routine);
}

void Win32CredentialsDisplay::Close()
{
  Syscall(DestroyWindow, _window);
  PostQuitMessage(0);
}
