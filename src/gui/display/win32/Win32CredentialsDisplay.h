#pragma once

#include <Windows.h>
#include <CommCtrl.h>
#include <map>
#include <functional>
#include <optional>
#include "display/CredentialsDisplay.h"

namespace bpm
{
  namespace display
  {
    class Win32CredentialsDisplay : public CredentialsDisplay
    {
    public:
      Win32CredentialsDisplay(ConfigManager& config, data::Storage& storage);

    protected:
      virtual void BuildWindow() override;
      virtual void RunEventLoop() override;
      virtual void CleanList() override;
      virtual void AddToList(const data::SecuredString& domain,
                             const data::SecuredString& username,
                             const data::SecuredString& password,
                             const std::string& date) override;
      virtual void Close() override;

    private:
      using eventhandler = std::function<int(WPARAM, LPARAM)>;
      using notifyhandler = std::function<int(NMHDR*)>;

      int Event(UINT msg, WPARAM wparam, LPARAM lwparam);
      int Command(WPARAM wparam, LPARAM lparam);
      int Notify(WPARAM wparam, LPARAM lparam);
      int RequestEdit(NMHDR* data);
      int ShowMenu(WPARAM wparam, LPARAM lparam);
      int WindowClosed(WPARAM wparam, LPARAM lparam);
      int WindowResized(WPARAM wparam, LPARAM lparam);
      int SelectedRow();
      void StartEdit();
      void RemoveRow();
      void InitHandlers();
      void InitDialog(HWND window);
      void InitContextMenu();
      void InitControlInfos();
      void SavePosInfos(HWND control);
      void BottomPad(HWND control, size_t width, size_t height, bool left);
      void AddCredentials(const std::optional<data::SecuredString>& symlinkDomain);
      void CreateSymlink();
      void InsertLine(const std::vector<data::SecuredString>& values);
      bool Confirm(const std::string& text) const;
      data::SecuredString ReadCell(size_t line, Column column);
      std::function<bool()>
      GetOverwriteCallback(const data::SecuredString& domain = "") const;
      
      static INT_PTR CALLBACK Callback(HWND window, UINT msg, WPARAM wparam,
                                       LPARAM lparam);
      static void AddColumn(HWND listview, const std::string& name, size_t pos);

      HWND _window = nullptr;
      HMENU _contextmenu = nullptr;
      size_t _window_width = 0;
      size_t _window_height = 0;
      std::map<int, eventhandler> _handlers;
      std::map<int, notifyhandler> _notifyhandlers;
      std::map<HWND, RECT> _posinfos;
    };
  }
}
