#pragma once

#include <Windows.h>
#include "display/Banner.h"

namespace bpm
{
  namespace display
  {
    class Win32Banner: public Banner
    {
    public:
      Win32Banner(const ConfigManager &config);
      virtual ~Win32Banner();

      virtual void Hide() override;

    protected:
      virtual void BuildWindow() override;
      virtual void ShowWindow() override;
      virtual void RunEventLoop() override;
      virtual void ExitEventLoop() override;
      virtual void SetTextImpl(const std::string &text) override;

    private:
      size_t GetTextWidth(const std::string &text) const;
      void AdjustWindow(const std::string &text);

      static INT_PTR CALLBACK Callback(HWND window, UINT msg, WPARAM wparam, LPARAM lparam);

      HWND _window = nullptr;
      DWORD _event_thread_id = 0;
    };
  }
}