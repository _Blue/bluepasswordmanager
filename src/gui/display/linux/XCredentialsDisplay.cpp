#include <functional>
#include <cstring>
#include <gtk/gtk.h>

#include "XCredentialsDisplay.h"
#include "CancelException.h"
#include "BPMException.h"
#include "Helper.h"
#include "input/linux/XCredentialsInput.h"
#include "input/GUIInput.h"

using bpm::data::SecuredString;
using bpm::display::XCredentialsDisplay;
using bpm::input::GTKHandler;

XCredentialsDisplay::XCredentialsDisplay(ConfigManager& config,
                                         data::Storage& storage)
    : CredentialsDisplay(config, storage)
{
}

void XCredentialsDisplay::BuildWindow()
{
  std::function<void()> build =
      std::bind(&XCredentialsDisplay::BuildWindowImpl, this);

  GTKHandler::RunInGtkContext(build);
}

void XCredentialsDisplay::Build()
{
}

void XCredentialsDisplay::RunEventLoop()
{
  XWindow::Display();
}

void XCredentialsDisplay::AddColumn(
    const char* name, Column col, bool editable, bool password, GtkWidget* view)
{
  GtkCellRenderer* renderer = gtk_cell_renderer_text_new();
  gtk_tree_view_insert_column_with_attributes(GTK_TREE_VIEW(view),
                                              -1,
                                              name,
                                              renderer,
                                              "text",
                                              static_cast<size_t>(col),
                                              nullptr);

  // Set the column number that we can recover in inside the callback
  g_object_set_data(G_OBJECT(renderer), "bpm_column", GUINT_TO_POINTER(col));

  if (editable)
  {
    g_object_set(renderer, "editable", true, nullptr); // Make the view editable
    g_signal_connect(
        renderer, "edited", G_CALLBACK(XCredentialsDisplay::Modify), this);

    if (password)
    {
      g_signal_connect(renderer,
                       "editing-started",
                       G_CALLBACK(XCredentialsDisplay::BeginEdit),
                       this);
    }
  }
}

void XCredentialsDisplay::BuildWindowImpl()
{
  _store = gtk_list_store_new(
      4, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING, G_TYPE_STRING);

  // Display content in scroll view
  GtkWidget* scrollview = gtk_scrolled_window_new(nullptr, nullptr);
  gtk_scrolled_window_set_min_content_width(GTK_SCROLLED_WINDOW(scrollview),
                                            450);
  gtk_scrolled_window_set_min_content_height(GTK_SCROLLED_WINDOW(scrollview),
                                             200);
  gtk_widget_set_vexpand(scrollview, true); // Expand scroll view vertically

  // Create list view
  _listview = gtk_tree_view_new();

  GtkTreeSelection* sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(_listview));
  gtk_tree_selection_set_mode(sel, GTK_SELECTION_BROWSE);

  // Create "add entry" button
  GtkWidget* add_entry = gtk_button_new_with_label("Add entry");

  // Create search line edit
  GtkWidget* search_line = gtk_entry_new();
  gtk_entry_set_placeholder_text(GTK_ENTRY(search_line), "Search");

  // Create 'show passwords' check box
  GtkWidget* show_passwords = gtk_check_button_new_with_label("Show passwords");

  // Add columns
  AddColumn("Domain", Column::Domain, true, false, _listview);
  AddColumn("Username", Column::Username, true, false, _listview);
  AddColumn("Password", Column::Password, true, true, _listview);
  AddColumn("Edited", Column::Date, false, false, _listview);

  // Assign model
  gtk_tree_view_set_model(GTK_TREE_VIEW(_listview), GTK_TREE_MODEL(_store));

  // Create vertical container
  GtkWidget* vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, 10);

  // Create horizontal container
  GtkWidget* hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 10);

  // Create window
  _window = gtk_window_new(GTK_WINDOW_TOPLEVEL);

  // Fill containers
  gtk_container_add(GTK_CONTAINER(scrollview), _listview);
  gtk_container_add(GTK_CONTAINER(vbox), scrollview);
  gtk_container_add(GTK_CONTAINER(vbox), hbox);
  gtk_container_add(GTK_CONTAINER(hbox), search_line);
  gtk_container_add(GTK_CONTAINER(hbox), add_entry);
  gtk_container_add(GTK_CONTAINER(hbox), show_passwords);
  gtk_container_add(GTK_CONTAINER(_window), vbox);

  // Set check box at right side of window
  gtk_box_set_child_packing(
      GTK_BOX(hbox), show_passwords, false, false, 10, GTK_PACK_END);

  gtk_box_set_child_packing(
      GTK_BOX(vbox), _listview, true, true, 0, GTK_PACK_START);
  gtk_box_set_child_packing(GTK_BOX(vbox), hbox, false, false, 0, GTK_PACK_END);

  // Connect signals
  g_signal_connect(_window, "delete_event", G_CALLBACK(WindowClosed), this);
  g_signal_connect(
      _listview, "button-press-event", G_CALLBACK(RightClicked), this);
  g_signal_connect(search_line, "changed", G_CALLBACK(SearchEdited), this);
  g_signal_connect(
      show_passwords, "toggled", G_CALLBACK(ShowPasswordsClicked), this);

  g_signal_connect(add_entry, "clicked", G_CALLBACK(AddEntryClicked), this);

  gtk_widget_grab_focus(search_line);

  gtk_widget_show_all(_window);
}

void XCredentialsDisplay::CleanList()
{
  gtk_list_store_clear(_store);
}

void XCredentialsDisplay::ShowAddCredentialsDialog(
    const std::optional<data::SecuredString>& symlink_domain)
{
  _on_close = {}; // Prevent infinite dialog recursion

  input::XCredentialsInput input(
      "",
      "",
      symlink_domain.value_or(""),
      symlink_domain.has_value(),
      _config,
      bpm::input::GUIInput::GetSymlinkValidateRoutine(_storage));

  try
  {
    if (!input.QueryCredentials())
    {
      Show();
      return;
    }
  }
  catch (const CancelException&)
  {
    Show();
    return;
  }

  std::function<bool()> callback = [this, &input]() {
    auto domain = input.GetDomain();
    auto* dialog = gtk_message_dialog_new(
        nullptr,
        GTK_DIALOG_DESTROY_WITH_PARENT,
        GTK_MESSAGE_QUESTION,
        GTK_BUTTONS_YES_NO,
        "An entry for domain\"%s\" already exists. Overwrite ?",
        domain.c_str());

    auto result = gtk_dialog_run(GTK_DIALOG(dialog));
    gtk_widget_destroy(dialog);

    return result == GTK_RESPONSE_YES;
  };

  Add(input.GetDomain(), input.GetUserName(), input.GetPassword(), callback);
  Show();
}

bool XCredentialsDisplay::AddEntryClicked(GtkWidget*, XCredentialsDisplay* ptr)
{
  ptr->_on_close = [ptr]() { ptr->ShowAddCredentialsDialog({}); };

  WindowClosed(nullptr, nullptr, ptr);

  return true;
}

void XCredentialsDisplay::AddToList(const SecuredString& domain,
                                    const SecuredString& username,
                                    const SecuredString& password,
                                    const std::string& date)
{
  GtkTreeIter it;
  gtk_list_store_append(_store, &it);
  gtk_list_store_set(_store,
                     &it,
                     Column::Domain,
                     domain.data(),
                     Column::Username,
                     username.data(),
                     Column::Password,
                     password.data(),
                     Column::Date,
                     date.data(),
                     -1);
}

// Called when the user changes data on the list view
void XCredentialsDisplay::Modify(GtkCellRendererText* cell,
                                 char* path,
                                 char* text,
                                 XCredentialsDisplay* instance)
{
  instance->OnModify(cell, path, text);
}

void XCredentialsDisplay::OnModify(GtkCellRendererText* cell,
                                   char* pathstr,
                                   char* text)
{

  // Get an iterator pointing to the cell that has been edited
  GtkTreePath* path = gtk_tree_path_new_from_string(pathstr);
  GtkTreeIter it;
  if (path == nullptr ||
      !gtk_tree_model_get_iter(GTK_TREE_MODEL(_store), &it, path))
  {
    throw BPMException("Failed to get iterator from text (gtk)");
  }

  // Save old domain value
  char* olddomain = nullptr;
  gtk_tree_model_get(
      GTK_TREE_MODEL(_store), &it, Column::Domain, &olddomain, -1);

  // Accept the modification (apply new text to the model)

  // Get the column number (previoulsy stored in AddColumn function)
  size_t cl = GPOINTER_TO_UINT(g_object_get_data(G_OBJECT(cell), "bpm_column"));

  // Check for empty domain
  if (cl == static_cast<size_t>(Column::Domain) && strlen(text) == 0)
  {
    return;
  }

  // Apply the text change to the cell
  gtk_list_store_set(_store, &it, cl, text, -1);

  // Get the row that has been edited
  char* domain = nullptr;
  char* username = nullptr;
  const char* password = ""; // Empty string does not modify password

  gtk_tree_model_get(GTK_TREE_MODEL(_store),
                     &it,
                     Column::Domain,
                     &domain,
                     Column::Username,
                     &username,
                     -1);

  if (cl == static_cast<size_t>(
                Column::Password)) // Special case: the password has changed
  {
    password = text;
  }

  // Apply changes to storage
  if (cl == static_cast<size_t>(Column::Domain) && strcmp(domain, olddomain))
  {
    auto cb = std::bind(&XCredentialsDisplay::Confirm,
                        this,
                        "Domain \"" + std::string(domain) +
                            "\" already exists, overwrite ?");
    if (!ChangeDomain(olddomain, domain, cb))
    {
      Refresh(); // Refresh if the user cancel
    }
  }
  else
  {
    std::string edited = Helper::TimeString(Edit(domain, username, password));

    // Update timestamp column
    gtk_list_store_set(_store, &it, Column::Date, edited.data(), -1);
  }

  // If the column is the password column, hide its content
  if (cl == static_cast<size_t>(Column::Password) && !_showpasswords)
  {
    gtk_list_store_set(_store, &it, cl, "", -1);
  }

  // Release memory
  g_free(domain);
  g_free(olddomain);
  g_free(username);
}

void XCredentialsDisplay::BeginEdit(GtkCellRenderer*,
                                    GtkEntry* entry,
                                    char*,
                                    XCredentialsDisplay* instance)
{
  if (!GTK_IS_ENTRY(entry))
  {
    return;
  }
  // This function is called when the user starts modifying a password cell
  // so make the text input not display chars

  if (!instance->_showpasswords)
  {
    gtk_entry_set_visibility(entry, false);
  }
}

bool XCredentialsDisplay::ShowMenu(GtkWidget*,
                                   GdkEvent* event,
                                   XCredentialsDisplay* instance)
{
  // Check that a row is selected
  GtkTreeSelection* sel =
      gtk_tree_view_get_selection(GTK_TREE_VIEW(instance->_listview));
  if (!gtk_tree_selection_get_selected(sel, nullptr, nullptr))
  {
    return false; // Nothing selected, don't display menu
  }

  GtkWidget* menu = gtk_menu_new();

  GtkWidget* login_item = gtk_menu_item_new_with_label("Login");
  GtkWidget* symlink_item = gtk_menu_item_new_with_label("Create symlink");
  GtkWidget* delete_item = gtk_menu_item_new_with_label("Delete");

  g_signal_connect(
      delete_item, "activate", G_CALLBACK(DeleteMenuClicked), instance);

  g_signal_connect(
      symlink_item, "activate", G_CALLBACK(SymlinkMenuClicked), instance);

  g_signal_connect(
      login_item, "activate", G_CALLBACK(LoginMenuClicked), instance);

  gtk_menu_shell_append(GTK_MENU_SHELL(menu), login_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(menu), symlink_item);
  gtk_menu_shell_append(GTK_MENU_SHELL(menu), gtk_separator_menu_item_new());
  gtk_menu_shell_append(GTK_MENU_SHELL(menu), delete_item);

  gtk_widget_show_all(menu);

  guint time = gdk_event_get_time(event);

  gtk_menu_popup(GTK_MENU(menu), nullptr, nullptr, nullptr, nullptr, 0, time);

  return true;
}

bool XCredentialsDisplay::DeleteMenuClicked(GtkWidget*,
                                            XCredentialsDisplay* instance)
{
  instance->OnDelete();

  return true;
}

bool XCredentialsDisplay::LoginMenuClicked(GtkWidget*,
                                           XCredentialsDisplay* instance)
{
  instance->OnLogin();

  return true;
}

bool XCredentialsDisplay::SymlinkMenuClicked(GtkWidget*,
                                             XCredentialsDisplay* ptr)
{
  auto domain = ptr->GetSelectedDomain();
  ptr->_on_close = [target = std::move(domain), ptr=ptr]() {
    ptr->ShowAddCredentialsDialog(target);
  };
  ptr->WindowClosed(nullptr, nullptr, ptr);
  return true;
}

SecuredString XCredentialsDisplay::GetSelectedDomain()
{
  // Read selection from list view
  GtkTreeModel* model;
  GtkTreeIter iter;

  GtkTreeSelection* sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(_listview));
  if (!gtk_tree_selection_get_selected(sel, &model, &iter))
  {
    throw BPMException("Failed to get select row from list store");
  }

  char* domain = nullptr;

  gtk_tree_model_get(model, &iter, Column::Domain, &domain, -1);

  if (domain == nullptr)
  {
    throw BPMException("Failed to get select row from list store");
  }

  SecuredString result(domain);

  g_free(domain);

  return result;
}

void XCredentialsDisplay::OnDelete()
{
  auto domain = GetSelectedDomain();

  // Get user confirmation
  if (Confirm("Delete credentials for \"" + domain + "\" ?"))
  {
    // Perform actual deletion in base class
    Delete(domain);
    Refresh();
  }
}

void XCredentialsDisplay::OnLogin()
{
  auto domain = GetSelectedDomain();
  Login(domain);
}

bool XCredentialsDisplay::RightClicked(GtkWidget* view,
                                       GdkEventButton* event,
                                       XCredentialsDisplay* instance)
{
  // Intercept left click event, 3 is code for left button
  if (event->type == GDK_BUTTON_PRESS && event->button == 3)
  {
    ShowMenu(view, reinterpret_cast<GdkEvent*>(event), instance);
    return true;
  }

  // Don't consume event otherwise
  return false;
}

bool XCredentialsDisplay::Confirm(const std::string& message) const
{
  GtkWidget* dialog = gtk_message_dialog_new(GTK_WINDOW(_window),
                                             GTK_DIALOG_MODAL,
                                             GTK_MESSAGE_QUESTION,
                                             GTK_BUTTONS_YES_NO,
                                             "%s",
                                             message.data());

  bool result = gtk_dialog_run(GTK_DIALOG(dialog)) == GTK_RESPONSE_YES;

  gtk_widget_destroy(dialog);

  return result;
}

bool XCredentialsDisplay::SearchEdited(GtkEntry* entry,
                                       XCredentialsDisplay* instance)
{
  instance->SearchPattern(
      gtk_editable_get_chars(reinterpret_cast<GtkEditable*>(entry), 0, -1));

  return true;
}

void XCredentialsDisplay::ShowPasswordsClicked(GtkToggleButton* button,
                                               XCredentialsDisplay* instance)
{
  instance->SetShowPasswords(gtk_toggle_button_get_active(button));
}

bool XCredentialsDisplay::WindowClosed(GtkWidget*,
                                       GdkEvent*,
                                       XCredentialsDisplay* instance)
{
  instance->Close();
  return false;
}

void XCredentialsDisplay::Close()
{
  gtk_widget_destroy(_window);
  XWindow::Quit();
}
