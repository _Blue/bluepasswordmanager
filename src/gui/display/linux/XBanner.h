#pragma once

#include "input/linux/GTKfwd.h"
#include "input/linux/GTKHandler.h"
#include "display/Banner.h"

namespace bpm
{
  namespace display
  {
    class XBanner : public Banner
    {
    public:
      XBanner(const ConfigManager &config, bool movable = false);
      ~XBanner();
      virtual void Hide() override;

      std::pair<int, int> Position() const;

    protected:
      virtual void BuildWindow() override;
      virtual void ShowWindow() override;
      virtual void RunEventLoop() override;
      virtual void ExitEventLoop() override;
      virtual void SetTextImpl(const std::string& text) override;

    private:
      void BuildWindowImpl();

      bool _event_loop_started = false;
      bool _movable = false;
      GtkWidget* _window = nullptr;
      GtkWidget* _label = nullptr;
    };
  }
}
