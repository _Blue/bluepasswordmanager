#pragma once

#include <optional>
#include "display/CredentialsDisplay.h"
#include "input/linux/GTKHandler.h"
#include "input/linux/GTKfwd.h"
#include "input/linux/XWindow.h"
#include <optional>

namespace bpm
{
  namespace display
  {
    class XCredentialsDisplay : public CredentialsDisplay, public input::XWindow
    {
    public:
      XCredentialsDisplay(ConfigManager& config, data::Storage& storage);

    protected:
      virtual void RunEventLoop();
      virtual void Close() override;
      virtual void BuildWindow() override;
      virtual void CleanList() override;
      virtual void AddToList(const data::SecuredString& domain,
                             const data::SecuredString& username,
                             const data::SecuredString& password,
                             const std::string& date) override;
      virtual void Build() override;

    private:
      void BuildWindowImpl();
      data::SecuredString GetSelectedDomain();
      void OnModify(GtkCellRendererText* cell, char* path, char* text);
      void OnDelete();
      void OnLogin();
      void OnSymlink();
      void AddColumn(const char* name,
                     Column col,
                     bool editable,
                     bool password,
                     GtkWidget* view);

      bool Confirm(const std::string& message) const;
      void ShowAddCredentialsDialog(const std::optional<data::SecuredString>& symlinkDomain);

      static bool RightClicked(GtkWidget* view,
                               GdkEventButton* event,
                               XCredentialsDisplay* ptr);

      static bool AddEntryClicked(GtkWidget*, XCredentialsDisplay* ptr);

      static bool
      ShowMenu(GtkWidget* view, GdkEvent* event, XCredentialsDisplay* ptr);
      static bool DeleteMenuClicked(GtkWidget*, XCredentialsDisplay* ptr);
      static bool LoginMenuClicked(GtkWidget*, XCredentialsDisplay* ptr);
      static bool SymlinkMenuClicked(GtkWidget*, XCredentialsDisplay* ptr);

      static void Modify(GtkCellRendererText* cell,
                         char* path,
                         char* text,
                         XCredentialsDisplay* ptr);

      static void BeginEdit(GtkCellRenderer* renderer,
                            GtkEntry* entry,
                            char* path,
                            XCredentialsDisplay* ptr);

      static bool SearchEdited(GtkEntry* entry, XCredentialsDisplay* ptr);
      static void ShowPasswordsClicked(GtkToggleButton* button,
                                       XCredentialsDisplay* ptr);

      static bool WindowClosed(GtkWidget*, GdkEvent*, XCredentialsDisplay* ptr);

      GtkWidget* _window = nullptr;
      GtkWidget* _listview = nullptr;
      GtkListStore* _store = nullptr;
      std::function<void()> _after_close;
    };
  }
}
