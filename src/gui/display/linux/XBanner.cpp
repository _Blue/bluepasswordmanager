#include <gtk/gtk.h>
#include <cassert>
#include "XBanner.h"
#include "input/linux/GTKHandler.h"

using bpm::display::XBanner;
using bpm::input::GTKHandler;

XBanner::XBanner(const ConfigManager& config, bool movable)
    : Banner(config), _movable(movable)
{
}

std::pair<int, int> XBanner::Position() const
{

  std::pair<int, int> position{};

  auto routine = [this, &position]()
  {
    gtk_window_get_position(GTK_WINDOW(_window), &position.first, &position.second);
  };

  GTKHandler::RunInGtkContext<void>(routine);

  return position;
}

void XBanner::BuildWindowImpl()
{
  _window = gtk_window_new(_movable ? GTK_WINDOW_TOPLEVEL : GTK_WINDOW_POPUP);
  _label = gtk_label_new(_text.data());

  gtk_container_add(GTK_CONTAINER(_window), _label);

  // Make window top most
  gtk_window_set_keep_above(GTK_WINDOW(_window), true);

  // Set minimum size, but allow the window to extend if needed
  gtk_widget_set_size_request(_window, 300, 30);

  const auto& hardcoded_position = _config.GetBannerPosition();
  if (hardcoded_position.has_value())
  {
    gtk_window_move(GTK_WINDOW(_window),
                    hardcoded_position->first,
                    hardcoded_position->second);
  }
  else
  {
    // Move the window to the bottom right corner
    size_t x = gdk_screen_width();
    size_t y = gdk_screen_height();

    gint windowx;
    gint windowy;
    gtk_window_get_size(GTK_WINDOW(_window), &windowx, &windowy);

    x -= windowx + _config.GetBannerOffset().first;
    y -= windowy + _config.GetBannerOffset().second;

    gtk_window_move(GTK_WINDOW(_window), x, y);
  }

  if (!_movable)
  {

    // Remove window's borders
    gtk_window_set_decorated(GTK_WINDOW(_window), false);
  }
}

void XBanner::BuildWindow()
{
  GTKHandler::RunInGtkContext<void>(std::bind(&XBanner::BuildWindowImpl, this));
}

void XBanner::ShowWindow()
{
  GTKHandler::RunInGtkContext<void>(std::bind(gtk_widget_show_all, _window));
}

void XBanner::Hide()
{
  if (_window != nullptr)
  {
    GTKHandler::RunInGtkContext<void>(std::bind(gtk_widget_hide, _window));
  }
}

void XBanner::SetTextImpl(const std::string& text)
{
  assert(_label != nullptr);

  GTKHandler::RunInGtkContext<void>(
      std::bind(gtk_label_set_text, GTK_LABEL(_label), text.data()));
}

void XBanner::RunEventLoop()
{
}

void XBanner::ExitEventLoop()
{
}

XBanner::~XBanner()
{
}
