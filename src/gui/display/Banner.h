#pragma once

#include <string>
#include <thread>
#include "ConfigManager.h"

namespace bpm
{
  namespace display
  {
    class Banner
    {
    public:
      Banner(const ConfigManager& config);
      virtual ~Banner();
      void ShowAsync();
      virtual void Hide() = 0;
      void SetText(const std::string& text);

    protected:
      virtual void BuildWindow() = 0;
      virtual void ShowWindow() = 0;
      virtual void RunEventLoop() = 0;
      virtual void ExitEventLoop() = 0;
      virtual void SetTextImpl(const std::string& text) = 0;

      std::string _text;
      bool _visible = false;
      const ConfigManager& _config;

    private:
      void Work();

      std::thread _thread;
    };
  }
}
