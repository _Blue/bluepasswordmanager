#include "GUIApplication.h"
#include "BPMException.h"
#include "CancelException.h"
#include "data/SecuredStorage.h"
#include "data/Serialization.h"

#if !defined(_WIN32) && !defined(ANDROID)
#include "input/linux/LibSecretKeyHandler.h"
#include "input/linux/GTKHandler.h"
#include "display/linux/XBanner.h"
#endif

using bpm::ConfigManager;
using bpm::GUIApplication;
using bpm::data::SecuredStorage;
using bpm::data::SecuredString;
using bpm::data::SecuredVector;
using bpm::data::Storage;
using bpm::input::InputMode;
using bpm::input::PasswordInput;
using bpm::persistence::KeyHandler;

using namespace std::placeholders;

void GUIApplication::LoadArguments()
{
  Application::LoadArguments();

  AddArgumentHandler("-k",
                     "Show keyboard keys as they are pressed",
                     std::bind(&GUIApplication::ShowKeys, this));

  AddArgumentHandler("-l",
                     "List / edit credentials in storage",
                     std::bind(&GUIApplication::ShowCredentials, this));

#ifndef WIN32
  AddArgumentHandler(
      "-m",
      "Show a movable banner (useful to determine its screen position)",
      std::bind(&GUIApplication::ShowBanner, this));
#endif
}

void GUIApplication::PrepareConfig(ConfigManager& config)
{
#ifdef WIN32 // Done here because KeyWatchers are only available in GUI and not
             // in core
  config.SetExitKeys("(" + OSSpecific::KeyWatcher::GetEscapeKeyName() + ")");
#else
  (void)config;
#endif
}

std::unique_ptr<InputMode> GUIApplication::BuildInputMode()
{
  _storage = BuildStorage(GetStoragePath());
  return BuildOSInput(*_storage);
}

std::unique_ptr<bpm::input::InputMode>
GUIApplication::BuildOSInput(Storage& storage)
{
  return std::make_unique<OSSpecific::InputMode>(*_config, storage);
}

std::unique_ptr<PasswordInput>
GUIApplication::BuildInputHandlerImpl(bool create,
                                      const PasswordInput::CheckRoutine& pred)
{
  try
  {
    SetupGUI();
  }
  catch (const std::runtime_error&)
  {
    std::cerr
        << "Failed to initialize GUI, falling back to console password input"
        << std::endl;

    return Application::BuildInputHandlerImpl(create, pred);
  }

  return std::make_unique<OSSpecific::PasswordInput>(pred, create);
}

std::unique_ptr<KeyHandler> GUIApplication::GetPersistenceHandler() const
{
  if (_config->IsLibSecretKeyStoreEnabled())
  {
#if defined(_WIN32) || defined(ANDROID)
    throw BPMException("LibSecret is enabled but unsuported on this platform");
#else
    return std::make_unique<input::LibSecretKeyHandler>(
        _config->GetEncryptionKeySize());
#endif
  }

  return std::make_unique<OSSpecific::PersistenceHandler>(
      _config->GetEncryptionKeySize());
}

bool GUIApplication::ShowKeys()
{
  SetupGUI();
  OSSpecific::KeyWatcher watcher;
  watcher.PrintPressedKeys();

  return true;
}

bool GUIApplication::ShowCredentials()
{
  SetupGUI();

  auto storage = BuildStorage(GetStoragePath());
  OSSpecific::CredentialsDisplay display(*_config, *storage);

  display.Show();
  return true;
}

std::function<bool()> GUIApplication::GetDefaultAction()
{
  return [this]()
  {
    SetupGUI();
    BuildInputMode()->Start();
    return true;
  };
}

bool GUIApplication::HasGUISupport() const
{
  return true;
}

void GUIApplication::SetupGUI()
{
  if (!_gui_lock)
  {
    _gui_lock = std::make_unique<OSSpecific::GUILock>();
  }
}


#ifndef WIN32
bool GUIApplication::ShowBanner()
{
  bpm::input::GTKHandler::GTKLock GTK;

  bpm::display::XBanner banner(*_config, true);
  banner.SetText("Sample BPM banner. Press ESC to close");
  banner.ShowAsync();

  std::cerr << "Banner should be visible. Press ESC to exit" << std::endl;

  OSSpecific::KeyWatcher watcher;
  watcher.WaitForKeys({{"Escape"}});

  auto [x, y] = banner.Position();
  std::cerr << "banner_position=" << x << "," << y << std::endl;

  return true;
}

#endif
