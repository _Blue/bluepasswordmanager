/*
 * This header is used to select the proper type depending on the platform
 */

#ifdef _WIN32
#include "input/win32/Win32Input.h"
#include "input/win32/Win32PasswordInput.h"
#include "input/win32/Win32KeyWatcher.h"
#include "persistence/Win32KeyHandler.h"
#include "display/win32/Win32CredentialsDisplay.h"
#else
#include "input/linux/XInput.h"
#include "input/linux/XPasswordInput.h"
#include "input/linux/XKeyWatcher.h"
#include "input/linux/GTKHandler.h"
#include "persistence/LinuxKeyHandler.h"
#include "display/linux/XCredentialsDisplay.h"
#endif

#pragma once

namespace bpm
{
  class OSSpecific
  {
  public:
#ifdef _WIN32
    using InputMode = input::Win32Input;
    using PasswordInput = input::Win32PasswordInput;
    using PersistenceHandler = persistence::Win32KeyHandler;
    using CredentialsDisplay = display::Win32CredentialsDisplay;
    using KeyWatcher = input::Win32KeyWatcher;
    using KeyHandler= input::Win32KeyWatcher;
    using GUILock = int; // Not needed on Windows
#else
    using InputMode = input::XInput;
    using PasswordInput = input::XPasswordInput;
    using PersistenceHandler = persistence::LinuxKeyHandler;
    using CredentialsDisplay = display::XCredentialsDisplay;
    using KeyWatcher = input::XKeyWatcher;
    using GUILock = input::GTKHandler::GTKLock;
#endif
  };
}
