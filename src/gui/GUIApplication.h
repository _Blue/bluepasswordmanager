#pragma once

#include <cstddef>
#include <functional>
#include <map>
#include <memory>
#include <string>
#include <deque>

#include "Application.h"
#include "input/InputMode.h"
#include "OSSpecific.h"

namespace bpm
{
  class GUIApplication : public Application
  {
  public:
    GUIApplication() = default;

  protected:
    bool ShowCredentials();

    virtual void LoadArguments() override;

    virtual void PrepareConfig(ConfigManager& config) override;

    virtual std::unique_ptr<input::PasswordInput> BuildInputHandlerImpl(
        bool create,
        const input::PasswordInput::CheckRoutine& pred) override;

    virtual std::unique_ptr<persistence::KeyHandler>
    GetPersistenceHandler() const override;

    virtual std::function<bool()> GetDefaultAction() override;

    virtual bool HasGUISupport() const override;

    std::unique_ptr<input::InputMode> BuildInputMode();

    std::unique_ptr<input::InputMode> BuildOSInput(data::Storage& storage);

    void SetupGUI();

    bool ShowKeys();

    bool ShowBanner();

    std::unique_ptr<OSSpecific::GUILock> _gui_lock;
  };
}
