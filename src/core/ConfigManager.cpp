#include <iostream>

#include <fstream>
#include <functional>
#include <algorithm>

#include "ConfigManager.h"
#include "BPMException.h"

using namespace std::placeholders;

using bpm::ConfigManager;

ConfigManager::ConfigManager()
{
}

std::map<std::string, ConfigManager::LineHandler> ConfigManager::GetHandlers()
{
  std::map<std::string, LineHandler> handlers;

  handlers.emplace("encryption",
                   [this](const std::string& str)
                   { _encryption_algorithm = str; });

  handlers.emplace("key_size",
                   [this](const std::string& str)
                   { _key_size = ReadInteger(str); });

  handlers.emplace("enable_daemon",
                   [this](const std::string&) { _enable_daemon = true; });

  handlers.emplace("disable_daemon",
                   [this](const std::string&) { _enable_daemon = false; });

  handlers.emplace("enable_libsecret",
                   [this](const std::string&) { _enable_libsecret = true; });

  handlers.emplace("paste_keys",
                   std::bind(&ConfigManager::SetPasteKeys, this, _1));

  handlers.emplace("exit_keys",
                   std::bind(&ConfigManager::SetExitKeys, this, _1));

  handlers.emplace("storage_path",
                   [this](const std::string& value) { _storage_path = value; });

  handlers.emplace("banner_x",
                   [this](const std::string& value)
                   { _banner_offset.first = ReadInteger(value); });

  handlers.emplace("banner_y",
                   [this](const std::string& value)
                   { _banner_offset.second = ReadInteger(value); });

  handlers.emplace("disable_banner",
                   [this](const std::string&) { _enable_banner = false; });

  handlers.emplace("password_size",
                   [this](const std::string& text)
                   { _password_generation_size = ReadInteger(text); });

  handlers.emplace("password_charset",
                   [this](const std::string& text)
                   { _password_generation_charset = text; });

  handlers.emplace("banner_position",
                   [this](const auto& text)
                   { _banner_position = ReadIntegerPair(text); });

  return handlers;
}

void ConfigManager::Load(const std::string& filename)
{
  std::ifstream file(filename);
  if (file) // if no configuration file found, keep default values
  {
    auto handlers = GetHandlers();

    std::string line;
    while (std::getline(file, line))
    {
      // Remove spaces, tabulation, and comments
      CleanLine(line);
      if (!line.empty())
      {
        ExecuteLine(line, handlers);
      }
    }
  }

  // If paste_keys are still empty, use default value
  if (_pastekeys.empty())
  {
    SetPasteKeys(defs::default_paste_keys);
  }

// Can't be done on Windows because the escape key name depends on the locale
#ifndef _WIN32
  if (_exitkeys.empty())
  {
    SetExitKeys(defs::default_exit_keys);
  }
#endif
}

void ConfigManager::ExecuteLine(
    std::string& line, const std::map<std::string, LineHandler>& handlers)
{
  /* Two possiblities:
   * 1) name = value
   * 2) value_enabled
   */

  // Extract the parameter name

  size_t delim = line.find(defs::config_delim);

  std::string name;
  if (delim != std::string::npos) // Check if 1) or 2)
  {
    name = line.substr(0, delim);
  }
  else
  {
    name = line;
  }

  // Find the appropriate handler

  auto it = handlers.find(name);

  if (it == handlers.end())
  {
    throw BPMException("Invalid identifier in configuration file: \"" + name + "\"");
  }

  // Extract the value of the value of the parameter (case 1))

  std::string value;
  if (delim != std::string::npos && delim != line.size() - 1)
  {
    value = line.substr(delim + 1, line.size() - delim - 1);
  }

  it->second(value);
}

void ConfigManager::CleanLine(std::string& str)
{
  // Remove enventual comment

  size_t commentpos = str.find(defs::config_comment);
  if (commentpos != std::string::npos)
  {
    str.resize(commentpos);
  }

  // Remove ignored characters (spaces, tabulations, etc)
  const char* it = defs::config_ignored_chars;

  while (*it != '\0')
  {
    str.erase(std::remove(str.begin(), str.end(), *it), str.end());
    it++;
  }
}

std::set<std::string> ConfigManager::StringToKeys(const std::string& text)
{
  // Syntax is the following:
  //(key1)(key2)...

  if (text.size() < 2)
  {
    throw BPMException("Invalid key syntax");
  }
  std::set<std::string> keys;

  size_t endpos;
  size_t startpos = 0;
  do
  {
    if (text[startpos] != '(')
    {
      throw BPMException("Invalid key syntax, expected \"(\"");
    }

    endpos = text.find(")", startpos + 2); // A key is at least one char
    if (endpos == std::string::npos)
    {
      throw BPMException("invalid keys syntax, missing \")\"");
    }

    keys.emplace(text.substr(startpos + 1, endpos - startpos - 1));
    startpos = endpos + 1;
  } while (startpos < text.size());

  return keys;
}

size_t ConfigManager::ReadInteger(const std::string& text)
{
  bool error = false;
  size_t value;
  try
  {
    size_t pos;
    value = static_cast<size_t>(std::stoull(text, &pos));
    error = pos != text.size();
  }
  catch (const std::invalid_argument&)
  {
    error = true;
  }
  if (error)
  {
    throw bpm::BPMException("Invalid integer in configuration file: \"" + text +
                            "\"");
  }

  return value;
}

std::pair<size_t, size_t>
ConfigManager::ReadIntegerPair(const std::string& text)
{
  auto space_pos = text.find(',');
  if (space_pos == std::string::npos || space_pos >= text.size() - 1)
  {
    throw bpm::BPMException("Invalid integer pair in configuration file: \"" +
                            text + "\"");
  }

  return std::make_pair(ReadInteger(text.substr(0, space_pos)),
                        ReadInteger(text.substr(space_pos + 1)));
}

void ConfigManager::SetPasteKeys(const std::string& text)
{
  _pastekeys = StringToKeys(text);
}

void ConfigManager::SetExitKeys(const std::string& text)
{
  _exitkeys = StringToKeys(text);
}

const std::string& ConfigManager::GetEncryptionAlgorithm() const
{
  return _encryption_algorithm;
}

size_t ConfigManager::GetEncryptionKeySize() const
{
  return _key_size;
}

bool ConfigManager::IsDaemonModeEnabled() const
{
  return _enable_daemon;
}

const std::set<std::string>& ConfigManager::GetPasteKeys() const
{
  return _pastekeys;
}

void ConfigManager::SetStoragePath(const std::string& path)
{
  _storage_path = path;
}

const std::string& ConfigManager::GetStoragePath() const
{
  return _storage_path;
}

const std::set<std::string>& ConfigManager::GetExitKeys() const
{
  return _exitkeys;
}

const std::pair<size_t, size_t>& ConfigManager::GetBannerOffset() const
{
  return _banner_offset;
}

bool ConfigManager::IsBannerEnabled() const
{
  return _enable_banner;
}

size_t ConfigManager::DefaultEncryptionKeySize()
{
  return defs::default_encryption_key_size;
}

bool ConfigManager::IsLibSecretKeyStoreEnabled() const
{
  return _enable_libsecret;
}

size_t ConfigManager::GetPasswordGenerationSize() const
{
  return _password_generation_size;
}

const std::string& ConfigManager::GetPasswordGenerationCharset() const
{
  return _password_generation_charset;
}

const std::optional<std::pair<size_t, size_t>>& ConfigManager::GetBannerPosition() const
{
  return _banner_position;
}
