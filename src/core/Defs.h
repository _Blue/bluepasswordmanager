#pragma once

#include <cstddef>

namespace bpm::defs
{
    inline const char* copy_suffix = ".update";
    inline const char* default_config_path = "~/.config/bpm/config";
    inline const char* default_storage_path = "~/.config/bpm/passwd";
    inline const char sepator = ':';
    inline const char config_delim = '=';
    inline const char* config_ignored_chars = "\t ";
    inline const char config_comment = '#';
    inline const char* placeholder_text = "BPM: waiting for input";
    inline size_t clip_refresh_ms = 10;
    inline unsigned long file_permissions = 0750;
    inline size_t x_clipboard_freq_ms = 10;
    inline const char* encryption_salt = "NeverStoreUnsaltedPW";
    inline size_t hmac_iterations = 256;
    inline size_t hash_salt_size = 256;
    inline size_t persistence_max_blob = 40960;
    inline const char* version = "1.1";
    inline const char* time_format = "%F %T";
    inline size_t storage_version = 1;
    inline const char* env_key_variable_name = "bpm_storage_key";
    inline const char* symlink_prefix = "bpm-symlink";

    // Default configuration values
    inline bool default_daemon_enabled = true;
    inline const char* default_encryption_algorithm = "AES";
    inline size_t default_encryption_key_size = 256;
    inline size_t default_banner_offset = 50;
    inline size_t default_password_generation_size = 15;
    inline const char* default_password_generation_charset =
        "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/"
        "|,.;!?:*+-=[]()|{}'\"\\#~&<>$^%";

// Platform specific configuration
#ifdef _WIN32
    inline const char* home_var = "USERPROFILE";
    inline const char* default_paste_keys = "(CTRL)(V)";
#else
    inline const char* home_var = "HOME";
    inline const char* shm_name = "/blue_password_manager_key";
    inline const char* default_paste_keys = "(Control_L)(v)";
    inline const char* default_exit_keys = "(Escape)";
    inline const char* gnome_keyring_display_name = "bpm-key";
#endif
}
