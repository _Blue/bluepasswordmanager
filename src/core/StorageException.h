#pragma once

#include "BPMException.h"
namespace bpm
{
  class StorageException : public BPMException
  {
  public:
    StorageException(const std::string& message, const std::string& storage);

    const std::string& Storage() const noexcept;

  private:
    std::string _storage;

  };
}
