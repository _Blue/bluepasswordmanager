#include "BPMException.h"

using bpm::BPMException;

BPMException::BPMException(const std::string& message)
    : std::exception(), _message(message)
{
}

const char* BPMException::what() const noexcept
{
  return _message.data();
}
