#pragma once

#include "data/SecuredString.h"
namespace bpm
{
  class Helper
  {
    public:
      static bool FileExists(const std::string &path);
      static bool FolderExists(const std::string &path);
      static void CreateFolder(const std::string &path);
      static void Remove(const std::string &path);
      static std::string ExpandPath(const std::string &path);
      static void FixOsPath(std::string &path);
      static void Rename(std::string oldname, std::string newname);
      static time_t  Now();
      static std::string TimeString(time_t ts);
      static void CenterWindow(void* window);
      static data::SecuredString GetDomainAuthority(const std::string& url);
      static std::string GetLastErrorMessage();
      static void AttachToConsole();
      static data::SecuredString GeneratePassword(const std::string& charset, size_t length);

  private:
    static void CreateFolderImpl(const std::string &path);
  };
}
