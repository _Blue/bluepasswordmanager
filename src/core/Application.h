#pragma once

#include <cstddef>
#include <functional>
#include <map>
#include <memory>
#include <string>
#include <deque>

#include "data/Storage.h"
#include "ConfigManager.h"
#include "persistence/KeyHandler.h"
#include "input/ConsolePasswordInput.h"

namespace bpm
{
  class Application
  {
  public:
    struct Argument
    {
      bool run_first = false;
      bool takes_value = false;
      bool optional_value = false;
      std::function<bool(const char* value)> handler;
      std::string description;
    };

    Application() = default;
    virtual ~Application() = default;

    void ApplyArgv(size_t argc, char** argv);
    int Run();

  protected:
    const char* GetConfigPath() const;
    const char* GetStoragePath() const;

    virtual std::unique_ptr<input::PasswordInput> BuildInputHandlerImpl(
        bool create,
        const input::PasswordInput::CheckRoutine& check_routine);

    virtual std::function<bool()> GetDefaultAction();

    virtual std::unique_ptr<persistence::KeyHandler>
    GetPersistenceHandler() const;

    virtual void LoadArguments();

    virtual bool HasGUISupport() const;

    virtual void PrepareConfig(ConfigManager& config);

    void
    AddValuedArgumentHandler(const std::string& value,
                             const std::string& description,
                             const std::function<bool(const char*)>& routine,
                             bool optional = false);

    void AddArgumentHandler(const std::string& value,
                            const std::string& description,
                            const std::function<bool()>& routine);

    template <typename T>
    void AddValueArgument(const std::string& value,
                          const std::string& description,
                          T& target);

    std::unique_ptr<data::Storage> BuildStorage(const std::string& path,
                                                bool save = true);

    bool _disable_import_timestamp = false;
    std::unique_ptr<data::Storage> _storage;
    std::unique_ptr<ConfigManager> _config;

  private:
    bool ClearKey() const;
    void ProcessArgument(const std::string& switch_name,
                         const Argument& argument,
                         const char* value);

    virtual bool ImportStorage(const char* path);

    std::unique_ptr<input::PasswordInput>
    BuildInputHandler(const std::string& path);

    bool ImportStorageImpl(
        data::Storage& storage,
        const char* path,
        const data::Storage::ConflictPred& username_conflict,
        const data::Storage::ConflictPred& password_conflict,
        std::vector<std::pair<data::SecuredString, data::SecuredString>>&
            imported);

    bool ShowHelp() const;

    bool Export();

    bool Display(bool username, const std::string& url);

    static void DisplayImportResult(
        const std::vector<std::pair<data::SecuredString, data::SecuredString>>&
            result,
        std::ostream& out);

    static data::Storage::ConflictAction
    ImportConflict(bool user_conflict,
                   const data::SecuredString& domain,
                   const data::SecuredString& current,
                   const data::SecuredString& imported);

    static data::Storage::ConflictAction
    ImportConflictWithTimestamp(bool user_conflict,
                                const data::SecuredString& domain,
                                const data::SecuredString& current,
                                const data::SecuredString& imported,
                                size_t current_ts,
                                size_t imported_ts);

    bool ExportStorageEncryptionKey();

    bool GeneratePassword() const;

    static std::unique_ptr<data::SecuredVector> FindKeyInEnvironement();

    std::string _storage_path_override;
    std::string _config_path_override;
    std::map<std::string, Argument> _arguments;
    std::deque<std::function<bool()>> _actions;
  };
}

#include "Application.hxx"
