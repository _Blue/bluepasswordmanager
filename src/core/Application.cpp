#include <cassert>
#include <ios>
#include <queue>
#include <algorithm>

#include "Application.h"
#include "BPMException.h"
#include "CancelException.h"
#include "Helper.h"
#include "data/SecuredStorage.h"
#include "data/Serialization.h"
#include "encryption/Encryptor.h"
#ifdef _WIN32
#include "persistence/Win32KeyHandler.h"
#else
#ifndef ANDROID
#include "persistence/LinuxKeyHandler.h"
#endif
#endif

using bpm::Application;
using bpm::ConfigManager;
using bpm::data::SecuredStorage;
using bpm::data::SecuredString;
using bpm::data::SecuredVector;
using bpm::data::Storage;
using bpm::input::PasswordInput;
using bpm::persistence::KeyHandler;

using namespace std::placeholders;

namespace
{
  bool IsPasswordValid(const std::string& path,
                       const ConfigManager& config,
                       const SecuredString& password)
  {
    SecuredStorage storage(path, config.GetEncryptionKeySize(), password);

    return storage.Good();
  }

  bool IsKeyValid(const std::string& path,
                  const SecuredVector& key,
                  bool must_exist = false)
  {
    SecuredStorage storage(path, key);

    return storage.Good() && (!must_exist || !storage.IsBeingCreated());
  }
}

void Application::AddArgumentHandler(const std::string& value,
                                     const std::string& description,
                                     const std::function<bool()>& routine)
{
  auto handler = [routine](const char*) { return routine(); };

  _arguments.emplace(value,
                     Argument{false, false, false, handler, description});
}

void Application::AddValuedArgumentHandler(
    const std::string& value,
    const std::string& description,
    const std::function<bool(const char*)>& routine,
    bool optional)
{
  _arguments.emplace(value,
                     Argument{false, true, optional, routine, description});
}

void Application::LoadArguments()
{
  AddValueArgument("-c", "Set configuration file path", _config_path_override);
  AddValueArgument("-s", "Set storage file path", _storage_path_override);

  AddArgumentHandler("-x",
                     "Delete storage key from memory",
                     std::bind(&Application::ClearKey, this));

  AddArgumentHandler(
      "--help", "Show this message", std::bind(&Application::ShowHelp, this));

  AddValuedArgumentHandler("-i",
                           "Import storage content (-i without value reads "
                           "serialized content from stdin)",
                           std::bind(&Application::ImportStorage, this, _1),
                           true);

  AddValueArgument("-t",
                   "When importing storage, don't merge using timestamp",
                   _disable_import_timestamp);

  AddValuedArgumentHandler("-u",
                           "Display username for specified domain on stdout",
                           std::bind(&Application::Display, this, true, _1));

  AddValuedArgumentHandler("-p",
                           "Display password for specified domain on stdout",
                           std::bind(&Application::Display, this, false, _1));

  AddArgumentHandler("-e",
                     "Export storage content on stdout (for import)",
                     std::bind(&Application::Export, this));

  AddArgumentHandler("-z",
                     "Export the storage encryption key (useful to forward it "
                     "through an SSH session)",
                     std::bind(&Application::ExportStorageEncryptionKey, this));

  AddArgumentHandler("-g",
                     "Generate a password and display it on stdout",
                     std::bind(&Application::GeneratePassword, this));

}

void Application::ApplyArgv(size_t argc, char** argv)
{
  argc--;
  argv++; // Ignore program name

  LoadArguments();

  for (size_t i = 0; i < argc; i++)
  {
    const auto& arg = _arguments.find(argv[i]);

    if (arg == _arguments.end())
    {
      throw BPMException("Unrecognized option in command line: " +
                         std::string(argv[i]));
    }

    const char* value = i < argc - 1 ? argv[i + 1] : nullptr;
    if (value != nullptr && value[0] == '-') // if value is a switch, ignore
    {
      value = nullptr;
    }
    else
    {
      i++; // else consume argument
    }

    ProcessArgument(arg->first, arg->second, value);
  }

  if (_actions.empty()) // If no action set, run default mode
  {
    _actions.push_back(GetDefaultAction());
  }
}

std::function<bool()> Application::GetDefaultAction()
{
  return std::bind(&Application::ShowHelp, this);
}

std::unique_ptr<KeyHandler> Application::GetPersistenceHandler() const
{
#ifdef _WIN32
  return std::make_unique<persistence::Win32KeyHandler>(
      _config->GetEncryptionKeySize());
#else
#ifdef ANDROID
  return nullptr;
#else
  return std::make_unique<persistence::LinuxKeyHandler>(
      _config->GetEncryptionKeySize());
#endif
#endif
}

void Application::ProcessArgument(const std::string& switch_name,
                                  const Argument& argument,
                                  const char* value)
{
  if (value == nullptr && argument.takes_value && !argument.optional_value)
  {
    throw BPMException("Argument: " + switch_name + " requires a value");
  }

  if (argument.run_first)
  {
    argument.handler(value);
  }
  else
  {
    _actions.push_back(
        [value, &argument]() { return argument.handler(value); });
  }
}

int Application::Run()
{
  std::string config_path(defs::default_config_path);
  if (!_config_path_override.empty())
  {
    config_path = _config_path_override;
  }

  _config = std::make_unique<ConfigManager>();
  PrepareConfig(*_config);

  _config->Load(Helper::ExpandPath(config_path));

  if (!_storage_path_override.empty())
  {
    _config->SetStoragePath(_storage_path_override);
  }

  // Execute pre-loaded actions
  assert(!_actions.empty());

  return !std::all_of(
      _actions.begin(), _actions.end(), [](const auto& e) { return e(); });
}

std::unique_ptr<bpm::data::Storage>
Application::BuildStorage(const std::string& path, bool save)
{
  std::string expanded_path = Helper::ExpandPath(path);

  // First check for key forwarding in environement
  auto key_from_env = FindKeyInEnvironement();

  if (key_from_env != nullptr)
  {
    if (!IsKeyValid(expanded_path, *key_from_env))
    {
      throw BPMException(
          "Failed to unlock storage with key found in env variable");
    }

    return std::make_unique<data::SecuredStorage>(expanded_path, *key_from_env);
  }

  // Then check for already set key
  auto keyhandler = GetPersistenceHandler();

  if (keyhandler && keyhandler->IsKeyAvailable())
  {
    auto key = keyhandler->RetrieveKey();

    if (IsKeyValid(expanded_path, key, true))
    {
      return std::make_unique<data::SecuredStorage>(expanded_path, key);
    }
    else
    {
      std::cerr << "Failed to unlock storage with persistence key, discarding "
                   "persistence data "
                << std::endl;

      keyhandler->Clear();
    }
  }

  auto passwordinput = BuildInputHandler(path);

  auto password = passwordinput->QueryPassword(path);

  encryption::Encryptor cryptor(_config->GetEncryptionKeySize(), password);

  auto key = cryptor.DeriveKey(password);

  if (_config->IsDaemonModeEnabled() && save && keyhandler)
  {
    try
    {
      keyhandler->SaveKey(key);
    }
    catch (
        const std::exception& e) // Can happen over SSH if libsecret is enabled
    {
      std::cerr << "Error while storing storage key: " << e.what() << std::endl;
    }
  }

  return std::make_unique<data::SecuredStorage>(expanded_path, key);
}

std::unique_ptr<PasswordInput>
Application::BuildInputHandler(const std::string& storage)
{
  std::string path = Helper::ExpandPath(storage);
  auto pred = std::bind(IsPasswordValid, path, *_config, _1);

  bool create = !Helper::FileExists(path);
  return BuildInputHandlerImpl(create, pred);
}

std::unique_ptr<PasswordInput>
Application::BuildInputHandlerImpl(bool create,
                                   const PasswordInput::CheckRoutine& pred)
{
  return std::make_unique<input::ConsolePasswordInput>(pred, create);
}

const char* Application::GetConfigPath() const
{
  if (!_config_path_override.empty())
  {
    return _config_path_override.data();
  }
  return defs::default_config_path;
}

const char* Application::GetStoragePath() const
{
  if (!_storage_path_override.empty())
  {
    return _storage_path_override.data();
  }

  if (!_config) // When Storage path is queried before Run() is called
  {
    return defs::default_storage_path;
  }
  return _config->GetStoragePath().data();
}

bool Application::ImportStorage(const char* path)
{
  // Retrieve base storage key and open it
  auto storage = BuildStorage(GetStoragePath(), false);

  Storage::ConflictPred username_pred;
  Storage::ConflictPred password_pred;

  if (!_disable_import_timestamp)
  {
    username_pred = std::bind(
        &Application::ImportConflictWithTimestamp, true, _1, _2, _3, _4, _5);

    password_pred = std::bind(
        &Application::ImportConflictWithTimestamp, false, _1, _2, _3, _4, _5);
  }
  else
  {
    username_pred = std::bind(&Application::ImportConflict, true, _1, _2, _3);

    password_pred = std::bind(&Application::ImportConflict, false, _1, _2, _3);
  }

  std::vector<std::pair<SecuredString, SecuredString>> imported_creds;

  if (ImportStorageImpl(
          *storage, path, username_pred, password_pred, imported_creds))
  {
    DisplayImportResult(imported_creds, std::cerr);
  }

  return true;
}

void Application::DisplayImportResult(
    const std::vector<std::pair<SecuredString, SecuredString>>& imported,
    std::ostream& out)
{
  if (imported.empty())
  {
    out << "No credentials imported" << std::endl;
    return;
  }

  out << "Imported credentials for: " << std::endl;
  for (const auto& e : imported)
  {
    out << "\t - " << e.first << " - " << e.second << std::endl;
  }
}

bool Application::ImportStorageImpl(
    Storage& storage,
    const char* path,
    const Storage::ConflictPred& username_pred,
    const Storage::ConflictPred& password_pred,
    std::vector<std::pair<SecuredString, SecuredString>>& imported_creds)
{
  if (path != nullptr) // If we are importing from a storage file
  {
    auto import_storage = BuildStorage(path, false);
    return storage.Import(
        *import_storage, username_pred, password_pred, &imported_creds);
  }
  else // Import from stdin
  {
    auto import_credentials = data::Serialization::ReadCredentials(std::cin);

    return storage.Import(
        import_credentials, username_pred, password_pred, &imported_creds);
  }
}

Storage::ConflictAction
Application::ImportConflictWithTimestamp(bool user,
                                         const SecuredString& domain,
                                         const SecuredString& current,
                                         const SecuredString& imported,
                                         size_t current_ts,
                                         size_t imported_ts)
{
  if (current_ts > imported_ts)
  {
    std::cout << "Kept our credentials for: " << domain
              << ". Our version was edited last" << std::endl;

    return Storage::ConflictAction::KeepCurrent;
  }
  else if (current_ts < imported_ts)
  {
    std::cout << "Imported credentials for: " << domain
              << ". Imported version was edited last" << std::endl;

    return Storage::ConflictAction::Overwrite;
  }

  std::cout << "Credentials for: " << domain
            << " have same timestamps, but content differ" << std::endl;

  return ImportConflict(user, domain, current, imported);
}

Storage::ConflictAction
Application::ImportConflict(bool user,
                            const SecuredString& domain,
                            const SecuredString& current,
                            const SecuredString& imported)
{
  std::cout << "Conflict: different " << (user ? "usernames" : "passwords");
  std::cout << " for domain: \"" << domain << "\"" << std::endl;

  if (user)
  {
    std::cout << "\tUsername currently in storage:: \"" << current << "\"\n";
    std::cout << "\tImported username: \"" << imported << "\"\n";
  }

  char action = '\0';

  while (action != 'k' && action != 'c' && action != 'o')
  {
    std::cout << "Overwrite(o), keep current(k), show clear text(s)  or "
                 "cancel operation(c)?"
              << std::endl;

    std::cin >> action;

    if (action == 's')
    {
      std::cout << "\tCurrent: \"" << current << "\"\n\tImported: \""
                << imported << "\"" << std::endl;
    }
  }

  return static_cast<Storage::ConflictAction>(action);
}

bool Application::ShowHelp() const
{
  std::cerr << "BluePasswordManager, version: " << defs::version << "\n";
  std::cerr << "\tBPM is a password manager that puts credentials in your "
               "clipboard when needed\n";
  if (!HasGUISupport())
  {
    std::cerr << "\tNote: BPM has been built WITHOUT GUI support\n";
  }

  std::cerr << "\nCommand line arguments: \n";

  for (const auto& e : _arguments)
  {
    std::cerr << "\t" << e.first;
    if (e.second.takes_value)
    {
      std::cerr << " [value]";
    }

    std::cerr << ": " << e.second.description << std::endl;
  }

  return true;
}

bool Application::ClearKey() const
{
  auto key_handler = GetPersistenceHandler();

  if (key_handler)
  {
    key_handler->Clear();
  }
  else
  {
    std::cerr << "BPM has been built without persistence support" << std::endl;
    return false;
  }

  return true;
}

bool Application::Display(bool username, const std::string& url)
{
  _storage = BuildStorage(GetStoragePath());

  SecuredString user;
  SecuredString password;
  auto domain = Helper::GetDomainAuthority(url);

  if (!_storage->ReadCred(domain, user, password))
  {
    std::cerr << "No credentials stored for domain: " << domain << std::endl;
    return false;
  }

  std::cout << (username ? user : password);

  return true;
}

bool Application::Export()
{
  _storage = BuildStorage(GetStoragePath());

  data::Serialization::Write(_storage->GetCredentials(), std::cout);

  return true;
}

bool Application::GeneratePassword() const
{
  std::cout << Helper::GeneratePassword(_config->GetPasswordGenerationCharset(),
                                        _config->GetPasswordGenerationSize())
            << std::endl;;

  return true;
}

bool Application::HasGUISupport() const
{
  return false;
}

void Application::PrepareConfig(ConfigManager&)
{
}

bool Application::ExportStorageEncryptionKey()
{
  auto storage = BuildStorage(GetStoragePath());

  auto* encrypted_storage = dynamic_cast<data::SecuredStorage*>(storage.get());
  if (encrypted_storage == nullptr)
  {
    std::cerr << "Storage is not encrypted" << std::endl;
    return false;
  }

  auto key = encryption::Encryptor::Base64Encode(encrypted_storage->Key());

  return !!(std::cout << key);
}

std::unique_ptr<SecuredVector> Application::FindKeyInEnvironement()
{
  const auto* key = getenv(defs::env_key_variable_name);
  if (key == nullptr || *key == '\0')
  {
    return {};
  }

  // TODO: Replace by std::optional when moving this project to c++17
  return std::make_unique<SecuredVector>(
      encryption::Encryptor::Base64Decode(SecuredString{key}));
}
