#pragma once

namespace bpm
{
  namespace seed
  {

#ifdef _WIN32
#ifndef SEED_GEN
#include "seed.h"
#endif
#endif

#ifdef RANDOM_BLOB
    static const constexpr char* random_blob = RANDOM_BLOB;
    // Make sure the entropy is correct
    static_assert(sizeof(RANDOM_BLOB) > 64, "blob entropy is strong");
#else

#ifndef SEED_GEN
#ifdef _WIN32
#pragma message("Random blob not set, persistence data will not be encrypted")
#else
#warning "Random blob not set, persistence data will not be encrypted"
#endif
#endif
    static const constexpr char* random_blob = "";
#endif
  } // namespace seed
} // namespace bpm
