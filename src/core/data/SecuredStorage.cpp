#include <fstream>
#include "SecuredStorage.h"
#include "Helper.h"

using bpm::data::SecuredStorage;
using bpm::data::SecuredString;
using bpm::data::SecuredVector;
using bpm::encryption::Encryptor;

SecuredStorage::SecuredStorage(const std::string& file,
                               size_t key_size,
                               const SecuredString& password)
    : Storage(file, false), _encryptor(key_size, password)
{
  Init(file);
}

SecuredStorage::SecuredStorage(const std::string& file,
                               const SecuredVector& key)
    : Storage(file, false), _encryptor(key)
{
  Init(file);
}

void SecuredStorage::Init(const std::string& file)
{
  // Create the storage file if it does not exist yet
  if (!Helper::FileExists(file))
  {
    InitFolders(file); // We cannot do this in the parent class
                       // because a virtual function call inside a ctor
                       // would not come down in the hierarchy
  }
}

bool SecuredStorage::Good() const
{
  std::ifstream file(GetFilePath());
  if (!file) // File does not exist (yet), it will be created
  {
    return true;
  }

  String header;
  if (!std::getline(file, header) || header.empty())
  {
    return false;
  }

  return _encryptor.HashMatch(header);
}

std::string SecuredStorage::CreateHeaderBlob() const
{
  return _encryptor.GenerateKeyHashBase64();
}

SecuredStorage::String SecuredStorage::Encode(const String& input) const
{
  return _encryptor.EncryptBase64(input);
}

SecuredStorage::String SecuredStorage::Decode(const String& input) const
{
  return _encryptor.DecryptBase64(input);
}

bool SecuredStorage::Encrypted() const
{
  return true;
}

SecuredVector SecuredStorage::Key() const
{
  return _encryptor.Key();
}
