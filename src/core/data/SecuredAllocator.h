#pragma once

#include <string>

namespace bpm
{
  namespace data
  {
    template <typename T>
    class SecuredAllocator : public std::allocator<T>
    {
    public:
      SecuredAllocator() = default;
      SecuredAllocator(const SecuredAllocator&) = default;
      SecuredAllocator(SecuredAllocator&&) = default;
      SecuredAllocator(const std::allocator<T>& other);

      const SecuredAllocator<T>& operator=(const std::allocator<T>& other);
      T* allocate(size_t count, const void* hint = nullptr);

      void deallocate(T* ptr, size_t size);
    };
  }
}

#include "SecuredAllocator.hxx"
