#pragma once

#include <functional>
#include <iostream>
#include <vector>

#include "Defs.h"
#include "Credentials.h"
#include "SecuredString.h"

namespace bpm
{
  namespace data
  {
    class Storage
    {
    public:
      enum class ConflictAction : char
      {
        KeepCurrent = 'k',
        Overwrite = 'o',
        Cancel = 'c'
      };

      using String = SecuredString;

      using CredPred = std::function<bool(
          const String&, const String&, const String&, size_t)>;

      Storage(const std::string& file, bool init = true);
      virtual ~Storage() = default;

      // A predicate to be called in case of import conflict
      // Parameters: domain, current username, imported username
      // Return value: -1: keep current
      //                1: take imported values
      //                0: cancel import

      using ConflictPred = std::function<ConflictAction(
          const String&, const String&, const String&, size_t, size_t)>;

      bool AddCred(const String& domain,
                   const String& user,
                   const String& password,
                   size_t ts,
                   bool overwrite);

      // Changes the username but keeps the saved password
      void ChangeUsername(const String& domain, const String& user, size_t ts);

      // Changes password, keeps saved username
      void ChangePassword(const String& domain,
                          const String& new_password,
                          size_t ts);

      // Changes domain, keeps saved username and password
      void ChangeDomain(const String& olddomain,
                        const String& newdomain,
                        size_t ts,
                        bool overwrite = false);

      bool ReadCred(const String& domain,
                    String& user,
                    String& password,
                    size_t* ts = nullptr) const;

      void DeleteCred(const String& domain);

      bool HasCred(const String& domain);

      virtual bool Encrypted() const;

      // Returns true if all predicates returned true
      // If one predicates returns false
      // The enumeration stops and return value is false
      bool Enumerate(const CredPred& pred) const;

      // Check that storage is valid
      virtual bool Good() const;

      const std::string& GetFilePath() const;

      // Return true if the storage file is being created
      bool IsBeingCreated() const;

      // Imports a new storage, and merges values into current one
      // returns false if import has been canceled
      // username_conflict is called in case of two domains with differents
      // usernames
      // password_conflict is called in case of two domains, with same users,
      // with different passwords
      // imported contains, if non null, the list of imported domains and users
      bool Import(const Storage& storage,
                  const ConflictPred& username_conflict,
                  const ConflictPred& password_conflict,
                  std::vector<std::pair<String, String>>* imported = nullptr);

      bool Import(const std::vector<Credentials>& credentials,
                  const ConflictPred& username_conflict,
                  const ConflictPred& password_conflict,
                  std::vector<std::pair<String, String>>* imported = nullptr);

      std::vector<Credentials> GetCredentials() const;

      // Returns true if is a subset of storage
      // A subset of a storage contains at least all value
      // stored in his superset
      bool SubsetOf(const Storage& storage) const;

      bool operator==(const Storage& other) const;
      bool operator!=(const Storage& other) const;

      static bool IsSymlink(const String& username);

    protected:
      virtual std::string CreateHeaderBlob() const;

      void InitFolders(const std::string& file);

    private:
      bool ReadCredImpl(const String& domain,
                        String& user,
                        String& password,
                        size_t* ts = nullptr) const;


      String GetCredLine(const String& url) const;

      virtual String Encode(const String& encrypted) const;
      virtual String Decode(const String& plain) const;

      void ReadBlob(const String& blob,
                    String* domain,
                    String* user = nullptr,
                    String* password = nullptr,
                    size_t* ts = nullptr) const;


      void FollowSymlink(String& username, String& password) const;

      std::ofstream CreateDataCopy(const std::string& target,
                                   const String& exclude) const;

      void ApplyConfigChange(const std::string& source);

      String CreateBlob(const String& domain,
                        const String& user,
                        const String& password,
                        size_t ts) const;

      bool ForAllBlobs(const std::string& file,
                       std::function<bool(const String&)> pred,
                       bool defaultval = true) const;

      bool ImportCredentials(const Credentials& credentials,
                             const ConflictPred& username_conflict,
                             const ConflictPred& password_conflict);

      static std::vector<String> SplitBlob(const String& blob);

      std::string _file;
      bool _was_just_created = false;
    };
  }
}
