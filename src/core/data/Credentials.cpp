#include "Credentials.h"

using bpm::data::Credentials;
using bpm::data::SecuredString;

Credentials::Credentials(const SecuredString& domain,
                         const SecuredString& username,
                         const SecuredString& password,
                         size_t ts)
    : ts(ts)
{
  this->domain = domain;
  this->username = username;
  this->password = password;
}
