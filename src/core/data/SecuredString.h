#pragma once

#include "SecuredAllocator.h"

namespace bpm
{
  namespace data
  {
#ifndef ANDROID
    using SecuredString =
        std::basic_string<char, std::char_traits<char>, SecuredAllocator<char>>;

    using SecuredStringStream = std::basic_stringstream<char,
                                                        std::char_traits<char>,
                                                        SecuredAllocator<char>>;

    inline std::string operator+(const char* left, const SecuredString& right)
    {
      return std::string(left) + right.c_str();
    }

    inline std::string operator+(const SecuredString& left, const char* right)
    {
      return left.c_str() + std::string(right);
    }

    inline std::string operator+(const std::string& left,
                                 const SecuredString right)
    {
      return left.c_str() + right;
    }

    inline std::string operator+(const SecuredString& left,
                                 const std::string& right)
    {
      return left + right.c_str();
    }

#else
    using SecuredString = std::string;

    using SecuredStringStream = std::stringstream;

#endif
  }
}
