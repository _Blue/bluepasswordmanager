#pragma once

#include <vector>
#include "SecuredAllocator.h"

namespace bpm
{
  namespace data
  {
#ifndef ANDROID
    using SecuredVector =
        std::vector<unsigned char, SecuredAllocator<unsigned char>>;
#else
    using SecuredVector = std::vector<unsigned char>;
#endif
  }
}
