#pragma once

#include "SecuredString.h"

namespace bpm
{
  namespace data
  {
    struct Credentials
    {
      Credentials(const SecuredString& domain,
                  const SecuredString& username,
                  const SecuredString& password,
                  size_t ts);

      Credentials(const Credentials&) = default;
      Credentials(Credentials&&) = default;

      SecuredString domain;
      SecuredString username;
      SecuredString password;
      size_t ts;
    };
  }
}
