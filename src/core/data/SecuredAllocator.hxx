#pragma once

#include "SecuredAllocator.h"

namespace bpm
{
  namespace data
  {
    template <typename T>
    inline SecuredAllocator<T>::SecuredAllocator(const std::allocator<T>& other)
        : std::allocator<T>(other)
    {
    }

    template <typename T>
    inline const SecuredAllocator<T>& SecuredAllocator<T>::
    operator=(const std::allocator<T>& other)
    {
      static_cast<std::allocator<T>&>(*this) = other;
    }

    template <typename T>
    inline T* SecuredAllocator<T>::allocate(size_t size, const void* hint)
    {
      return std::allocator<T>::allocate(size, hint);
    }

    template <typename T>
    inline void SecuredAllocator<T>::deallocate(T* ptr, size_t size)
    {
      memset(ptr, 0, size * sizeof(T));

      std::allocator<T>::deallocate(ptr, size);
    }
  }
}
