#pragma once

#include "Storage.h"

namespace bpm
{
  namespace data
  {
    class Serialization
    {
    public:
      static void Write(const std::vector<Credentials>& credentials,
                        std::ostream& out);

      static std::vector<Credentials> ReadCredentials(std::istream& in);

    private:
      static void Write(const Credentials& credentials, std::ostream& out);
      static void Write(const SecuredString& value, std::ostream& out);
      static void Write(uint64_t value, std::ostream& out);
      static void WriteHeader(const std::vector<Credentials>& credentials,
                              std::ostream& out);

      static Credentials ReadCredential(std::istream& in);
      static SecuredString ReadString(std::istream& in);
      static uint64_t ReadInteger(std::istream& in);
      static std::pair<uint64_t, uint64_t> ReadHeader(std::istream& in);

      static void ThrowIfFalse(bool value, const std::string& message);
    };
  }
}
