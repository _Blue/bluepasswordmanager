#include <cassert>

#include <cstdio>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <set>

#include "StorageException.h"
#include "CancelException.h"
#include "Helper.h"
#include "Storage.h"

using bpm::data::Credentials;
using bpm::data::Storage;
using namespace std::placeholders;

bool Storage::IsSymlink(const Storage::String& username)
{
  return username == bpm::defs::symlink_prefix;
}

Storage::Storage(const std::string& file, bool init) : _file(file)
{
  // The below initialization is a duplicate of the SecuredStorage ctor
  // This is done so that a Storage will also initialize correctly
  // if directory tree is not existing
  //
  // Moreover, factorization is not possible because it would imply
  // the call of virtual function from super class inside ctor

  if (init && !Helper::FileExists(file))
  {
    InitFolders(file);
  }
}

void Storage::InitFolders(const std::string& file)
{
  _was_just_created = true;

  // Create directory tree
  size_t slashpos = file.find_last_of("/");
  if (slashpos == std::string::npos)
    return;

  std::string folder = file.substr(0, slashpos);

  Helper::CreateFolder(folder);

  // Make sure that we won't override an existing storage
  if (Helper::FileExists(file))
  {
    throw StorageException("Failed to create empty storage: file exists", file);
  }

  // Write the file
  std::ofstream copy = CreateDataCopy(file, "");
}

bool Storage::AddCred(const String& domain,
                      const String& user,
                      const String& password,
                      size_t ts,
                      bool overwrite)
{
  if (domain.empty())
  {
    throw StorageException("Can't add an empty domain name", _file);
  }

  if (!overwrite && HasCred(domain))
  {
    return false;
  }

  // Copy all credentials in another file, except the domain that we want
  auto filename = _file + defs::copy_suffix;
  std::ofstream copy = CreateDataCopy(filename, domain);

  String newblob = CreateBlob(domain, user, password, ts);

  copy << newblob << '\n';
  copy.close();

  ApplyConfigChange(filename);

  return true;
}

bool Storage::ReadCred(const String& domain,
                       String& username,
                       String& password,
                       size_t* ts) const
{
  if (!ReadCredImpl(domain, username, password, ts))
  {
    return false;
  }

  if (IsSymlink(username))
  {
    FollowSymlink(username, password);
  }

  return true;
}

bool Storage::ReadCredImpl(const String& domain,
                           String& username,
                           String& password,
                           size_t* ts) const
{
  String blob = GetCredLine(domain);

  if (blob.empty())
  {
    return false;
  }

  ReadBlob(blob, nullptr, &username, &password, ts);

  return true;
}

void Storage::FollowSymlink(String& username, String& password) const
{
  std::set<String> visited;

  while (IsSymlink(username))
  {
    if (visited.find(password) != visited.end())
    {
      throw StorageException(
          "Infinite symlink recursion following: '" + username + "'", _file);
    }

    auto blob = GetCredLine(password);
    if (blob.empty())
    {
      throw StorageException("Broken symlink: '" + password + "'", _file);
    }

    visited.emplace(std::move(password));

    ReadBlob(blob, nullptr, &username, &password);
  }
}

void Storage::DeleteCred(const String& domain)
{
  std::string filename = _file + defs::copy_suffix;
  std::ofstream copy = CreateDataCopy(filename, domain);
  copy.close();

  ApplyConfigChange(filename);
}

bool Storage::HasCred(const String& domain)
{
  auto pred = [&domain, this](const String& blob) {
    String blobdomain;
    ReadBlob(blob, &blobdomain);

    return domain != blobdomain;
  };

  return !ForAllBlobs(_file, pred, true);
}

void Storage::ChangeUsername(const String& domain,
                             const String& user,
                             size_t ts)
{
  // First read current password
  String olduser;
  String password;
  if (!ReadCredImpl(domain, olduser, password))
  {
    throw StorageException("Can't change user,  domain not found: " + domain,
                           _file);
  }

  AddCred(domain, user, password, ts, true);
}

void Storage::ChangePassword(const String& domain,
                             const String& new_password,
                             size_t ts)
{
  // First read current user name
  String user;
  String oldpassword;
  if (!ReadCredImpl(domain, user, oldpassword))
  {
    throw StorageException("Can't change username: domain not found: " + domain,
                           _file);
  }

  AddCred(domain, user, new_password, ts, true);
}

void Storage::ChangeDomain(const String& olddomain,
                           const String& newdomain,
                           size_t ts,
                           bool overwrite)
{
  // First read current password
  String user;
  String password;

  if (!ReadCredImpl(olddomain, user, password))
  {
    throw StorageException("ChangeDomain: domain not found: " + olddomain,
                           _file);
  }

  // Create new value with new domain
  if (!AddCred(newdomain, user, password, ts, overwrite))
  {
    throw StorageException("ChangeDomain: Can't rename " + olddomain + " to " +
                               newdomain + ", target already exists",
                           _file);
    ;
  }

  // Delete old value
  DeleteCred(olddomain);
}

Storage::String Storage::GetCredLine(const String& domain) const
{
  String result;
  auto pred = [&domain, &result, this](const String& blob) {
    String blobdomain;
    ReadBlob(blob, &blobdomain);

    bool ret = blobdomain == domain;
    if (ret)
    {
      result = blob;
    }
    return !ret;
  };

  ForAllBlobs(_file, pred, true);

  return result;
}

Storage::String Storage::Encode(const String& blob) const
{
  return blob;
}

Storage::String Storage::Decode(const String& plain) const
{
  return plain;
}

std::vector<Storage::String> Storage::SplitBlob(const String& blob)
{
  std::vector<String> output;

  String token;

  data::SecuredStringStream str(blob);
  while (std::getline(str, token, ':'))
  {
    output.emplace_back(std::move(token));
  }

  return output;
}

void Storage::ReadBlob(const String& blob,
                       String* domain,
                       String* user,
                       String* password,
                       size_t* ts) const
{
  // Blob format:
  // encrypt(domain):encrypt(user):encrypt(passord):encrypt(ts)\n

  auto tokens = SplitBlob(blob);
  if (tokens.size() < 3)
  {
    throw StorageException("Failed to read blob: \"" + blob + "\"", _file);
  }

  if (domain != nullptr)
  {
    *domain = Decode(tokens[0]);
  }

  if (user != nullptr)
  {
    *user = Decode(tokens[1]);
  }

  if (password != nullptr)
  {
    *password = Decode(tokens[2]);
  }

  if (ts != nullptr)
  {
    if (tokens.size() ==
        3) // Can happen with storages created with an old version
    {
      std::cerr << "No timestamp for domain: " << *domain << ", defaulting to 0"
                << std::endl;
      *ts = 0;
    }
    else
    {
      *ts = std::stoul(Decode(tokens[3]).c_str());
    }
  }
}

std::ofstream Storage::CreateDataCopy(const std::string& target,
                                      const String& exclude) const
{

  if (Helper::FileExists(target))
  {
    throw StorageException(
        "Temporary ghost file already exist: \"" + target + "\"", _file);
  }

  std::ofstream file(target);
  if (!file)
  {
    throw StorageException("Failed to open ghost file for writting: \"" +
                               target + "\", " + Helper::GetLastErrorMessage(),
                           _file);
  }

  if (!(file << CreateHeaderBlob() << '\n')) // Add header first
  {
    throw StorageException("IO error while writing header: " +
                               Helper::GetLastErrorMessage(),
                           _file);
  }

  auto pred = [this, &file, &exclude](const String& blob) {
    String domain;
    ReadBlob(blob, &domain);
    if (domain == exclude)
      return true;

    if (!(file << blob << '\n'))
    {
      throw StorageException("IO error while writing blob: " +
                                 Helper::GetLastErrorMessage(),
                             _file);
    }
    return true;
  };

  ForAllBlobs(_file, pred);

  return file;
}

void Storage::ApplyConfigChange(const std::string& source)
{
  Helper::Rename(source, _file);
}

Storage::String Storage::CreateBlob(const String& domain,
                                    const String& user,
                                    const String& password,
                                    size_t ts) const
{
  SecuredStringStream str;
  str << Encode(domain) << ':' << Encode(user) << ':' << Encode(password) << ':'
      << Encode(std::to_string(ts).c_str());

  return str.str();
}

std::string Storage::CreateHeaderBlob() const
{
  return "Plain";
}

bool Storage::Good() const
{
  return true;
}

bool Storage::ForAllBlobs(const std::string& filename,
                          std::function<bool(const String&)> pred,
                          bool defaultval) const
{
  std::ifstream file(filename);
  if (!file) // Consider failure to open the file means an empty file
  {
    return defaultval;
  }

  String blob;
  if (!getline(file, blob)) // Discard header blob
  {
    throw StorageException("IO error while reading storage header: " +
                               Helper::GetLastErrorMessage(),
                           _file);
  }

  bool empty = true;
  while (getline(file, blob) && !blob.empty())
  {
    empty = false;
    if (!pred(blob))
    {
      return false;
    }
  }

  if (empty)
  {
    return defaultval;
  }

  return true;
}

bool Storage::Enumerate(const Storage::CredPred& routine) const
{
  auto pred = [&routine, this](const String& blob) {
    String domain;
    String user;
    String password;
    size_t ts;
    ReadBlob(blob, &domain, &user, &password, &ts);
    return routine(domain, user, password, ts);
  };

  return ForAllBlobs(_file, pred, false);
}

bool Storage::Encrypted() const
{
  return false;
}

const std::string& Storage::GetFilePath() const
{
  return _file;
}

bool Storage::IsBeingCreated() const
{
  return _was_just_created;
}

bool Storage::ImportCredentials(const Credentials& credentials,
                                const ConflictPred& username_conflict,
                                const ConflictPred& password_conflict)
{
  String current_user;
  String current_password;
  size_t current_ts = 0;

  // See if we already have that domain
  if (!this->ReadCredImpl(
          credentials.domain, current_user, current_password, &current_ts))
  {
    return true; // We don't, so no conflict
  }

  if (current_user == credentials.username &&
      current_password == credentials.password)
  {
    return false; // No change, nothing to import
  }

  ConflictAction action = ConflictAction::Cancel;

  if (current_user != credentials.username)
  {
    // username conflicts
    action = username_conflict(credentials.domain,
                               current_user,
                               credentials.username,
                               current_ts,
                               credentials.ts);
  }
  else // if password is different but username is the same
  {
    action = password_conflict(credentials.domain,
                               current_password,
                               credentials.password,
                               current_ts,
                               credentials.ts);
  }

  if (action == ConflictAction::Cancel)
  {
    throw CancelException();
  }

  return action == ConflictAction::Overwrite;
}

std::vector<Credentials> Storage::GetCredentials() const
{
  std::vector<Credentials> credentials;

  auto pred = [&credentials](const String& domain,
                             const String& username,
                             const String& password,
                             size_t ts) {
    credentials.emplace_back(domain, username, password, ts);
    return true;
  };

  Enumerate(pred);

  return credentials;
}

bool Storage::Import(const Storage& storage,
                     const ConflictPred& username_conflict,
                     const ConflictPred& password_conflict,
                     std::vector<std::pair<String, String>>* imported)
{
  return Import(
      storage.GetCredentials(), username_conflict, password_conflict, imported);
}

bool Storage::Import(const std::vector<Credentials>& credentials,
                     const ConflictPred& username_conflict,
                     const ConflictPred& password_conflict,
                     std::vector<std::pair<String, String>>* imported)

{
  // Save the values to insert in a vector
  std::vector<Credentials> new_values;

  auto pred = [&](const Credentials& creds) {
    try
    {
      if (ImportCredentials(creds, username_conflict, password_conflict))
      {
        new_values.push_back(creds);
      }

      return true;
    }
    catch (const CancelException&)
    {
      return false;
    }
  };

  // Read imported storage
  if (!std::all_of(credentials.begin(), credentials.end(), pred))
  {
    return false; // Operation cancelled
  }

  // Conflicts are now solved, proceed actual import
  for (const auto& e : new_values)
  {
    AddCred(e.domain, e.username, e.password, e.ts, true);
    if (imported != nullptr)
    {
      imported->emplace_back(e.domain, e.username);
    }
  }

  return true; // Import is successful
}

bool Storage::SubsetOf(const Storage& storage) const
{
  auto pred = [this](const String& domain,
                     const String& username,
                     const String& password,
                     size_t ts) {
    String local_username;
    String local_password;
    size_t local_ts = 0;
    return ReadCredImpl(domain, local_username, local_password, &local_ts) &&
           local_username == username && local_password == password &&
           local_ts == ts;
  };

  return storage.Enumerate(pred);
}

bool Storage::operator==(const Storage& other) const
{
  return SubsetOf(other) && other.SubsetOf(*this);
}

bool Storage::operator!=(const Storage& other) const
{
  return !(other == *this);
}
