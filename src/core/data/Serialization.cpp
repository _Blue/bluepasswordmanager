#include <cassert>
#include <cstring>

#include "Serialization.h"
#include "Defs.h"
#include "BPMException.h"

using bpm::data::Serialization;
using bpm::data::Credentials;
using bpm::data::SecuredString;
using bpm::BPMException;

void Serialization::Write(const std::vector<Credentials>& credentials,
                         std::ostream& out)
{
  WriteHeader(credentials, out);

  for (const auto& e : credentials)
  {
    Write(e, out);
  }
}

void Serialization::Write(const Credentials& credentials, std::ostream& out)
{
  Write(credentials.domain, out);
  Write(credentials.username, out);
  Write(credentials.password, out);
  Write(credentials.ts, out);
}

void Serialization::Write(const SecuredString& value, std::ostream& out)
{
  Write(value.size(), out);
  out.write(value.data(), value.size());
}

void Serialization::Write(uint64_t value, std::ostream& out)
{
  ThrowIfFalse(
      out.write(reinterpret_cast<const char*>(&value), sizeof(value)).good(),
      "Error while writing data");
}

void Serialization::WriteHeader(const std::vector<Credentials>& credentials,
                               std::ostream& out)
{
  Write(defs::storage_version, out);
  Write(credentials.size(), out);
}

std::vector<Credentials> Serialization::ReadCredentials(std::istream& in)
{
  auto header = ReadHeader(in);

  if (header.first != defs::storage_version)
  {
    throw BPMException("Incompatible storage version. Expected " +
                       std::to_string(defs::storage_version) + ", got " +
                       std::to_string(header.first));
  }

  std::vector<Credentials> credentials;

  for (uint64_t i = 0; i < header.second; i++)
  {
    credentials.emplace_back(ReadCredential(in));
  }

  return credentials;
}

Credentials Serialization::ReadCredential(std::istream& in)
{
  SecuredString domain(ReadString(in));
  SecuredString username(ReadString(in));
  SecuredString password(ReadString(in));
  auto ts = ReadInteger(in);

  return {domain, username, password, static_cast<size_t>(ts)};
}

SecuredString Serialization::ReadString(std::istream& in)
{
  auto size = ReadInteger(in);

  SecuredString value(static_cast<unsigned int>(size), '\0');

  ThrowIfFalse(in.read(const_cast<char*>(value.data()), size).good(),
               "Error while reading data");

  assert(value.size() == strlen(value.data()));

  return value;
}

uint64_t Serialization::ReadInteger(std::istream& in)
{
  uint64_t value = 0;

  ThrowIfFalse(in.read(reinterpret_cast<char*>(&value), sizeof(value)).good(),
               "Error while reading data");

  return value;
}

std::pair<uint64_t, uint64_t> Serialization::ReadHeader(std::istream& in)
{
  auto version = ReadInteger(in);
  auto size = ReadInteger(in);

  return std::make_pair(version, size);
}

void Serialization::ThrowIfFalse(bool value, const std::string& message)
{
  if (value)
  {
    return;
  }

  throw BPMException(message);
}
