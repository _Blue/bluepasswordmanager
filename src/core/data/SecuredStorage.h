#pragma once

#include "Storage.h"
#include "encryption/Encryptor.h"

namespace bpm
{
  namespace data
  {
    class SecuredStorage : public Storage
    {
    public:
      SecuredStorage(const std::string& file,
                     size_t key_size,
                     const data::SecuredString& password);

      SecuredStorage(const std::string& file,
                     const SecuredVector& key);

      virtual String Encode(const String& plain) const override;
      virtual String Decode(const String& plain) const override;
      virtual bool Encrypted() const override;
      // Check that storage is valid and password is correct
      virtual bool Good() const override;

      SecuredVector Key() const;

    protected:
      virtual std::string CreateHeaderBlob() const override;

    private:
      void Init(const std::string& file);
      encryption::Encryptor _encryptor;
    };
  } // namespace data
} // namespace bpm
