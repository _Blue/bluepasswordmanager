#pragma once

#include <string>
#include <map>
#include <functional>
#include <set>
#include <optional>

#include "Defs.h"
namespace bpm
{
  class ConfigManager
  {
  public:
    ConfigManager();

    void Load(const std::string& file);

    void SetStoragePath(const std::string& path);

    const std::string& GetEncryptionAlgorithm() const;
    const std::string& GetStoragePath() const;
    const std::string& GetPasswordGenerationCharset() const;
    size_t GetPasswordGenerationSize() const;
    size_t GetEncryptionKeySize() const;
    bool IsDaemonModeEnabled() const;
    bool IsBannerEnabled() const;
    bool IsLibSecretKeyStoreEnabled() const;
    const std::set<std::string>& GetPasteKeys() const;
    const std::set<std::string>& GetExitKeys() const;
    const std::pair<size_t, size_t>& GetBannerOffset() const;
    const std::optional<std::pair<size_t, size_t>>& GetBannerPosition() const;
    void SetExitKeys(const std::string& keys);

    static size_t DefaultEncryptionKeySize();

  private:
    using LineHandler = std::function<void(const std::string&)>;

    void ExecuteLine(std::string& line,
                     const std::map<std::string, LineHandler>& handlers);
    static void CleanLine(std::string& line);
    std::map<std::string, LineHandler> GetHandlers();
    void SetPasteKeys(const std::string& keys);
    static std::set<std::string> StringToKeys(const std::string& text);
    static size_t ReadInteger(const std::string& text);
    static std::pair<size_t, size_t> ReadIntegerPair(const std::string& text);

    bool _enable_daemon = defs::default_daemon_enabled;
    bool _enable_banner = true;
    bool _enable_libsecret = false;
    std::string _encryption_algorithm{defs::default_encryption_algorithm};
    std::string _storage_path{defs::default_storage_path};
    size_t _key_size = defs::default_encryption_key_size;
    size_t _password_generation_size = defs::default_password_generation_size;
    std::string _password_generation_charset =
        defs::default_password_generation_charset;
    std::set<std::string> _pastekeys;
    std::set<std::string> _exitkeys;
    std::pair<size_t, size_t> _banner_offset{defs::default_banner_offset,
                                             defs::default_banner_offset};

    std::optional<std::pair<size_t, size_t>> _banner_position;
  };
}
