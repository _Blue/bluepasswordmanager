#pragma once

#include "Defs.h"
#include "data/SecuredVector.h"

namespace bpm
{
  namespace persistence
  {
    class KeyHandler
    {
    public:
      KeyHandler(size_t key_size);
      virtual ~KeyHandler() = default;

      virtual bool IsKeyAvailable() = 0;
      virtual void Clear() = 0;
      data::SecuredVector RetrieveKey();
      void SaveKey(const data::SecuredVector& key);

    protected:
      virtual data::SecuredVector
      Read(size_t max = defs::persistence_max_blob) = 0;
      virtual void Write(const data::SecuredVector& data) = 0;

    private:
      size_t _key_size = 0;
    };
  } // namespace persistence
} // namespace bpm
