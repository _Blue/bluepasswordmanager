#include "KeyHandler.h"
#include "encryption/Encryptor.h"
#include "RandomBlob.h"

using bpm::data::SecuredVector;
using bpm::encryption::Encryptor;
using bpm::persistence::KeyHandler;

KeyHandler::KeyHandler(size_t key_size) : _key_size(key_size)
{
}

SecuredVector KeyHandler::RetrieveKey()
{ 
  Encryptor cryptor(_key_size, seed::random_blob);

  auto data = Read();

  cryptor.Decrypt(data);

  return data;
}

void KeyHandler::SaveKey(const SecuredVector& key)
{
  Encryptor cryptor(_key_size, seed::random_blob);

  auto blob = key;

  cryptor.Encrypt(blob);

  Write(blob);
}
