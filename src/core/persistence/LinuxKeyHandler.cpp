#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

#include "LinuxKeyHandler.h"
#include "BPMException.h"

using bpm::data::SecuredVector;
using bpm::persistence::LinuxKeyHandler;

LinuxKeyHandler::LinuxKeyHandler(size_t key_size) : KeyHandler(key_size)
{
}

bool LinuxKeyHandler::IsKeyAvailable()
{
  int fd = OpenSHM(O_RDONLY);

  if (fd != -1)
  {
    close(fd);
    return true;
  }

  return false;
}

int LinuxKeyHandler::OpenSHM(int permissions)
{
  return shm_open(defs::shm_name, permissions, S_IRUSR | S_IWUSR);
}

void LinuxKeyHandler::Write(const SecuredVector& data)
{
  int fd = OpenSHM(O_WRONLY | O_TRUNC | O_CREAT);

  if (fd == -1 ||
      write(fd, data.data(), data.size()) != static_cast<int>(data.size()))
  {
    throw BPMException("Failed to write to SHM");
  }

  close(fd);
}

SecuredVector LinuxKeyHandler::Read(size_t len)
{
  SecuredVector data(len);

  int fd = OpenSHM(O_RDONLY);

  if (fd == -1)
  {
    throw BPMException("Failed to open SHM");
  }

  int datasize = read(fd, data.data(), len);

  close(fd);
  if (len <= 0)
  {
    throw BPMException("Failed to read from SHM");
  }

  data.resize(datasize);

  return data;
}

void LinuxKeyHandler::Clear()
{
  int ret = shm_unlink(defs::shm_name);
  // Consider that an unexisting SHM is not an error when deleting it

  if (ret != 0 && errno != ENOENT)
  {
    throw BPMException("Failed to remove SHM");
  }
}
