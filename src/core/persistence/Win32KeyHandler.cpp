#define WIN32_LEAN_AND_MEAN
#include <Windows.h>

#include "Win32KeyHandler.h"
#include "BPMException.h"
#include "input/Win32/SyscallWrapper.h"

static const HKEY key_root = HKEY_CURRENT_USER; // Can't be constexpr
static const DWORD key_value_type = REG_BINARY;
static const constexpr char* key_path = "Software\\BlueSoftware\\BPM";

using bpm::data::SecuredVector;
using bpm::input::SyscallWithExpectedReturn;
using bpm::persistence::Win32KeyHandler;

namespace
{
  HKEY OpenKey()
  {
    HKEY key = nullptr;
    if (RegOpenKeyExA(key_root, key_path, 0, KEY_ALL_ACCESS, &key))
    {
      throw bpm::BPMException("Failed to open registry key");
    }

    return key;
  }

  HKEY CreateKey()
  {
    // The 'REG_OPTION_VOLATILE' allows not to save the value on the disk
    // so that the data written in this key gets dropped
    // when the system is rebooted
    HKEY key = nullptr;
    SyscallWithExpectedReturn(RegCreateKeyExA,
                              0,
                              key_root,
                              key_path,
                              0,
                              nullptr,
                              REG_OPTION_VOLATILE,
                              KEY_ALL_ACCESS,
                              nullptr,
                              &key,
                              nullptr);

    return key;
  }

} // namespace

Win32KeyHandler::Win32KeyHandler(size_t key_size) : KeyHandler(key_size)
{
}

bool Win32KeyHandler::IsKeyAvailable()
{
  try
  {
    RegCloseKey(OpenKey());
  }
  catch (const bpm::BPMException&)
  {
    return false;
  }

  return true;
}

void Win32KeyHandler::Clear()
{
  long ret = RegDeleteKeyA(key_root, key_path);

  // Consider deletion of non-existing key is not an error
  if (ret != 0 && ret != ERROR_FILE_NOT_FOUND)
  {
    throw bpm::BPMException("Failed to delete persistence data");
  }
}

SecuredVector Win32KeyHandler::Read(size_t max)
{
  HKEY key = OpenKey();

  SecuredVector data(max);

  DWORD value_type = 0;
  DWORD size = static_cast<DWORD>(max);
  SyscallWithExpectedReturn(
      RegQueryValueExA, 0, key, "", nullptr, &value_type, data.data(), &size);
  RegCloseKey(key);

  if (value_type != key_value_type)
  {
    throw bpm::BPMException(
        "Persistence data is invalid (bad registry format)");
  }

  data.resize(size);
  return data;
}

void Win32KeyHandler::Write(const SecuredVector& data)
{
  HKEY key = CreateKey();

  SyscallWithExpectedReturn(
      RegSetValueExA, 0, key, "", 0, key_value_type, data.data(), static_cast<DWORD>(data.size()));

  RegCloseKey(key);
}
