#pragma once

#include "persistence/KeyHandler.h"

namespace bpm
{
  namespace persistence
  {
    class Win32KeyHandler : public KeyHandler
    {
    public:
      Win32KeyHandler(size_t key_size);

      virtual bool IsKeyAvailable() override;
      virtual void Clear() override;

    protected:
      virtual data::SecuredVector
      Read(size_t max = defs::persistence_max_blob) override;
      virtual void Write(const data::SecuredVector& data) override;
    };
  } // namespace persistence
} // namespace bpm
