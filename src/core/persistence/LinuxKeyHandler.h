#pragma once

#include "persistence/KeyHandler.h"

namespace bpm
{
  namespace persistence
  {
    class LinuxKeyHandler: public KeyHandler
    {
    public:
      LinuxKeyHandler(size_t key_size);

      virtual bool IsKeyAvailable() override;
      virtual void Clear() override;

    protected:
      virtual data::SecuredVector Read(size_t max) override;
      virtual void Write(const data::SecuredVector& data) override;

    private:
      int OpenSHM(int permissions);
    };
  }
}
