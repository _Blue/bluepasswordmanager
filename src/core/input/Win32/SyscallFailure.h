#pragma once

#include <stdexcept>

namespace bpm
{
  namespace input
  {
    class SyscallFailure : public std::exception
    {
    public:
      SyscallFailure(const std::string& message);

      const char* what() const noexcept override;
    private:
      std::string _message;
    };
  }
}

