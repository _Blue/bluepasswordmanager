#pragma once

#include <type_traits>
#include "SyscallFailure.h"

namespace bpm
{
  namespace input
  {

    template <typename Routine, typename ...Args>
    typename std::invoke_result<Routine, Args...>::type Syscall(Routine routine, Args... args);

    template <typename Routine, typename ...Args>
    typename std::invoke_result<Routine, Args...>::type
      SyscallWithError(Routine routine, typename std::invoke_result<Routine, Args...>::type error_value, Args... args);

    template <typename Routine, typename ...Args>
    typename std::invoke_result<Routine, Args...>::type
      SyscallWithExpectedReturn(Routine routine, typename std::invoke_result<Routine, Args...>::type expected_value, Args... args);
  }
}
#include "SyscallWrapper.hxx"
