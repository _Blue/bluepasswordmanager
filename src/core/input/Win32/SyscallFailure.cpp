#include "SyscallFailure.h"

using bpm::input::SyscallFailure;

SyscallFailure::SyscallFailure(const std::string& message): 
  _message(message)
{
}

const char* SyscallFailure::what() const noexcept
{
  return _message.c_str();
}