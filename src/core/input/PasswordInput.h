#pragma once

#include <functional>
#include <string>
#include "data/SecuredString.h"

namespace bpm
{
  namespace input
  {
    class PasswordInput
    {
    public:
      using CheckRoutine = std::function<bool(const data::SecuredString&)>;
      PasswordInput(const CheckRoutine& routine, bool create);
      virtual ~PasswordInput() = default;

      virtual data::SecuredString QueryPassword(const std::string& storage) = 0;

    protected:
      CheckRoutine _checkpassword;
      bool _create = false;
    };
  }
}
