#include "PasswordInput.h"

namespace bpm
{
  namespace input
  {
    class ConsolePasswordInput : public PasswordInput
    {
    public:
      ConsolePasswordInput(const CheckRoutine& routine, bool create);
      virtual ~ConsolePasswordInput() = default;

      virtual data::SecuredString
      QueryPassword(const std::string& storage) override;
    };
  }
}
