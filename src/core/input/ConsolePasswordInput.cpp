#include <iostream>
#include "ConsolePasswordInput.h"
#include "CancelException.h"

#if defined(_WIN32) or defined(ANDROID)

static std::string password;

static const char* getpass(const char* prompt)
{
  std::cerr << prompt << std::endl;
  if (!std::getline(std::cin, password))
  {
    return nullptr;
  }

  return password.c_str();
}

#else
#include <unistd.h>
#endif

using bpm::input::ConsolePasswordInput;

ConsolePasswordInput::ConsolePasswordInput(
    const ConsolePasswordInput::CheckRoutine& routine, bool create)
    : PasswordInput(routine, create)
{
}

bpm::data::SecuredString
ConsolePasswordInput::QueryPassword(const std::string& storage)
{
  data::SecuredString password;

  std::string prompt = "Password required to unlock \"" + storage + "\":";

  do
  {
    const char* input = getpass(prompt.c_str());
    if (input == nullptr || *input == '\0')
    {
      throw CancelException();
    }

    password = input;
  } while (!_checkpassword(password));

  return password;
}
