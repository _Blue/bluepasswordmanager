#include "PasswordInput.h"

using bpm::input::PasswordInput;

PasswordInput::PasswordInput(const CheckRoutine& routine, bool create)
    : _checkpassword(routine), _create(create)
{
}
