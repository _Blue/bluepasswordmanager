#pragma once

#include <cstddef>
namespace bpm
{
  namespace encryption
  {
    class HashEngine
    {
      public:
        HashEngine() = default;

        size_t DigestSize() const;

        void Hash(const unsigned char *data, size_t len, unsigned char *buffer) const;
    };
  }
}
