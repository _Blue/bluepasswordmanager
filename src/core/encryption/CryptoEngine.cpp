#include <cryptopp/rng.h>
#include <cryptopp/randpool.h>
#include <cryptopp/osrng.h>
#include <cryptopp/aes.h>
#include <cryptopp/modes.h>
#include <cryptopp/pwdbased.h>
#include <cstring>

#include "CryptoEngine.h"
#include "Defs.h"

using namespace CryptoPP;

using bpm::encryption::CryptoEngine;
using bpm::data::SecuredVector;

CryptoEngine::CryptoEngine(const char* password, size_t keylen)
{
  _key.resize(keylen);
  DeriveKey(password, _key.data(), keylen);
}

void CryptoEngine::DeriveKey(const char* password,
                             unsigned char* buffer,
                             size_t keylen)
{
  PKCS5_PBKDF2_HMAC<SHA512> generator;

  // Perform key derivation
  generator.DeriveKey(buffer,
                      keylen,
                      0,
                      reinterpret_cast<const byte*>(password),
                      strlen(password),
                      reinterpret_cast<const byte*>(defs::encryption_salt),
                      strlen(defs::encryption_salt),
                      static_cast<unsigned int>(defs::hmac_iterations));
}

CryptoEngine CryptoEngine::FromKey(const unsigned char* key, size_t len)
{
  CryptoEngine engine;

  engine._key = SecuredVector(key, key + len);
  return engine;
}

void CryptoEngine::Encrypt(unsigned char* data,
                           size_t len,
                           const unsigned char* iv) const
{
  CFB_Mode<AES>::Encryption encryptor(_key.data(), _key.size(), iv);
  encryptor.ProcessData(data, data, len);
}

void CryptoEngine::Decrypt(unsigned char* data,
                           size_t len,
                           const unsigned char* iv) const
{
  CFB_Mode<AES>::Decryption decryptor(_key.data(), _key.size(), iv);
  decryptor.ProcessData(data, data, len);
}

void CryptoEngine::GenerateRandomBlock(unsigned char* data, size_t len)
{
  AutoSeededRandomPool randompool;
  randompool.GenerateBlock(data, len);
}

size_t CryptoEngine::BlockSize()
{
  return AES::BLOCKSIZE;
}

size_t CryptoEngine::GetKeyLen() const
{
  return _key.size();
}

const unsigned char* CryptoEngine::GetKey() const
{
  return _key.data();
}
