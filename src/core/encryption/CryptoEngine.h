#pragma once

#include <cstddef>
#include "data/SecuredVector.h"

namespace bpm
{
  namespace encryption
  {
    class CryptoEngine
    {
    public:
      CryptoEngine(const char* password, size_t keylen);

      static CryptoEngine FromKey(const unsigned char* key_, size_t keylen);

      void Encrypt(unsigned char* data, size_t len,
                   const unsigned char* iv) const;

      void Decrypt(unsigned char* data, size_t len,
                   const unsigned char* iv) const;

      static void GenerateRandomBlock(unsigned char* data, size_t len);

      static void DeriveKey(const char* password, unsigned char* buffer,
                            size_t len);

      const unsigned char* GetKey() const;
      size_t GetKeyLen() const;

      static size_t BlockSize();

    private:
      data::SecuredVector _key;
      CryptoEngine() = default;
    };
  }
}
