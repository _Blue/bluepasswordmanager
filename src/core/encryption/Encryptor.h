#pragma once

#include <string>
#include <vector>
#include <memory>

#include "CryptoEngine.h"
#include "data/SecuredString.h"
#include "data/SecuredVector.h"

namespace bpm
{
  namespace encryption
  {
    class Encryptor
    {
    public:
      Encryptor(size_t key_size, const data::SecuredString& password);
      Encryptor(const data::SecuredVector& key);

      // Input is clear text, output is base64(encrypted)
      data::SecuredString EncryptBase64(const data::SecuredString& data) const;

      // Input is base64(encrypted), output is clear text
      data::SecuredString DecryptBase64(const data::SecuredString& data) const;

      void Encrypt(data::SecuredVector& data) const;
      void Decrypt(data::SecuredVector& data) const;

      // returns salted hash of the encryption key in base64 format
      // if the salt is empty, the salt will be generated inside the function
      std::string GenerateKeyHashBase64() const;

      // Check that the provided hash matches the key we have
      bool HashMatch(const data::SecuredString& blob) const;

      data::SecuredVector DeriveKey(const data::SecuredString& password);

      static data::SecuredString Base64Encode(const data::SecuredVector& data);
      static data::SecuredVector Base64Decode(const data::SecuredString& data);

      data::SecuredVector Key() const;

    private:
      data::SecuredVector
      ComputeSaltedHash(const data::SecuredVector& salt) const;

      size_t _key_size = 0;
      CryptoEngine _engine;
    };
  } // namespace encryption
} // namespace bpm
