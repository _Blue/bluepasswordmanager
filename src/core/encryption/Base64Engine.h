#pragma once

#include <cstddef>
namespace bpm
{
  namespace encryption
  {
    class Base64Engine
    {
    public:
      static void Encode(const unsigned char* data, size_t datalen,
                         char* buffer, size_t& bufferlen);

      static void Decode(const char* base64str, unsigned char* data,
                         size_t& datalen);

      // Returns the number of bytes needed to encode 'datasize' bytes
      static size_t GetEncodedSize(size_t datasize);

      // Returns the number of byted needed to decode 'datasize' bytes
      static size_t GetDecodedSize(size_t encoded);
    };
  }
}
