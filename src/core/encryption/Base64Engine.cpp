#include <cryptopp/base64.h>
#include <cmath>
#include "Base64Engine.h"
#include "BPMException.h"

using namespace CryptoPP;
using bpm::encryption::Base64Engine;

void Base64Engine::Encode(const unsigned char* data, size_t datalen,
                          char* buffer, size_t& bufferlen)
{
  Base64Encoder encoder(nullptr, false); // Don't put LF in base64
  encoder.Put(data, datalen);
  encoder.MessageEnd();

  size_t size = static_cast<size_t>(encoder.MaxRetrievable());
  if (size > bufferlen)
  {
    throw BPMException("Buffer size too short to retrieve base64 value");
  }

  encoder.Get(reinterpret_cast<byte*>(buffer), size);
  bufferlen = size;
}

void Base64Engine::Decode(const char* base64str, unsigned char* data,
                          size_t& datalen)
{
  Base64Decoder decoder;
  decoder.Put(reinterpret_cast<const byte*>(base64str), strlen(base64str));
  decoder.MessageEnd();

  size_t size = static_cast<size_t>(decoder.MaxRetrievable());

  if (size > datalen)
  {
    throw BPMException("Buffer too short to retrieve data from base64");
  }

  decoder.Get(data, datalen);
  datalen = size;
}

size_t Base64Engine::GetEncodedSize(size_t size)
{
  return static_cast<size_t>(std::ceil(size / 3.) * 4 + 1);
}

size_t Base64Engine::GetDecodedSize(size_t size)
{
  return static_cast<size_t>(size * 4 * 3 + 1);
}
