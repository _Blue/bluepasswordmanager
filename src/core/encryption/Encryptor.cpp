#include <cmath>
#include <sstream>
#include <cassert>
#include <cstring>
#include "Encryptor.h"
#include "Defs.h"
#include "BPMException.h"
#include "Base64Engine.h"
#include "HashEngine.h"

using bpm::data::SecuredString;
using bpm::data::SecuredVector;
using bpm::encryption::Encryptor;

Encryptor::Encryptor(size_t key_size, const SecuredString& password)
    : _key_size(key_size), _engine(password.data(), key_size / 8)

{
}

Encryptor::Encryptor(const SecuredVector& key)
    : _key_size(key.size() * 8),
      _engine(CryptoEngine::FromKey(key.data(), key.size()))
{
}

SecuredVector Encryptor::DeriveKey(const SecuredString& password)
{
  SecuredVector key;

  size_t len = _key_size / 8; // Key size is in bits
  key.resize(len);

  _engine.DeriveKey(password.data(), key.data(), len);

  return key;
}

void Encryptor::Encrypt(SecuredVector& block) const
{
  // Generate IV
  SecuredVector iv(_engine.BlockSize());
  _engine.GenerateRandomBlock(iv.data(), iv.size());

  // Perform actual encryption
  // On some STL implementations, vector.data()
  // can return nullptr when the vector is empty.
  // This makes cryptopp throw an exception
  // this special case handling corrects the issue
  if (!block.empty())
  {
    _engine.Encrypt(block.data(), block.size(), iv.data());
  }

  // Add IV at the beginning of the block
  block.insert(block.begin(), iv.begin(), iv.end());
}

void Encryptor::Decrypt(SecuredVector& block) const
{
  // Build decryptor using begin of block as IV

  _engine.Decrypt(block.data() + _engine.BlockSize(),
                  block.size() - _engine.BlockSize(),
                  block.data());

  // Remove IV from block
  block.erase(block.begin(), block.begin() + _engine.BlockSize());
}

SecuredString Encryptor::EncryptBase64(const SecuredString& data) const
{
  // Encrypt block
  SecuredVector block(data.begin(), data.end());
  Encrypt(block);

  // Encode result
  return Base64Encode(block);
}

SecuredString Encryptor::DecryptBase64(const SecuredString& data) const
{
  // Recover block from base64
  auto block = Base64Decode(data);

  // Perform decryption
  Decrypt(block);

  // Build string back
  return {block.begin(), block.end()};
}

SecuredString Encryptor::Base64Encode(const SecuredVector& data)
{
  SecuredString result(Base64Engine::GetEncodedSize(data.size()), '\0');

  size_t resultsize = result.size();
  Base64Engine encoder;
  encoder.Encode(
      data.data(), data.size(), const_cast<char*>(result.data()), resultsize);

  assert(resultsize <= result.size());
  result.resize(resultsize);

  return result;
}

SecuredVector Encryptor::Base64Decode(const SecuredString& base64)
{
  SecuredVector block(Base64Engine::GetDecodedSize(base64.size()));

  Base64Engine decoder;
  size_t size = block.size();
  decoder.Decode(base64.data(), block.data(), size);
  block.resize(size);

  return block;
}

std::string Encryptor::GenerateKeyHashBase64() const
{
  // Generate salt
  SecuredVector salt(defs::hash_salt_size);
  _engine.GenerateRandomBlock(salt.data(), defs::hash_salt_size);

  // Compute hash
  auto hashvalue = ComputeSaltedHash(salt);

  // Build blob
  std::stringstream str;
  str << Base64Encode(salt) << ':' << Base64Encode(hashvalue);

  return str.str();
}

SecuredVector Encryptor::ComputeSaltedHash(const SecuredVector& salt) const
{
  SecuredVector block(salt.begin(), salt.end());
  // append key to salt
  const unsigned char* key = _engine.GetKey();
  block.insert(block.end(), key, key + _engine.GetKeyLen());

  // Compute hash
  HashEngine hashengine;
  SecuredVector hashvalue(hashengine.DigestSize());
  hashengine.Hash(block.data(), block.size(), hashvalue.data());

  return hashvalue;
}

bool Encryptor::HashMatch(const SecuredString& blob) const
{
  // Data format is:
  // base64(salt):base64(hash(salt + key))
  size_t sep = blob.find(':');

  if (sep == std::string::npos || sep == 0 || sep == blob.size())
  {
    throw BPMException("Invalid header in storage");
  }

  // Check that there is no more ':'
  if (blob.find(':', sep + 1) != std::string::npos)
  {
    throw BPMException("Invalid header in storage");
  }

  // Decode salt
  auto salt = Base64Decode(blob.substr(0, sep));

  // Decode hash value
  auto hashvalue = Base64Decode(blob.substr(sep + 1));

  return ComputeSaltedHash(salt) == hashvalue;
}

SecuredVector Encryptor::Key() const
{
  SecuredVector key(_engine.GetKeyLen(), '\0');
  memcpy(key.data(), _engine.GetKey(), _engine.GetKeyLen());

  return key;
}
