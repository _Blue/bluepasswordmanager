#include <cryptopp/sha.h>
#include "HashEngine.h"

using namespace CryptoPP;

using bpm::encryption::HashEngine;

size_t HashEngine::DigestSize() const
{
  return 64;
}

void HashEngine::Hash(const unsigned char* data, size_t len,
                      unsigned char* output) const
{
  SHA512 hashfunction;

  hashfunction.CalculateDigest(output, data, len);
}
