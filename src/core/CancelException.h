#pragma once

#include <stdexcept>

namespace bpm
{
  class CancelException : public std::exception
  {
  public:
    CancelException() = default;
  };
}
