#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <Shlobj.h>
#else
#include <sys/stat.h>
#include <unistd.h>
#endif

#include <algorithm>
#include <iostream>
#include <sstream>
#include <fstream>
#include <time.h>
#include <iomanip>
#include <string.h>
#include <cryptopp/randpool.h>
#include <cryptopp/osrng.h>
#include "Helper.h"
#include "BPMException.h"
#include "Defs.h"

using bpm::Helper;
using bpm::data::SecuredString;

bool Helper::FileExists(const std::string& path)
{
  if (FolderExists(path))
  {
    return false;
  }

  std::ifstream file;
  file.open(path);

  return file.good();
}

#ifdef WIN32
void Helper::CreateFolderImpl(const std::string& path)
{
  std::string ospath(path);

  FixOsPath(ospath);

  if (!CreateDirectoryA(path.data(), nullptr))
  {
    throw BPMException("Failed to create directory");
  }
}

bool Helper::FolderExists(const std::string& path)

{
  DWORD attributes = GetFileAttributesA(path.data());

  return attributes != INVALID_FILE_ATTRIBUTES &&
         attributes & FILE_ATTRIBUTE_DIRECTORY;
}

void Helper::Remove(const std::string& path)
{
  std::string ospath(path);

  FixOsPath(ospath);

  if (!RemoveDirectoryA(ospath.data()) &&
      GetLastError() != ERROR_FILE_NOT_FOUND)
  {
    throw BPMException("Failed to remove file or folder");
  }
}

void Helper::FixOsPath(std::string& path)
{
  std::replace(path.begin(), path.end(), '/', '\\');
}

#else

void Helper::CreateFolderImpl(const std::string& path)
{
  if (mkdir(path.data(), defs::file_permissions) != 0)
  {
    throw BPMException("Creation of directory failed");
  }
}

bool Helper::FolderExists(const std::string& path)

{
  struct stat folder;
  if (stat(path.data(), &folder) != 0)
  {
    return false;
  }
  return S_ISDIR(folder.st_mode);
}

void Helper::Remove(const std::string& path)
{
  if (std::remove(path.data()) == 0)
    return;

  // Consider that the deletion of non-existing file is not an error
  if (errno != 2)
  {
    throw BPMException("Failed to remove file or folder");
  }
}

void Helper::FixOsPath(std::string&)
{
}

#endif

void Helper::CreateFolder(const std::string& path)
{
  if (path.size() == 0)
  {
    throw BPMException("Invalid path");
  }

  if (FolderExists(path))
    return;

  size_t slash = path.find("/", 1); // We do not want to create '/'

  // Create parents of the wanted folder
  while (slash != std::string::npos)
  {
    std::string abspath = path.substr(0, slash);

    if (!FolderExists(abspath.data()))
    {
      CreateFolderImpl(abspath.data());
    }
    slash = path.find("/", slash + 1);
  }

  // Then actually create the folder
  CreateFolderImpl(path.data());
}

std::string Helper::ExpandPath(const std::string& path)
{
  std::stringstream str;

  for (auto const& e : path)
  {
    if (e == '~')
    {
      str << getenv(defs::home_var);
    }
    else
    {
      str << e;
    }
  }
  return str.str();
}

SecuredString Helper::GetDomainAuthority(const std::string& url)
{
  // Possible data: https://bluecode.fr/static/home.jpg
  // We want to isolate: bluecode.fr

  // First remove the protocol prefix

  SecuredString result;
  size_t pos = url.find("://");
  // Do someting without considering the protocol
  if (pos != std::string::npos)
  {
    result = url.substr(pos + 3).c_str();
  }
  else
  {
    result = url.c_str();
    pos = 0;
  }

  if (result.empty()) // if had an input equivalent to "http://" -> invalid
  {
    throw BPMException("Invalid url: \"" + url + "\"");
  }

  // remove what may be after the slash
  pos = result.find('/', pos);

  if (pos != std::string::npos && pos != 0)
  {
    result = result.substr(0, pos);
  }

  return result;
}

void Helper::Rename(std::string oldname, std::string newname)
{
  FixOsPath(oldname);
  FixOsPath(newname);

// On windows, std::rename fails when the target file exists, so prefer
// MoveFileEx
#ifdef _WIN32
  bool success =
      MoveFileExA(oldname.data(), newname.data(), MOVEFILE_REPLACE_EXISTING) ==
      TRUE;
#else
  bool success = std::rename(oldname.data(), newname.data()) == 0;
#endif

  if (!success)
  {
    throw BPMException("Failed to rename file: " + oldname + "to: " + newname);
  }
}

time_t Helper::Now()
{
  return time(nullptr);
}

std::string Helper::TimeString(time_t ts)
{
  tm val;
#ifdef _WIN32
  localtime_s(&val, &ts);
#else
  localtime_r(&ts, &val);
#endif

  std::ostringstream str;
  str << std::put_time(&val, defs::time_format);

  return str.str();
}

#ifdef _WIN32
void Helper::CenterWindow(void* input)
{
  HWND window = reinterpret_cast<HWND>(input);
  size_t screen_x = GetSystemMetrics(SM_CXFULLSCREEN);
  size_t screen_y = GetSystemMetrics(SM_CYFULLSCREEN);

  RECT rect = {0};
  if (!GetWindowRect(window, &rect))
  {
    throw std::runtime_error("GetWindowRect failed: " +
                             std::to_string(GetLastError()));
  }

  int window_x =
      static_cast<int>(std::round(screen_x / (float)2 - (rect.right - rect.left) / (float)2));
  int window_y =
      static_cast<int>(std::round(screen_y / (float)2 - (rect.bottom - rect.top) / (float)2));

  if (!SetWindowPos(
          window, nullptr, window_x, window_y, 0, 0, SWP_NOSIZE | SWP_NOZORDER))
  {
    throw std::runtime_error("SetWindowPos failed: " +
                             std::to_string(GetLastError()));
  }
}
#endif //  _WIN32

std::string Helper::GetLastErrorMessage()
{
  return strerror(errno);
}

void Helper::AttachToConsole()
{
#ifdef _WIN32
  if (AttachConsole(ATTACH_PARENT_PROCESS) || AttachConsole(GetCurrentProcessId()))
  {
    freopen("CONIN$", "r", stdin);
    freopen("CONOUT$", "w", stderr);
    freopen("CONOUT$", "w", stdout);

    std::ios::sync_with_stdio();
    return; // Attached to parent process, we're good
  }

  // If both above syscall fails, it means that this process has no attached console
  // So nothing to do in the error case, there's no console to write to
#endif
}

SecuredString Helper::GeneratePassword(const std::string& charset, size_t length)
{
  CryptoPP::AutoSeededRandomPool pool;

  SecuredString output;
  for (size_t i = 0; i < length; i++)
  {
    auto word =  pool.GenerateWord32(0, static_cast<CryptoPP::word32>(charset.size() -1));

    output += charset[word];
  }

  return output;
}

