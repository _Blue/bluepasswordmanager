#pragma once

#include <stdexcept>
#include <string>

namespace bpm
{
  class BPMException : public std::exception
  {
  public:
    BPMException(const std::string& message);

    virtual const char *what() const noexcept override;

  private:
    std::string _message;

  };
}
