#pragma once

#include "Application.h"

namespace bpm
{
  template <typename T>
  inline void Application::AddValueArgument(const std::string& value,
                                     const std::string& description,
                                     T& target)
  {
    auto routine = [&target](const char* value) {
      target = value;
      return true;
    };

    _arguments.emplace(value,
                       Argument{true, true, false, routine, description});
  }

  template <>
  inline void Application::AddValueArgument<bool>(const std::string& value,
                                           const std::string& description,
                                           bool& target)
  {
    auto routine = [&target](const char*) {
      target = true;
      return true;
    };

    _arguments.emplace(value,
                       Argument{true, false, false, routine, description});
  }
}
