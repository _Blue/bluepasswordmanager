#include "StorageException.h"

using bpm::StorageException;

StorageException::StorageException(const std::string& message,
                                   const std::string& storage)
    : BPMException("Error in storage: \"" + storage + "\" : " + message),
      _storage(storage)
{
}

const std::string& StorageException::Storage() const noexcept
{
  return _storage;
}
