//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Resource.rc
//
#define IDD_ADDCRED                     101
#define IDD_QUERYPW                     111
#define IDD_DISPLAY                     113
#define IDD_BANNER                      114
#define IDM_DISPLAY                     117
#define IDC_OK                          1001
#define IDC_CANCEL                      1002
#define IDC_DNS                         1003
#define IDC_USER                        1004
#define IDC_PASSWORD                    1005
#define IDC_CANCEL2                     1006
#define IDC_GENERATEPW                  1006
#define IDC_ADDCRED_DOMAIN              1030
#define IDC_TEXT                        1031
#define IDC_PW_PASSWORD                 1031
#define IDC_DISPLAY_PATTERN             1031
#define IDC_BANNER_LABEL                1031
#define IDC_PW_STORAGE                  1032
#define IDC_PW_NOTICE                   1033
#define IDC_DISPLAY_SHOWPW              1034
#define IDC_DISPLAY_LIST                1035
#define IDC_DISPLAY_ADD                 1036
#define IDC_PW_CONFIRM                  1037
#define IDC_BUTTON1                     1038
#define IDC_SHOWLIST                    1038
#define IDC_SYMLINK                     1039
#define MENU_DELETE                     40001
#define MENU_EDIT                       40002
#define MENU_LOGIN                      40003
#define ID_DUMMY_CREATESYMLINK          40004
#define IDM_CREATE_SYMLINK              40005
#define MENU_CREATE_SYMLINK             40006

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        119
#define _APS_NEXT_COMMAND_VALUE         40007
#define _APS_NEXT_CONTROL_VALUE         1040
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
