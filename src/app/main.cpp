#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#include <shellapi.h>
#include "Helper.h"
#endif

#include <iostream>
#include "GUIApplication.h"
#include "CancelException.h"

int main(int argc, char** argv)
{
  bpm::GUIApplication app;

  try
  {
    app.ApplyArgv(argc, argv);
    return app.Run();
  }
  catch (const bpm::CancelException&)
  {
    return 1;
  }
  catch (const std::exception& e)
  {
    std::cerr << "bpm ended due to the following error: \n"
              << e.what() << std::endl;
  }
  return 1;
}

#ifdef _WIN32
// Using WinMain on Windows to hide the console
int CALLBACK WinMain(HINSTANCE hinstance, HINSTANCE, LPSTR, int)
{
  SetProcessDPIAware();
  bpm::Helper::AttachToConsole();

  return main(__argc, __argv);
}
#endif
