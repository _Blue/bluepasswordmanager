#pragma once



namespace bpm
{
  namespace test
  {
    class Colors
    {
    public:
      static const char *Green();
      static const char *Red();
      static const char *Blue();
      static const char *Bold();
      static const char *Reset();
    };
  }
}
