#include <iostream>
#include "Tester.h"

using std::cout;
using std::endl;

namespace bpm
{
  namespace test
  {
    Tester::Tester(const std::string& category) : _category(category)
    {
    }

    bool Tester::Run()
    {
      cout << Colors::Blue() << Colors::Bold() << "\n\t\t\t======= Tests for \""
           << _category << "\" =======" << endl
           << Colors::Reset();
      RunImpl();

      return _failed == 0;
    }

    void Tester::ShowResult() const
    {
      cout << Colors::Blue() << Colors::Bold()
           << "Test results for category: \"" << _category << "\""
           << Colors::Reset() << endl;

      if (_failed == 0)
      {
        cout << "\t " << Colors::Green() << "all passed (" << _passed
             << " tests)\n";
      }
      else
      {
        cout << "\t Passed: " << _passed << endl;
        cout << "\t " << Colors::Red() << "Failed: " << _failed << endl;
      }

      cout << Colors::Reset() << '\n';
    }

    bool Tester::TestThrow(const std::function<void()>& routine,
                           bool shouldthrow, const std::string& name)
    {
      size_t failed = _failed;
      try
      {
        routine();

        if (shouldthrow)
        {
          cout << Colors::Red() << "FAILED: " << name
               << " did not throw as expected" << endl;
          _failed++;
        }
        else
        {
          _passed++;
          cout << Colors::Green() << "PASSED: " << name << std::endl;
        }
      }
      catch (const std::exception& ex)
      {
        if (!shouldthrow)
        {
          cout << Colors::Red() << "FAILED: \"" << name
               << "\" because of exception: \"" << ex.what() << "\""
               << endl;
          _failed++;
        }
        else
        {
          _passed++;
          cout << Colors::Green() << "PASSED: " << name << std::endl;
        }
      }

      cout << Colors::Reset();

      return failed == _failed;
    }

    size_t Tester::Passed() const
    {
      return _passed;
    }

    size_t Tester::Failed() const
    {
      return _failed;
    }
  }
}
