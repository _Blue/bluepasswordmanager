#include <algorithm>
#include <cassert>
#include <fstream>
#include <functional>
#include <sstream>

#include "TestImport.h"
#include "Helper.h"
#include "data/SecuredStorage.h"
#include "data/Serialization.h"
#include "ConfigManager.h"

using namespace std::placeholders;
using bpm::data::SecuredStorage;
using bpm::data::SecuredString;
using bpm::data::Serialization;
using ConflictAction = bpm::data::Storage::ConflictAction;

namespace bpm
{
  namespace test
  {
    TestImport::TestImport() : Tester("Storage import")
    {
    }

    void TestImport::RunImpl()
    {
      // Run test with both encrypted and non encrypted storage
      _storage = std::make_unique<data::Storage>("storage_imported.clear");
      Test(_storage->IsBeingCreated(), true, "new storage creation state");

      RunTests();

      // Create encrypted storage
      _storage = std::make_unique<data::SecuredStorage>(
          "storage_imported.encrypted",
          ConfigManager::DefaultEncryptionKeySize(),
          "Password");

      Test(_storage->IsBeingCreated(), true, "new storage creation state");

      // Reset data shadow
      _content.clear();
      _imports.clear();
      RunTests();
      _content.clear();
      _imports.clear();

      RunResourceTest();
    }

    void TestImport::RunResourceTest()
    {
      CopyStorage("src/test/resources/base_storage", "storage.tmp");
      _storage = std::make_unique<data::Storage>("storage.tmp");
      Test(!_storage->IsBeingCreated() && _storage->Good(),
           true,
           "Base storage is correctly opened");

      data::Storage imported("src/test/resources/import_storage");
      Test(!imported.IsBeingCreated() && imported.Good(),
           true,
           "Imported storage is correctly opened");

      struct Import
      {
        SecuredString domain;
        ConflictType conflict;
        ConflictAction result;
      };

      std::vector<Import> imports{
          {"1.com", ConflictType::User, ConflictAction::Overwrite},
          {"2.com", ConflictType::User, ConflictAction::KeepCurrent},
          {"3.com", ConflictType::Password, ConflictAction::Overwrite},
          {"4.com", ConflictType::Password, ConflictAction::KeepCurrent}};

      auto find_pred = [](const SecuredString& domain, const auto& e) {
        return e.domain == domain;
      };

      auto pred = [&imports, &find_pred](const SecuredString& domain,
                                         const SecuredString&,
                                         const SecuredString&,
                                         size_t,
                                         size_t) {
        auto it = std::find_if(
            imports.begin(), imports.end(), std::bind(find_pred, domain, _1));

        assert(it != imports.end());

        return it->result;
      };

      _storage->Import(imported, pred, pred);

      // Load reference
      data::Storage expected("src/test/resources/expected");

      Test(expected == *_storage, true, "Imported storage equals ref");
      _storage.reset(nullptr);

      // Create another copy
      std::remove("storage.tmp");
      CopyStorage("src/test/resources/base_storage", "storage.tmp");

      // Add cancel to last import
      imports[3].result = ConflictAction::Cancel;

      // Perform import
      _storage = std::make_unique<data::Storage>("storage.tmp");
      _storage->Import(imported, pred, pred);

      // Load reference
      data::Storage base("src/test/resources/base_storage");
      Test(!base.IsBeingCreated() && base.Good(),
           true,
           "Imported storage is correctly opened");

      Test(*_storage == base,
           true,
           "Cancelling an import does not modify storage");
    }

    void TestImport::CopyStorage(const std::string& src, const std::string& dst)
    {
      std::ifstream in(src);
      std::ofstream out(dst);

      if (!in)
      {
        throw std::logic_error("Failed to open: " + src + " for reading");
      }
      if (!out)
      {
        throw std::logic_error("Failed to open: " + dst + " for writing");
      }

      out << in.rdbuf();
    }

    void TestImport::RunTests()
    {
      Add({"foo2.com", "foo2", "bar2", 2});
      Add({"foo3.com", "foo3", "bar3", 4});
      Check();

      // Basic import
      Import({"foo4.com", "foo", "bar", 6}, ConflictType::None);

      Check();

      // Basic conflict
      Import({"foo4.com", "foo2", "bar", 8},
             ConflictType::User,
             ConflictAction::KeepCurrent);
      Check();

      // Same as above, but overwrite
      Import({"foo4.com", "foo2", "bar", 10},
             ConflictType::User,
             ConflictAction::Overwrite);
      Check();

      // Password conflict
      Import({"foo4.com", "foo2", "bar2", 12},
             ConflictType::Password,
             ConflictAction::KeepCurrent);
      Check();

      // Password conflict, overwrite
      Import({"foo4.com", "foo2", "bar2", 14},
             ConflictType::Password,
             ConflictAction::Overwrite);
      Check();

      // No conflict, overwrite
      Import({"foo4.com", "foo2", "bar2", 16}, ConflictType::Same);

      // Symlink conflict, no overwrite
      Add({"linkto-foo2.com", bpm::defs::symlink_prefix, "foo2.com", 0});
      Import({"linkto-foo2.com", bpm::defs::symlink_prefix, "foo3.com", 16},
             ConflictType::Password,
             ConflictAction::KeepCurrent);
      Check();

      data::Storage::String username;
      data::Storage::String password;
      Test(_storage->ReadCred("linkto-foo2.com", username, password, nullptr),
           true,
           "Read symlink after import");
      Test<data::Storage::String>(
          username, "foo2", "Symlink username after import");
      Test<data::Storage::String>(
          password, "bar2", "Symlink username after import");

      Import({"linkto-foo2.com", bpm::defs::symlink_prefix, "foo3.com", 16},
             ConflictType::Password,
             ConflictAction::Overwrite);
      Check();

      Test(_storage->ReadCred("linkto-foo2.com", username, password, nullptr),
           true,
           "Read symlink after import 2");

      Test<data::Storage::String>(
          username, "foo3", "Symlink username after import 2");
      Test<data::Storage::String>(
          password, "bar3", "Symlink username after import 2");

      Check();

      TestSerialization();
    }

    void TestImport::TestSerialization()
    {
      data::SecuredStorage storage(
          "storage.tmp", ConfigManager::DefaultEncryptionKeySize(), "password");
      data::SecuredStorage storage2("storage.tmp2",
                                    ConfigManager::DefaultEncryptionKeySize(),
                                    "password");

      std::vector<data::Credentials> credentials{
          {"foo.com", "foo", "bar", 13},
          {"foo2.com", "empty", "", 14},
          {"foo3.com", "", "bar", 1337},
          {"foo4.com", defs::symlink_prefix, "foo.com", 1338},
      };

      for (const auto& e : credentials)
      {
        storage2.AddCred(e.domain, e.username, e.password, e.ts, true);
      }

      std::stringstream out;
      Serialization::Write(storage2.GetCredentials(), out);

      std::istringstream in(out.str());
      auto imported = Serialization::ReadCredentials(in);

      Test<size_t>(4, imported.size(), "Correct deserilaized size");

      for (size_t i = 0; i < credentials.size(); i++)
      {
        Test(credentials[i].domain,
             imported[i].domain,
             "Deserialized domain is correct");

        Test(credentials[i].username,
             imported[i].username,
             "Deserialized username is correct");

        Test(credentials[i].password,
             imported[i].password,
             "Deserialized password is correct");

        Test(credentials[i].ts,
             imported[i].ts,
             "Deserialized timestamp is correct");
      }
    }

    void TestImport::Import(const StorageContent& content,
                            ConflictType type,
                            ConflictAction result)
    {
      _imports.emplace_back(content, type, result);
    }

    void TestImport::ImportImpl(bool serialized)
    {
      for (const auto& e : _imports)
      {
        auto pred = [&e](const auto& elm) {
          return elm.domain == e.content.domain;
        };

        auto it = std::find_if(_content.begin(), _content.end(), pred);
        if (it != _content.end())
        {
          ImportContent(e, &*it, serialized);
        }
        else
        {
          ImportContent(e, nullptr, serialized);
        }
      }
      // Data is now imported
      _imports.clear();
    }

    bpm::data::Storage::ConflictPred
    TestImport::BuildPred(bool& called,
                          const SecuredString& expected_domain,
                          const SecuredString& expected_current,
                          const SecuredString& expected_imported,
                          size_t expected_current_ts,
                          size_t expected_imported_ts,
                          ConflictAction result)
    {
      return [result,
              expected_current_ts,
              expected_imported_ts,
              &called,
              &expected_current,
              &expected_domain,
              &expected_imported,
              this](const SecuredString& domain,
                    const SecuredString& current,
                    const SecuredString& imported,
                    size_t current_ts,
                    size_t imported_ts) {
        called = true;
        Test(domain, expected_domain, "Hook is called with correct domain");
        Test(current,
             expected_current,
             "Hook is called with correct current value");
        Test(imported,
             expected_imported,
             "Hook is called with correct imported value");

        Test(current_ts, expected_current_ts, "Hook is called with current ts");
        Test(imported_ts,
             expected_imported_ts,
             "Hook is called with correct imported ts");

        return result;
      };
    }

    void TestImport::ImportContent(const StorageImport& content,
                                   StorageContent* current,
                                   bool serialized)
    {
      data::Storage::ConflictPred username_pred;
      data::Storage::ConflictPred password_pred;
      bool username_called = false;
      bool password_called = false;

      bool username_should_be_called = false;
      bool password_should_be_called = false;
      if (content.conflict == ConflictType::None)
      {
        if (!Test(true,
                  current == nullptr,
                  "Non-conflicted import is not in storage"))
        {
          return;
        }
        username_pred = BuildPred(username_called);
        password_pred = BuildPred(password_called);
        _content.emplace_back(content.content);
      }
      else if (content.conflict == ConflictType::Same)
      {
        if (!Test(true,
                  current != nullptr,
                  "Non-conflicted import is not in storage"))
        {
          return;
        }
        username_pred = BuildPred(username_called);
        password_pred = BuildPred(password_called);
      }
      else
      {
        if (!Test(true, current != nullptr, "Conflicted import is in storage"))
        {
          return;
        }
        if (content.conflict == ConflictType::User)
        {
          username_pred = BuildPred(username_called,
                                    current->domain,
                                    current->user,
                                    content.content.user,
                                    current->ts,
                                    content.content.ts,
                                    content.result);
          password_pred = BuildPred(password_called);

          username_should_be_called = true;
        }
        else if (content.conflict == ConflictType::Password)
        {
          password_pred = BuildPred(password_called,
                                    current->domain,
                                    current->password,
                                    content.content.password,
                                    current->ts,
                                    content.content.ts,
                                    content.result);
          username_pred = BuildPred(username_called);

          password_should_be_called = true;
        }
      }

      std::remove("new_storage.tmp"); // Remove any old storage
      data::SecuredStorage new_storage(
          "new_storage.tmp",
          ConfigManager::DefaultEncryptionKeySize(),
          "password");

      new_storage.AddCred(content.content.domain,
                          content.content.user,
                          content.content.password,
                          content.content.ts,
                          false);

      std::vector<std::pair<SecuredString, SecuredString>> imported;
      if (serialized)
      {
        std::stringstream out;
        Serialization::Write(new_storage.GetCredentials(), out);

        std::istringstream in(out.str());

        _storage->Import(Serialization::ReadCredentials(in),
                         username_pred,
                         password_pred,
                         &imported);
      }
      else
      {
        _storage->Import(new_storage, username_pred, password_pred, &imported);
      }

      // Update shadow
      if (content.result == ConflictAction::Overwrite)
      {
        *current = content.content;
      }

      Test(username_called, username_should_be_called, "User Hook called");
      Test(password_called, password_should_be_called, "Password Hook called");

      if (content.conflict == ConflictType::None ||
          content.result == ConflictAction::Overwrite)
      {
        if (!Test<size_t>(
                imported.size(), 1, "Imported report contains one entry"))
        {
          return;
        }
        Test(imported[0].first,
             content.content.domain,
             "Imported report contains correct domain");
        Test(imported[0].second,
             content.content.user,
             "Imported report contains correct username");
      }
      else
      {
        Test<size_t>(imported.size(), 0, "Imported report contains no entry");
      }
    }

    void TestImport::Add(const StorageContent& content)
    {
      _storage->AddCred(
          content.domain, content.user, content.password, content.ts, true);
      _content.push_back(content);
    }

    void TestImport::Check()
    {
      ImportImpl(true);
      ValidateImportResult();

      ImportImpl(false);
      ValidateImportResult();
    }

    void TestImport::ValidateImportResult()
    {
      std::string type;

      if (_storage->Encrypted())
      {
        type = ", (encrypted storage)";
      }

      Test(true, _storage->Good(), "Storage is valid" + type);

      for (auto const& e : _content)
      {
        if (e.user == defs::symlink_prefix)
        {
          continue;
        }

        Test(_storage->HasCred(e.domain),
             true,
             "Storage has credentials (" + e.domain + ")" + type);

        SecuredString user;
        SecuredString password;
        Test(_storage->ReadCred(e.domain, user, password),
             true,
             "Read credentials (" + e.domain + ")" + type);

        Test(user, e.user, "User is valid" + type);
        Test(password, e.password, "Password is valid" + type);
      }
    }
  }
}
