#include <cstdio>
#include <memory>
#include <vector>

#ifdef _WIN32
#include <ShlObj.h>
#endif

#include "TestStorage.h"
#include "TestConfigManager.h"
#include "TestMain.h"
#include "TestURLParsing.h"
#include "TestHelper.h"
#include "TestEncryption.h"
#include "TestPersistence.h"
#include "TestImport.h"

using namespace bpm::test;

static void clean()
{
  std::remove("storage.clear");
  std::remove("storage.clear.update");
  std::remove("storage.encrypted.update");
  std::remove("storage.encrypted");
  std::remove("storage.encrypted");
  std::remove("storage_imported.clear");
  std::remove("storage_imported.encrypted");
  std::remove("new_storage.tmp");
  std::remove("storage.tmp");
  std::remove("storage.tmp2");

// Delete test folder
#ifdef _WIN32
  SHFILEOPSTRUCT file_op = {nullptr,
                            FO_DELETE,
                            "test_folder",
                            "",
                            FOF_NOCONFIRMATION | FOF_NOERRORUI | FOF_SILENT,
                            false,
                            0,
                            ""};
  SHFileOperation(&file_op);
#endif
}

static bool RunTestSuite(std::vector<std::unique_ptr<Tester>>& tests)
{
  clean();

  size_t passed = 0;
  size_t failed = 0;

  bool success = true;
  for (auto& e : tests)
  {
    success &= e->Run();
  }

  std::cout << Colors::Blue() << Colors::Bold()
            << "\n\n\t\t\t======= Results: =======" << std::endl;

  for (auto& e : tests)
  {
    e->ShowResult();
    passed += e->Passed();
    failed += e->Failed();
  }

  if (failed == 0)
  {
    std::cout << Colors::Green();
  }
  else
  {
    std::cout << Colors::Red();
  }

  std::cout << Colors::Bold();

  std::cout << "Total: " << passed << " / " << failed + passed
            << Colors::Reset() << std::endl;

  clean();
  return success;
}

int main()
{
  clean();

  std::vector<std::unique_ptr<Tester>> tests;

#ifndef ANDROID
  tests.emplace_back(std::make_unique<TestPersistence>());
#endif

  tests.emplace_back(std::make_unique<TestEncryption>());
  tests.emplace_back(std::make_unique<TestStorage>());
  tests.emplace_back(std::make_unique<TestConfigManager>());
  tests.emplace_back(std::make_unique<TestMain>());
  tests.emplace_back(std::make_unique<TestURLParsing>());
  tests.emplace_back(std::make_unique<TestHelper>());
  tests.emplace_back(std::make_unique<TestImport>());

  // Return 0 if all tests passed
  return !RunTestSuite(tests);
}
