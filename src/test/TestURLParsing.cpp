#include <vector>
#include "TestURLParsing.h"
#include "Helper.h"

using bpm::test::TestURLParsing;

TestURLParsing::TestURLParsing() : Tester("URL parsing")
{
}

void TestURLParsing::RunImpl()
{
  std::vector<std::pair<std::string, data::SecuredString>> cases{
      {"www.domain.com", "www.domain.com"},
      {"http://www.domain.com", "www.domain.com"},
      {"http://www.domain.com/url", "www.domain.com"},
      {"http://www.domain.com/url/file", "www.domain.com"},
      {"https://www.domain.com/url/file", "www.domain.com"},
      {"https://domain.com/url/file", "domain.com"},
      {"http://domain.com/url/file", "domain.com"},
      {"domain.com/url/file", "domain.com"},
      {"sub.domain.com/url/file", "sub.domain.com"},
      {"sub.domain.com/url/file/", "sub.domain.com"}};

  for (auto const& e : cases)
  {
    Test(Helper::GetDomainAuthority(e.first), e.second, e.first);
  }

  TestThrow(std::bind(&Helper::GetDomainAuthority, "http://"),
            true,
            "\"http://\" is not a valid url");
}
