#pragma once

#include "Tester.h"
#include "data/SecuredString.h"
#include "data/SecuredVector.h"

namespace bpm
{
  namespace test
  {
    class TestEncryption : public Tester
    {
    public:
      TestEncryption();

      virtual void RunImpl() override;

    private:
      void CheckStr(const data::SecuredString& data,
                    const data::SecuredString& password,
                    const std::string& name);
      void Check(const data::SecuredVector& data,
                 const data::SecuredString& password,
                 const std::string& name);

      static data::SecuredVector RandomBlock(size_t len);
    };
  }
}
