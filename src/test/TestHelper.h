#pragma once

#include "Tester.h"

namespace bpm
{
  namespace test
  {
    class TestHelper : public Tester
    {
    public:
     TestHelper();

      virtual void RunImpl() override;
    };
  }
}
