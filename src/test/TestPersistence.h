#pragma once

#include <memory>
#include "Tester.h"
#include "persistence/KeyHandler.h"

namespace bpm
{
  namespace test
  {
    class TestPersistence : public Tester
    {
    public:
      TestPersistence();

      virtual void RunImpl() override;

    private:
      void RunTests(persistence::KeyHandler& handler);

      void ReadWriteData(persistence::KeyHandler& handler,
                         const data::SecuredVector& data,
                         const std::string& name);
    };
  }
}
