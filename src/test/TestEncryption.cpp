#include <ctime>
#include <cstdlib>
#include <algorithm>
#include "Defs.h"
#include "TestEncryption.h"
#include "encryption/Encryptor.h"
#include "ConfigManager.h"

using bpm::data::SecuredString;
using bpm::data::SecuredVector;
using bpm::encryption::Encryptor;
using bpm::test::TestEncryption;

TestEncryption::TestEncryption() : Tester("Encryption")
{
}

void TestEncryption::RunImpl()
{
  Test(RandomBlock(42) != RandomBlock(42), true, "Random blank generation");

  Check({'a'}, "BlueRocks", "One byte encryption");
  Check({'a', 'b', 'c', 'd'}, "BlueRocks", "Multiple  byte encryption");

  Check(RandomBlock(50), "Sample", "Small random block");
  Check(RandomBlock(255), "Sample", "255 bytes random block");
  Check(RandomBlock(256), "Sample", "256 bytes random block");
  Check(RandomBlock(257), "Sample", "257 bytes random block");
  Check(RandomBlock(4096), "Sample", "4096 bytes random block");
  Check(RandomBlock(41554), "Sample", "41554 bytes random block");
  Check(RandomBlock(41554), "s", "41554 bytes random block with small key");

  CheckStr("foo", "bar", "Simple case");
  CheckStr("foobar", "password", "password bigger than block");
  CheckStr(
      "kjsldjgfkdsjdsdshgfkkfgjhdkfdjgh", "complex_password", "Larger block");

  CheckStr("", "Foo", "Empty string encryption");

  Check({}, "Bar", "Emtpy block encryption");
  Check(RandomBlock(1337), "", "Empty password encryption");

  auto block = RandomBlock(4097);
  auto blockcopy(block);

  Encryptor cryptor(ConfigManager::DefaultEncryptionKeySize(),
                    "RandomPassword");
  cryptor.Encrypt(block);
  cryptor.Encrypt(blockcopy);
  Test(block != blockcopy, true, "Encryption is non deterministic");

  block = RandomBlock(444);
  blockcopy = block;

  Encryptor decryptor(ConfigManager::DefaultEncryptionKeySize(),
                      "Wrongpassword");
  cryptor.Encrypt(block);
  decryptor.Decrypt(block);
  Test(block != blockcopy, true, "Decryption fails with wrong password");

  Test(cryptor.GenerateKeyHashBase64() != cryptor.GenerateKeyHashBase64(),
       true,
       "Salted password hash is non deterministic");
}

void TestEncryption::CheckStr(const SecuredString& data,
                              const SecuredString& password,
                              const std::string& name)
{
  Encryptor cryptor(ConfigManager::DefaultEncryptionKeySize(), password);
  auto result = cryptor.EncryptBase64(data);

  // Make sure to use two different instances
  Encryptor decryptor(ConfigManager::DefaultEncryptionKeySize(), password);

  Test(decryptor.DecryptBase64(result), data, name);
}

void TestEncryption::Check(const SecuredVector& data,
                           const SecuredString& password,
                           const std::string& name)
{
  // Copy data
  SecuredVector copy(data);

  Encryptor cryptor(ConfigManager::DefaultEncryptionKeySize(), password);

  std::cerr << "TestEncryption: copy.size(): before encryption: " << copy.size() << std::endl;
  cryptor.Encrypt(copy);
  std::cerr << "TestEncryption: copy.size(): after encryption: " << copy.size() << std::endl;

  Test(data != copy, true, "Encryption does change data - " + name);
  std::cerr << "TestEncryption: copy.size(): after test: " << copy.size() << std::endl;


  // Make sure to use two different instances
  Encryptor decryptor(ConfigManager::DefaultEncryptionKeySize(), password);

  std::cerr << "TestEncryption: copy.size(): " << copy.size()
            << ", data.size(): " << data.size() << std::endl;
  decryptor.Decrypt(copy);

  Test(data == copy, true, name);
}

SecuredVector TestEncryption::RandomBlock(size_t len)
{
  SecuredVector block(len);

  std::transform(block.begin(), block.end(), block.begin(), [](unsigned char) {
    return std::rand() % 256;
  });

  return block;
}
