#include "TestPersistence.h"
#include "OSSpecific.h"

using bpm::data::SecuredVector;
using bpm::test::TestPersistence;
using bpm::persistence::KeyHandler;

TestPersistence::TestPersistence() : Tester("Persistence")
{
}

void TestPersistence::RunImpl()
{
  OSSpecific::PersistenceHandler handler(
      ConfigManager::DefaultEncryptionKeySize());

  RunTests(handler);
}

void TestPersistence::RunTests(KeyHandler& handler)
{
  TestThrow([&]() { handler.Clear(); }, false, "Clear persistence key");

  TestThrow(
      [&]() {
        Test(handler.IsKeyAvailable(), false, "Persistence data was cleared");
      },
      false,
      "Persistence data check");

  TestThrow([&]() { handler.RetrieveKey(); },
            true,
            "Read unset persistence data fails");

  ReadWriteData(handler, {}, "Empty block");
  ReadWriteData(handler, {42}, "1 byte block");
  ReadWriteData(handler, {42, 43, 44}, "3 bytes block");

  SecuredVector bigblock(4096, 127);
  ReadWriteData(handler, bigblock, "4096 bytes block");

  TestThrow([&]() { handler.Clear(); }, false, "Clear persistence key");

  TestThrow(
      [&]() { handler.Clear(); }, false, "Clear persistence key one more time");

  TestThrow(
      [&]() {
        Test(handler.IsKeyAvailable(), false, "Persistence data was cleared");
      },
      false,
      "Persistence data check");
}

void TestPersistence::ReadWriteData(KeyHandler& handler,
                                    const SecuredVector& data,
                                    const std::string& name)
{
  TestThrow([&]() { handler.SaveKey(data); },
            false,
            "Set persistence data - " + name);

  TestThrow(
      [&]() {
        auto result = handler.RetrieveKey();
        Test(result == data, true, "Persistence data integrity - " + name);
      },
      false,
      "Read persistence data - " + name);
}
