#include <vector>
#include <algorithm>
#include "TestConfigManager.h"
#include "ConfigManager.h"
#include "Helper.h"

using bpm::test::TestConfigManager;
using bpm::ConfigManager;
using bpm::test::ConfigResult;
namespace
{
  struct ConfigTest
  {
    ConfigTest(const std::string& file, size_t keysize, const std::string& alg,
               bool daemon, const std::set<std::string>& paste_keys = {},
               const std::set<std::string>& exit_keys = {})
    {
      this->file = file;
      this->result =
          bpm::test::ConfigResult{keysize, alg, daemon, paste_keys, exit_keys};
    }

    ConfigTest(const std::string& file)
    {
      result = {bpm::ConfigManager::DefaultEncryptionKeySize(),
                bpm::defs::default_encryption_algorithm,
                bpm::defs::default_daemon_enabled,
                {},
                {}};
      this->file = file;
    }

    std::string file;
    bpm::test::ConfigResult result;
  };

  // Because std::set doesn't have operator==
  template <typename T>
  bool SetEquals(const std::set<T>& a, const std::set<T>& b)
  {
    for (auto const& e : a)
    {
      if (std::find(b.begin(), b.end(), e) == b.end())
      {
        return false;
      }
    }
    return true;
  }
}

TestConfigManager::TestConfigManager() : Tester("ConfigManager")
{
}

void TestConfigManager::RunImpl()
{
  std::vector<ConfigTest> success;

  std::vector<std::string> errors;

  success.emplace_back("IDontExist.mp3");
  success.emplace_back("src/test/resources/big_comment", 256, "AES", true,
                       std::set<std::string>({"Control_L", "v"}));
  success.emplace_back("src/test/resources/normal", 2048, "RSA", false);
  success.emplace_back("src/test/resources/Empty");
  success.emplace_back("src/test/resources/weird", 2048, "AES", true);
  success.emplace_back("src/test/resources/custom",
                       static_cast<size_t>(ConfigManager::DefaultEncryptionKeySize()),
                       "ECC", true);

  success.emplace_back("src/test/resources/paste_keys", 2048, "AES", true,
                       std::set<std::string>{"a"});

  success.emplace_back("src/test/resources/paste_keys", 2048, "AES", true,
                       std::set<std::string>{"a", "b", "Control_L"});

  success.emplace_back("src/test/resources/paste_keys_complex", 2048, "AES", true,
                       std::set<std::string>{"a", "(", "b", ")", "c"});

  success.emplace_back("src/test/resources/exit_keys", 256, "AES", true,
                       std::set<std::string>{},
                       std::set<std::string>{"Ctrl", "Maj", "Enter", "F10"});

  errors.emplace_back("src/test/resources/invalid1");
  errors.emplace_back("src/test/resources/invalid2");
  errors.emplace_back("src/test/resources/invalid3");
  errors.emplace_back("src/test/resources/invalid4");
  errors.emplace_back("src/test/resources/invalid5");
  errors.emplace_back("src/test/resources/paste_keys_bad_start");
  errors.emplace_back("src/test/resources/paste_keys_bad_end");
  errors.emplace_back("src/test/resources/paste_keys_bad_end_multi");
  for (auto const& e : success)
  {
    Load(e.file, e.result);
  }

  for (auto const& e : errors)
  {
    ErrorLoad(e);
  }
}

void TestConfigManager::Load(const std::string& file, const ConfigResult& ref,
                             bool shouldthrow)
{
  std::cout << "Loading: \"" << file << "\"\n";
  std::function<void()> pred = [&file, &ref, this]()
  {
    ConfigManager config;

    config.Load(file);

    Test(config.IsDaemonModeEnabled(), ref.daemon, "Config: Daemon_value");
    Test(config.GetEncryptionKeySize(), ref.keysize, "Config: key size");
    Test(config.GetEncryptionAlgorithm(), ref.algorithm, "Config: algo");
    if (!ref.paste_keys.empty()) // Means default case (OS dependant)
    {
      Test(SetEquals(config.GetPasteKeys(), ref.paste_keys), true,
           "Config: paste_keys");
    }
    if (!ref.exit_keys.empty()) // Means default case (OS dependant)
    {
      Test(SetEquals(config.GetExitKeys(), ref.exit_keys), true,
           "Config: exit_keys");
    }
  };

  if (shouldthrow)
  {
    TestThrow(pred, true, std::string("Config: Load \"") + file + "\"(error)");
  }
  else
  {
    TestThrow(pred, false, "Config: Load \"" + file + "\"(valid)");
  }

  std::cout << std::endl;
}

void TestConfigManager::ErrorLoad(const std::string& file)
{
  Test(Helper::FileExists(file), true, "Test resource is present: " + file);
  Load(file, {0, "", false, {}, {}}, true);
}
