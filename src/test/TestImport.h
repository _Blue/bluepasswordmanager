#pragma once

#include <memory>
#include <vector>

#include "data/Storage.h"
#include "TestStorage.h"
#include "Tester.h"

namespace bpm
{
  namespace test
  {
    enum class ConflictType
    {
      None,
      User,
      Password,
      Same
    };

    struct StorageImport
    {
      StorageImport(const StorageContent& content,
                    ConflictType conflict,
                    data::Storage::ConflictAction result)
      {
        this->content = content;
        this->result = result;
        this->conflict = conflict;
      }
      StorageContent content;
      ConflictType conflict;
      data::Storage::ConflictAction result;
    };

    class TestImport : public Tester
    {
    public:
      TestImport();

      virtual void RunImpl() override;

    private:
      void RunTests();
      void RunResourceTest();
      void Add(const StorageContent& content);
      void Import(const StorageContent& content,
                  ConflictType type,
                  data::Storage::ConflictAction result =
                      data::Storage::ConflictAction::Cancel);
      void Check();
      void ValidateImportResult();
      void ImportImpl(bool serialized);
      void ImportContent(const StorageImport& import,
                         StorageContent* current,
                         bool serialized);
      void TestSerialization();

      static void CopyStorage(const std::string& src, const std::string& dest);

      data::Storage::ConflictPred
      BuildPred(bool& called,
                const data::SecuredString& domain = "",
                const data::SecuredString& expected_current = "",
                const data::SecuredString& expected_imported = "",
                size_t expected_current_ts = 0,
                size_t expected_imported_ts = 0,
                data::Storage::ConflictAction result =
                    data::Storage::ConflictAction::Cancel);

      std::vector<StorageContent> _content;
      std::vector<StorageImport> _imports;
      std::unique_ptr<data::Storage> _storage;
    };
  }
}
