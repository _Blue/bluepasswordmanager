#pragma once

#include <string>
#include <set>
#include "Tester.h"

namespace bpm
{
  namespace test
  {
    struct ConfigResult
    {
      size_t keysize;
      std::string algorithm;
      bool daemon;
      std::set<std::string> paste_keys;
      std::set<std::string> exit_keys;
    };

    class TestConfigManager : public Tester
    {
    public:
      TestConfigManager();

      virtual void RunImpl() override;

    private:
      void Load(const std::string& file, const ConfigResult& result,
                bool shouldthrow = false);
      void ErrorLoad(const std::string& file);
    };
  }
}
