#include "TestMain.h"

#include "Application.h"

using bpm::test::TestMain;

using std::vector;

class DummyApp : public bpm::Application
{
public:
  std::string Config() const
  {
    return GetConfigPath();
  }

  std::string Storage() const
  {
    return GetStoragePath();
  }

  bool DisableTimestamps() const
  {
    return _disable_import_timestamp;
  }
};

TestMain::TestMain() : Tester("Command line parsing")
{
}

void TestMain::RunImpl()
{
  LaunchMain({});

  LaunchMain({"-s"}, nullptr, nullptr, false);
  LaunchMain({"-c"}, nullptr, nullptr, false);
  LaunchMain({"-whatever"}, nullptr, nullptr, false);

  LaunchMain({"-c", "cfg_path_test"}, "cfg_path_test");
  LaunchMain({"-s", "storage_path_test"},
             defs::default_config_path,
             "storage_path_test");

  LaunchMain({"-c", "cfg", "-s", "storage"}, "cfg", "storage");

  LaunchMain({"-c", "-s"}, nullptr, nullptr, false);
  LaunchMain(
      {"-i", "file"}, defs::default_config_path, defs::default_storage_path);
  LaunchMain({"-i"}, defs::default_config_path, defs::default_storage_path);
  LaunchMain({"-t"},
             defs::default_config_path,
             defs::default_storage_path,
             true,
             true);
  LaunchMain({"-c", "storage", "-t"},
             "storage",
             defs::default_storage_path,
             true,
             true);

  LaunchMain({"RandomOption"}, nullptr, nullptr, false);
}

void TestMain::LaunchMain(const vector<const char*>& args,
                          const char* config,
                          const char* storage,
                          bool valid,
                          bool disable_timestamp)
{
  // Append the program name to the vector
  auto full_args = vector<const char*>{"bpm"};
  full_args.insert(full_args.end(), args.begin(), args.end());

  // Get vector internal pointer
  char** argv = const_cast<char**>(full_args.data());
  size_t argc = full_args.size();

  DummyApp app;
  // Create lambda
  auto pred = [&argv, argc, &app]() {
    app.ApplyArgv(argc, argv);
  };

  // Execute test
  TestThrow(pred, !valid, "Argument parsing");

  // Verify values validity
  if (valid)
  {
    Test(std::string(config), app.Config(), "Config path is correct");
    Test(std::string(storage), app.Storage(), "Storage path is correct");
    Test(disable_timestamp,
         app.DisableTimestamps(),
         "Import timestamp is correct");
  }
}
