#pragma once

#include <vector>
#include "Tester.h"
#include "Defs.h"
namespace bpm
{
  namespace test
  {
    class TestMain : public Tester
    {
    public:
      TestMain();

      virtual void RunImpl() override;

    private:
      void LaunchMain(const std::vector<const char*> &args,
                      const char* config = defs::default_config_path,
                      const char* storage = defs::default_storage_path,
                      bool valid = true,
                      bool disable_timestamp = false);
    };
  }
}
