#pragma once

#include <vector>
#include <memory>

#include "data/Storage.h"
#include "Tester.h"
#include "Helper.h"

namespace bpm
{
  namespace test
  {
    struct StorageContent
    {
      StorageContent(const data::SecuredString& d,
                     const data::SecuredString& u,
                     const data::SecuredString& p,
                     size_t t = Helper::Now())
          : domain(d), user(u), password(p), ts(t)
      {
      }

      StorageContent() = default;

      data::SecuredString domain;
      data::SecuredString user;
      data::SecuredString password;
      size_t ts;
    };

    class TestStorage : public Tester
    {
    public:
      TestStorage();

      virtual void RunImpl() override;

    private:
      void RunTests();
      void Add(const StorageContent& content);
      void Remove(const data::SecuredString& domain);
      void ChangeUser(const data::SecuredString& domain,
                      const data::SecuredString& user,
                      size_t ts = Helper::Now());
      void ChangeDomain(const data::SecuredString& olddomain,
                        const data::SecuredString& newdomain,
                        size_t ts = Helper::Now());

      void ChangePassword(const data::SecuredString& domain,
                        const data::SecuredString& newpassword,
                        size_t ts = Helper::Now());

      void Check();

      std::vector<StorageContent> _content;
      std::vector<data::SecuredString> _removed;
      std::shared_ptr<data::Storage> _storage;
    };
  }
}
