#include <algorithm>
#include <cassert>
#include "TestStorage.h"
#include "Helper.h"
#include "data/SecuredStorage.h"
#include "ConfigManager.h"

using bpm::data::SecuredStorage;
using bpm::data::SecuredString;

namespace bpm
{
  namespace test
  {

    TestStorage::TestStorage() : Tester("Storage")
    {
    }

    void TestStorage::RunImpl()
    {
      // Run test with both encrypted and non encrypted storage
      _storage = std::make_shared<data::Storage>("storage.clear");
      Test(_storage->IsBeingCreated(), true, "new storage creation state");

      RunTests();

      // Create encrypted storage
      _storage = std::make_shared<data::SecuredStorage>(
          "storage.encrypted",
          ConfigManager::DefaultEncryptionKeySize(),
          "ILikeTrains");

      Test(_storage->IsBeingCreated(), true, "new storage creation state");

      // Reset data shadow
      _content.clear();
      _removed.clear();
      RunTests();

      // Destroy storage and create another instance, check that data is
      // still the same
      _storage = std::make_unique<data::SecuredStorage>(
          "storage.encrypted",
          ConfigManager::DefaultEncryptionKeySize(),
          "ILikeTrains");
      Check();
      Test(_storage->IsBeingCreated(), false, "storage open creation state");

      // Check that openning the storage with another password fails

      auto other = std::make_unique<SecuredStorage>(
          "storage.encrypted",
          ConfigManager::DefaultEncryptionKeySize(),
          "IDontLikeTrains");

      Test(other->Good(), false, "Bad password fails to unlock storage");

      other = std::make_unique<SecuredStorage>(
          "storage.encrypted",
          ConfigManager::DefaultEncryptionKeySize(),
          "ILikeTrains");
      Test(other->Good(), true, "Correct password unlocks storage");
      Test(_storage->IsBeingCreated(), false, "storage open creation state");
    }

    void TestStorage::RunTests()
    {
      Add({"foo.com", "foo", "bar", 10});
      Check();

      ChangeUser("foo.com", "foo2", 12);
      Check();
      ChangeUser("foo.com", "foo3", 12);
      Check();

      TestThrow(
          [this]() {
            _storage->ChangeUsername("IDontExist.com", "koin", Helper::Now());
          },
          true,
          "Change user with unset domain");

      Remove("foo.com");
      Check();

      Add({"foo.com", "foo", "bar", 14});
      Check();
      ChangePassword("foo.com", "bar2");
      Check();

      Add({"foo2.com", "foo2", "bar2", 16});
      Add({"foo3.com", "foo3", "bar3", 18});
      Add({"foo4.com", "foo4", "bar4", 20});
      ChangeUser("foo3.com", "foo4", 22);
      ChangeUser("foo2.com", "foo7", 24);
      Remove("foo3.com");

      Check();

      ChangeDomain("foo4.com", "foo12.com", 26);
      Check();

      ChangePassword("foo12.com", "bar1337", 47);
      Check();

      TestThrow(
          [this]() {
            _storage->ChangeDomain("IDontExist.com", "", Helper::Now());
          },
          true,
          "Change domain with unset domain");

      Remove("foo.com");
      Add({"foo.com", "foo", "bar", 28});
      TestThrow(
          [this]() {
            _storage->ChangeDomain("foo.com", "foo2.com", Helper::Now());
          },
          true,
          "Change domain override already existing domain");

      TestThrow([this]() { ChangeDomain("foo.com", "foo2.com"); },
                false,
                "Change domain override");

      Check();

      Test(_storage->AddCred("foo2.com", "", "", Helper::Now(), false),
           false,
           "Duplicate check");

      Check();

      _storage->DeleteCred("bar"); // Check invalid deletion
      Check();

      Add({"lastone.net", "user", "password", 30});
      Remove("foo2.com");
      Check();

      TestThrow( // Add empty string as domain name (invalid)
          [this]() { _storage->AddCred("", "a", "b", Helper::Now(), true); },
          true,
          "Add empty domain to storage");

      Add({"bluecode.fr", "", "empty username", 32});
      Add({"bluecode2.fr", "empty password", "", 34});
      Add({"empty.com", "", "", 36});

      Check();
      Test(*_storage == *_storage, true, "Storage is equal to itself");
      Test(*_storage != *_storage, false, "Comparaison operator is consistant");

      Add({"target.com", "foo", "bar"});
      Add({"linkto-target", "bpm-symlink", "target.com"});

      bpm::data::Storage::String user;
      bpm::data::Storage::String password;

      _storage->ReadCred("linkto-target", user, password, nullptr);
      Test<bpm::data::Storage::String>(user, "foo", "Symlink username");
      Test<bpm::data::Storage::String>(password, "bar", "Symlink password");

      Add({"meta-link.com", "bpm-symlink", "target.com"});
      _storage->ReadCred("meta-link.com", user, password, nullptr);
      Test<bpm::data::Storage::String>(user, "foo", "Symlink username");
      Test<bpm::data::Storage::String>(password, "bar", "Symlink password");

      ChangeUser("target.com", "bpm-symlink", 1);
      ChangePassword("target.com", "meta-link.com", 1);

      TestThrow(
          [&]() {
            _storage->ReadCred("meta-link.com", user, password, nullptr);
          },
          true,
          "Symlink loop");

      TestThrow(
          [&]() { _storage->ReadCred("target.com", user, password, nullptr); },
          true,
          "Symlink loop 2");

      TestThrow(
          [&]() {
            _storage->ReadCred("linkto-target", user, password, nullptr);
          },
          true,
          "Symlink loop 3");

      ChangeUser("target.com", "new-user", 3);
      ChangePassword("target.com", "new-password", 4);
      _storage->ReadCred("meta-link.com", user, password, nullptr);
      Test<bpm::data::Storage::String>(
          user, "new-user", "Updated symlink username");
      Test<bpm::data::Storage::String>(
          password, "new-password", "Updated symlink password");

      Remove("target.com");

      TestThrow(
          [&]() {
            _storage->ReadCred("linkto-target", user, password, nullptr);
          },
          true,
          "Broken symlink");

      Remove("linkto-target");
      Remove("meta-link.com");
    }

    void TestStorage::Add(const StorageContent& content)
    {
      _storage->AddCred(
          content.domain, content.user, content.password, content.ts, true);
      _content.push_back(content);
    }

    void TestStorage::Remove(const SecuredString& domain)
    {
      _storage->DeleteCred(domain);
      _content.erase(std::remove_if(_content.begin(),
                                    _content.end(),
                                    [&domain](auto const& e) {
                                      return e.domain == domain;
                                    }),
                     _content.end());

      _removed.push_back(domain);
    }

    void TestStorage::ChangeUser(const SecuredString& domain,
                                 const SecuredString& user,
                                 size_t ts)
    {
      _storage->ChangeUsername(domain, user, ts);

      auto it =
          std::find_if(_content.begin(),
                       _content.end(),
                       [&domain](const auto& e) { return e.domain == domain; });

      assert(it != _content.end());
      it->user = user;
      it->ts = ts;
    }

    void TestStorage::ChangePassword(const SecuredString& domain,
                                     const SecuredString& new_password,
                                     size_t ts)
    {
      _storage->ChangePassword(domain, new_password, ts);

      auto it =
          std::find_if(_content.begin(),
                       _content.end(),
                       [&domain](const auto& e) { return e.domain == domain; });

      assert(it != _content.end());
      it->password = new_password;
      it->ts = ts;
    }

    void TestStorage::ChangeDomain(const SecuredString& olddomain,
                                   const SecuredString& newdomain,
                                   size_t ts)
    {
      // Change domain in storage
      _storage->ChangeDomain(olddomain, newdomain, ts, true);

      // Remove target
      _content.erase(std::remove_if(_content.begin(),
                                    _content.end(),
                                    [&newdomain](auto const& e) {
                                      return e.domain == newdomain;
                                    }),
                     _content.end());

      // Change domain in storage copy
      auto it = std::find_if(
          _content.begin(), _content.end(), [&olddomain](const auto& e) {
            return e.domain == olddomain;
          });

      assert(it != _content.end());
      it->domain = newdomain;
      it->ts = ts;
    }

    void TestStorage::Check()
    {
      std::string type;

      if (_storage->Encrypted())
      {
        type = ", (encrypted storage)";
      }

      Test(_storage->Good(), true, "Storage is valid" + type);

      for (auto const& e : _content)
      {
        Test(_storage->HasCred(e.domain),
             true,
             "Storage has credentials (" + e.domain + ")" + type);

        SecuredString user;
        SecuredString password;
        size_t ts = 0;
        Test(_storage->ReadCred(e.domain, user, password, &ts),
             true,
             "Read credentials (" + e.domain + ")" + type);

        Test(user, e.user, "User is valid" + type);
        Test(password, e.password, "Password is valid" + type);
        Test(ts, e.ts, "Timestamp is valid" + type);
      }

      for (auto const& e : _removed)
      {
        Test(_storage->HasCred(e), false, "Credentials were removed" + type);
        SecuredString user;
        SecuredString password;
        Test(_storage->ReadCred(e, user, password),
             false,
             "Credentials are removed" + type);
      }

      _removed.clear();
    }
  }
}
