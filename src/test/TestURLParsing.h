#pragma once

#include <string>
#include "Tester.h"

namespace bpm
{
  namespace test
  {
    class TestURLParsing : public Tester
    {
    public:
      TestURLParsing();

      virtual void RunImpl() override;
    };
  }
}
