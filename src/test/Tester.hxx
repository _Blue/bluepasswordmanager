#pragma once

#include <iostream>
#include "Tester.h"
#include "Colors.h"

using bpm::test::Tester;
using bpm::test::Colors;

template <typename T>
bool Tester::Test(const T& value, const T& ref, const std::string& name)
{
  if (ref != value)
  {
    std::cout << Colors::Red() << "FAILED: " << name << "\n" << Colors::Reset();

    std::cout << "\tRef: \"" << ref << "\", result: \"" << value << "\""
              << std::endl;
    _failed++;
    return false;
  }
  else
  {
    std::cout << Colors::Green() << "PASSED: " << name << ": " << name << "\n"
              << Colors::Reset();
    _passed++;
    return true;
  }
}
