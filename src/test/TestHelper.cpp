#ifdef _WIN32
#define WIN32_LEAN_AND_MEAN
#include <Windows.h>
#endif

#include "TestHelper.h"
#include "Helper.h"

namespace bpm
{
  namespace test
  {
    TestHelper::TestHelper() : Tester("File operations")
    {
    }

    void TestHelper::RunImpl()
    {
      Test(Helper::FolderExists("."), true, "Folder existence");
      Test(Helper::FolderExists("IDontExist"), false, "Folder absence");

      TestThrow(
          []()
          {
            Helper::CreateFolder("test_folder");
          },
          false, "Basic folder creation");

      TestThrow(
          []()
          {
            Helper::CreateFolder("test_folder/sub_folder");
          },
          false, "Sub folder creation");

      Test(Helper::FolderExists("test_folder"), true, "test_folder presence");
      Test(Helper::FolderExists("test_folder/sub_folder"), true,
           "sub_folder presence");

      Test(Helper::FileExists("test_folder"), false, "File as folder");

      std::string filename = "check";

#ifdef _WIN32
      //Because windows binary name can vary
      filename.resize(MAX_PATH);

      filename.resize(GetModuleFileName(nullptr,
      const_cast<char*>(filename.data()),MAX_PATH));
#endif

      Test(Helper::FileExists(filename), true, "Existing file verification");

      TestThrow(
          []()
          {
            Helper::Remove("IDontExist");
          },
          false, "Deletion of non-existing");

      TestThrow(
          []()
          {
            Helper::Remove("test_folder/sub_folder");
          },
          false, "Deletion of existing sub_folder");

      TestThrow(
          []()
          {
            Helper::Remove("test_folder");
          },
          false, "Deletion of existing folder");

      Test(Helper::FolderExists("test_folder"), false, "Folder deleted");

      TestThrow(
          []()
          {
            Helper::CreateFolder("test_folder/sub/subsub/subsubsub");
          },
          false, "Depth folder creation");

      Test(Helper::FolderExists("test_folder/sub/subsub/subsubsub"), true,
           "Tree has been created");

      TestThrow(
          [filename]()
          {
            Helper::CreateFolder(filename);
          },
          true, "Invalid folder creation (file exists)");

      TestThrow(
          []()
          {
            Helper::Remove("test_folder/sub/subsub/subsubsub");
            Helper::Remove("test_folder/sub/subsub");
            Helper::Remove("test_folder/sub");
            Helper::Remove("test_folder");
          },
          false, "Deletion of existing folder (4 at once)");

      Test(Helper::FolderExists("test_folder"), false, "test_folder removed");
    }
  }
}
