#include <iostream>
#include "Application.h"
#include "CancelException.h"

int Run(int argc, char** argv)
{
  bpm::Application app;

  try
  {
    app.ApplyArgv(argc, argv);
    return app.Run();
  }
  catch (const bpm::CancelException&)
  {
    return 1;
  }
  catch (const std::exception& e)
  {
    std::cerr << "bpm ended due to the following error: \n"
              << e.what() << std::endl;
  }
  return 1;
}

int main(int argc, char** argv)
{
  Run(argc, argv);
}
