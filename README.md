# BluePasswordManager

*Get your credentials in your clipboard when you need them*

This C++ project is a password keeper.  
The given credentials are stored on a secured storage (encrypted using AES in CBC mode).  

When credentials are needed, *BPM* asks for the master password and then allows you to log in  
if the given password is correct, then BPM can decrypt the secured storage.

## Features:

* Encrypted local credential storage in a git track-able format
* Timestamp based synchronization between different machines
* Secure password generation
* Per-login master password persistence
* GUI to view and edit stored credentials (`bpm -l`)
* Zero interaction credential access in scripts (via `bpm -u` and `bpm -p`)
* Clipboard based credential access (bpm tracks ctrl-c and ctrl-v keypresses to put the right credentials when needed)
* Symlinks to point different URL's to the same underlying credentials (configurable via the GUI)
* Windows, Linux and Android support

## Usage
The procedure to use *BPM* is the following:

* Start *BPM*
* Copy the URI of the web site you want to log into in your clipboard
* If this URI is know by *BPM*, the username will be pasted in your clipboard
* Then *BPM* waits for a key stroke (ctrl-V by default) to put the password in your clipboard
* *BPM* then waits for a another key stroke to know that you pasted the password, and restore the clipboard (as it were before *BPM* was started
* If the URI was not in the secured storage, *BPM* will ask for the credentials and store them in his secured storage.

**Note that if you forget your password, there is no way to recover your saved credentials, be careful**

### Command line options

* **-c <config_path>** override the default path of the configuration file
* **-s <storage_path>** override the default path of the encrypted storage file
* **-p** print the content of the secured storage, __ warning: shows passwords in clear text__
* **-x** delete any persistence data (memory area use to store storage's encryption key while the computer if powered on)
* **-k** show the names of the key that are pressed (useful when setting `paste_key` in configuration)
* **-l** Show the content of storage within a window. This window allows the user to changes domain names, user names and passwords
* **-i [storage_path]** import the content of another secured storage. Will prompt the user in case of conflict. If [storage_path] is omitted, read from stdout
* **-t** When importing a storage, don't take the credentials with the latest timestamp, but ask manually for each conflict
* **-u <url>** Print the username associated with <url> on stdout
* **-p <url>** Print the password associated with <url> on stdout
* **-e** Export all the storage content on stdout
* **-z** Export the storage encryption key

### Configuration

The configuration file's location is `~/.config/bpm/config`.  
If absent, bpm will use default values.  

The configuration file format is the following:  

`value_name` = `value`  

or  

`enable_value`  

`#` represents the begining of a comment  


Below is an example configuration:  
```
paste_keys = (Control_L)(v) #Tell BPM that we use ctrl+v to paste
enable_daemon #Enable daemon mode (Store the encryption key in memory until the user logs out)
banner_x = 20 #Set banner x offset to 20 pixels
```

Configuration values:

* **encryption**: encryption algorithm to use, default: `AES`
* **key_size**: encryption key size (in bits), default: `512`
* **enable_daemon**: enable daemon mode, default value
* **disable_daemon**: disable daemon mode, ask for the storage password every time
* **paste_keys**: key stroke used to paste, in the following format: (key_1)(key2)(...), default: `(Control_L)(v)`
* **exit_keys**: key stroke to exit the program, default: `(Escape)`
* **storage_path**: path pointing to the encrypted storage, default: `~/.config/bpm/passwd`
* **banner_x**: banner offset on x axis, default: depends on the platform
* **banner_y**: same as above, but on the y axis
* **disable_banner**: do not show the banner while logging in
* **enable_libsecret**: Use libsecret to store the storage key, allowing the user to save the storage passphrase, even between reboots. (Supported on Linux only)
* **password_size**: The length of generated passwords. Default: 15
* **password_charset**: The set of characters to generate passwords with. Default: `ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/|,.;!?:*+-=[]()|{}'\"\\#~&<>$^%";`


## Synchronizing between devices

The easiest way to synchronize different credentials between devices is over ssh / psremote. To import the content of a remote machine, do:

```
$ ssh remote-machine bpm -e | bpm -i
```

To export the content of the local storage to a remote machine, do:

```
$ bpm -e | ssh remote-machie bpm -i
```

Note that if the remote machine's storage is locked, the storage key can be forwarded via:

```
bpm -e | ssh remote-machine bpm_storage_key=$(bpm -z) bpm -i
```

When imported credentials conflicts with local credentials, the entry that the changed at the latest timestamp is chosen by default. This behavior can be disabled with `-t`, in which case bpm will ask the user for each conflicts.


## Android support
You can read and use bpm password storages on Android using [BPMDroid](https://bitbucket.org/_Blue/BPMDroid)

## Build from source
### Linux
You need the following libraries on your system:

* [gtk 3](http://www.gtk.org/)
* [Crypto++ 5 ](https://www.cryptopp.com/)
* [libsecret-1](https://wiki.gnome.org/Projects/Libsecret)
* X11

To build, clone the repository:

```
git clone https://bitbucket.org/_Blue/bluepasswordmanager
```

Move to the repository, and run make

```
cd bluepasswordmanager
make
```

You can run `make install` to copy the binaries in /usr/bin.  
*Note: to run the test suite, you can run "make check"*

### Windows

To build for Windows, you need at least Visual Studio 2017.  

Build steps:

```bash
# Clone this repository and its submodules
$ git clone https://_Blue@bitbucket.org/_Blue/bluepasswordmanager.git --recursive

# Build cryptopp
$ cd deps/cryptopp
$ msbuild cryptest.sln /p:Configuration="Release"

# Build bpm
$ cd ../../
$ msbuild BluePasswordManager.sln /p:Configuration="Release" /p:Platform="X86"

# (Optional) Run the unit tests
$ Win32\BPMTest\Release\check.exe
```

Alernatively, you can build all platform and targets by running:

```bash
$ tools\windows-ci.bat
```

## Build modules
* **core** (src/core): Contains the core cryptographic and storage logic
* **gui** (src/gui): Contains the user interface and application logic
* **app** (src/app): Contains the boilerplate code to have an entry point that starts the application logic
* **light-app** (src/app): A console only entry point. Mainly used to have a command line tool on Android
* **test** (src/test): Contains the test cases and the test suite logic

## Technical documentation
Technical documentation is available [here](https://bluecode.fr/pages/projects/bpm-doc.html).

## License
You are allowed to use, modify and redistribute the content of this repository as you wish.
