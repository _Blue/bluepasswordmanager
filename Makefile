THREADS = 8

all: bpm bpm-light

seedgen:
	$(MAKE) -f src/seedgen/Makefile

core: seedgen
	$(MAKE) -f src/core/Makefile -j$(THREADS)

gui:
	$(MAKE) -f src/gui/Makefile -j$(THREADS)

bpm: core gui
	$(MAKE) -f src/app/Makefile -j$(THREADS)

bpm-light: core
	$(MAKE) -f src/light-app/Makefile -j$(THREADS)

install: bpm
	$(MAKE) -f src/core/Makefile install
	$(MAKE) -f src/gui/Makefile install
	$(MAKE) -f src/app/Makefile install

check: core
	$(MAKE) -f src/test/Makefile -j$(THREADS)
	LD_LIBRARY_PATH=./lib ./check

android-check: core
	$(MAKE) -f src/test/Makefile -j$(THREADS)

clean:
	$(MAKE) -f src/core/Makefile clean
	$(MAKE) -f src/gui/Makefile clean
	$(MAKE) -f src/app/Makefile clean
	$(MAKE) -f src/light-app/Makefile clean
	$(MAKE) -f src/test/Makefile clean
	$(MAKE) -f src/seedgen/Makefile clean

.PHONY: core gui
