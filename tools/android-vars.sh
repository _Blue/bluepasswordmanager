#! /bin/bash


# Export the installation pathes
export ANDROID_SDK_PATH="/tmp/bpm/ndk-instance-$cpu/bin"
export AOSP_TOOLCHAIN_PATH="/tmp/bpm/ndk-instance-$cpu/bin"
export ANDROID_NDK_ROOT=/tmp/bpm/android-ndk-r21
export ANDROID_SDK_ROOT=$ANDROID_SDK_PATH
export ANDROID_API=29
export PATH="$ANDROID_SDK_PATH":"$PATH"

