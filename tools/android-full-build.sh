#! /bin/bash

set -ue

if [ "$cpu" == 'arm' ]; then
  export ANDROID_CPU="armv7a" # Hack around inconsistent naming in crypto++
else
  export ANDROID_CPU="$cpu"
fi

# Build cryptopp

export SOLIB_FLAGS=-Wl,-soname,libcryptopp.so
bash -c "source tools/android-vars.sh &&
         cd deps/cryptopp &&
         source ./TestScripts/setenv-android.sh &&
         set -x &&
         LDFLAGS=\$(echo \$LDFLAGS | sed s\|-Wl,--gc-sections\|\|g) &&
         make -f GNUmakefile-cross clean &&
         make -f GNUmakefile-cross shared -j12"

# Build BPM core
make clean
tools/build-android.sh core

