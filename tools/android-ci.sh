#! /bin/bash

set -ue

export cpu="$1"
export lib_name="$2"

# First download and setup the toolchain
tools/setup-android-env.sh "$cpu"

# Build cryptopp and BPM
tools/android-full-build.sh

tools/build-android.sh bpm-light
# Then build check (the command will fail because of architecture mismatch)
tools/build-android.sh check light-app|| true

# Ensure that the binary exists
file ./check

echo "Android build success verified (platform: $cpu)"
