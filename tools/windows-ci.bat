@echo off

call :build_and_test Debug, Win32, x86 || goto fail
call :build_and_test Debug, x64, x64 || goto fail
call :build_and_test Release, Win32, x86 || goto fail
call :build_and_test Release, x64, x64 || goto fail

exit /b 0

:fail
echo An error occured
exit /b 1

:build_and_test
msbuild deps\cryptopp\cryptest.sln /p:Configuration=%~1 /p:Platform=%~2 || goto fail
msbuild BluePasswordManager.sln /p:Configuration=%~1 /p:Platform=%~3 || goto fail

exe\%~2\%~1\check.exe || goto fail