#! /bin/bash

set -ue

# Prepare ndk folder
mkdir -p /tmp/bpm

# First download the android ndk
if ! [ -f /tmp/bpm/android-ndk-r21-linux-x86_64.zip ]; then
  wget --tries=10  https://dl.google.com/android/repository/android-ndk-r21-linux-x86_64.zip -P /tmp/bpm
fi

# Then unzip it
if ! [ -d /tmp/bpm/android-ndk-r21 ]; then
  unzip /tmp/bpm/android-ndk-r21-linux-x86_64.zip -d /tmp/bpm
fi

# Install the platform we want
if ! [ -d /tmp/bpm/ndk-instance-$1/bin ]; then
  /tmp/bpm/android-ndk-r21/build/tools/make_standalone_toolchain.py --arch $1 --install-dir /tmp/bpm/ndk-instance-$1 --api 29
fi
