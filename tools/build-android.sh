#! /bin/bash

set -ue

# seedgen needs to run on the current platform
make seedgen

source tools/android-vars.sh

export CC=$lib_name-clang++
export CXX=$lib_name-clang++
export LD=$lib_name-ld

make PLATFORM=ANDROID $*
