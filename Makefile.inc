#Compilation flags
CXXFLAGS += -std=c++17 -Wall -Wextra -O2 -g3 $(shell pkg-config --cflags $(DEPS)) -I$(DIRPATH) -Isrc/core -Isrc/gui -fPIC
LDFLAGS += -L$(LIB_OUT)

ifeq ($(PLATFORM), ANDROID)
	CXXFLAGS += -Ideps -DANDROID -frtti -fexceptions -funwind-tables
  LDFLAGS += -ldl
else
	LDFLAGS += -lm -lrt $(shell pkg-config --libs $(DEPS))
endif

# Directories
LIB_OUT = lib

# Eventual library name
ifdef LIBNAME
  LIB = $(LIB_OUT)/lib$(LIBNAME).so
endif

# Size of the seed key in bits
RANDOM_BLOB_SIZE = 512

# Objects
OBJ = $(addprefix $(DIRPATH), $(addsuffix .o, $(SRC)))

# Rules
$(LIB): $(OBJ)
	mkdir -p $(LIB_OUT)
	$(LINK.c) -Wl,-soname,$(shell basename $(LIB)) $(LDFLAGS) -shared $^ -o $@

$(BIN): $(OBJ)
	$(CXX) $(CXXFLAGS) $(OBJ) $(LDFLAGS) -o $(BIN)


clean:
	$(RM) $(OBJ) $(LIB) $(BIN)
	$(RM) -r $(LIB_OUT)

ifdef LIB
install: all
	cp -i $(LIB) /usr/lib
else
install: all
	cp -i $(BIN) /usr/bin
endif
